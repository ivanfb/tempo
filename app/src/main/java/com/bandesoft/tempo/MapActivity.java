package com.bandesoft.tempo;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bandesoft.tempo.Utils.Prefs;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import static android.provider.SettingsSlicesContract.KEY_LOCATION;

public class MapActivity extends BaseActivity
   implements OnMapReadyCallback, LocationListener, SeekBar.OnSeekBarChangeListener {

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    public static final String EXTRA_CITY_NAME = "EXTRA_CITY_NAME";
    public static final String EXTRA_LON = "EXTRA_LON";
    public static final String EXTRA_LAT = "EXTRA_LAT";
    public static final String EXTRA_RADIUS = "EXTRA_RADIUS";
    public static final String EXTRA_PREVIOUS_ACTIVITY_IS_PROFILE = "EXTRA_PREV_ACT_IS_PROF";

    public static final String RESULT_POS_TEXT = "RES_POS_TEXT";
    public static final String RESULT_POS_LAT = "RES_POS_LAT";
    public static final String RESULT_POS_LON = "RES_POS_LON";
    public static final String RESULT_RADIUS = "RES_RADIUS";
    private static final String KEY_CAMERA_POSITION = "CAMERA_POS";

    private static final double ZGZ_LAT = 41.655609;
    private static final double ZGZ_LON = -0.879597;

    private static final double BARNA_LAT = 41.397273;
    private static final double BARNA_LON = 2.165191;

    private static final String TAG = "MapActivity";

    private static final int ZOOM = 11;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;

    private String mCity;
    private LatLng mPos;
    private int mMinRadiusKm;
    private int mRangeKm;

    private Location mUserLocation;
    private int mNumLocationUpdates = 0;

    private LocationManager mLocationManager;

    private GoogleMap mMap;

    private TextView mTextViewRadiusKm;
    private int mSelectedRadiusKm;

    private boolean mComesFromProfileActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent i = getIntent();

        mComesFromProfileActivity =
                i.getBooleanExtra(EXTRA_PREVIOUS_ACTIVITY_IS_PROFILE, false);

        mMinRadiusKm = getResources().getInteger(R.integer.min_radius_km);
        mRangeKm = getResources().getInteger(R.integer.range_radius_km);

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (savedInstanceState != null) {
            mPos = savedInstanceState.getParcelable(KEY_LOCATION);
        }
        else {
            double lon = getIntent().getDoubleExtra(EXTRA_LON, ZGZ_LON);
            double lat = getIntent().getDoubleExtra(EXTRA_LAT, ZGZ_LAT);
            mPos = new LatLng(lat, lon);
        }

        mCity = i.getStringExtra(EXTRA_CITY_NAME);

        mSelectedRadiusKm = i.getIntExtra(EXTRA_RADIUS, mMinRadiusKm);
        mTextViewRadiusKm = findViewById(R.id.text_radious);
        mTextViewRadiusKm.setText(mSelectedRadiusKm + " km");

        SeekBar seekBar = findViewById(R.id.seek_bar_radious);
        seekBar.getProgressDrawable().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
        seekBar.getThumb().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
        seekBar.setProgress((int)(((float)(mSelectedRadiusKm - mMinRadiusKm) / mRangeKm) * 100));
        seekBar.setOnSeekBarChangeListener(this);

        if (mComesFromProfileActivity) {
            seekBar.setVisibility(View.GONE);
            mTextViewRadiusKm.setVisibility(View.GONE);
        }

        FloatingActionButton lBtnCenterMapInMyLoc = findViewById(R.id.center_screen_in_current_location);
        lBtnCenterMapInMyLoc.setOnClickListener((View view) -> {
                if (mUserLocation != null) {

                    mMap.setMyLocationEnabled(true);

                    LatLng lCurrentPos = new LatLng(mUserLocation.getLatitude(), mUserLocation.getLongitude());
                    centerCameraLocation(lCurrentPos);
                    setMarker();
                    Toast.makeText(this, R.string.this_is_your_current_location, Toast.LENGTH_LONG).show();
                }
            }
        );

        FloatingActionButton lSetMarkerToCenter = findViewById(R.id.set_marker_in_center_screen);
        lSetMarkerToCenter.setOnClickListener((View view) -> {
            setMarkerInCenterScreen();
            Toast.makeText(this, R.string.position_saved, Toast.LENGTH_LONG).show();
        });

        SupportMapFragment mapFragment =
                ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(this); // to make the map ready sometime

        super.initToolBar();

        checkLocationPermission();
        activateLocationSettings();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mPos);
            super.onSaveInstanceState(outState);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startPositionUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopPositionUpdates();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(mPos, ZOOM);
        mMap.animateCamera(cameraUpdate);

        setMarker();

        mMap.setOnMapClickListener(latLng -> {
            mPos = latLng;
            requestLocationFromGpsCoordinates();
            setMarker();
        });
    }

    private void onLocationSettingsSatisfied() {
        if (mUserLocation == null) {
            getLastLocation();
            startPositionUpdates();
            centerCameraLocation(mPos != null ? mPos:new LatLng(ZGZ_LAT, ZGZ_LON));
        }
    }

    private void onLocationSettingsNotSatisfied(@NonNull Exception e) {
        if (e instanceof ResolvableApiException) {
            // Location settings are not satisfied, but this can be fixed
            // by showing the user a dialog.
            try {
                // Show the dialog by calling startResolutionForResult(),
                // and check the result in onActivityResult().
                ResolvableApiException resolvable = (ResolvableApiException) e;
                resolvable.startResolutionForResult(this, Prefs.REQUEST_CHECK_SETTINGS);
            } catch (IntentSender.SendIntentException sendEx) {
                // Ignore the error.
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            onLocationSettingsSatisfied();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {

                if (grantResults.length > 0) {
                    boolean fineLocation = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (fineLocation) {
                        if (checkPermission()) {
                            startPositionUpdates();
                        }
                    } else {
                        Toast.makeText(this, R.string.please_enable_location,
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
            }
        }
    }

    public void getLastLocation() {
        FusedLocationProviderClient locationClient =
                LocationServices.getFusedLocationProviderClient(this);
        try {
            locationClient.getLastLocation()
                    .addOnSuccessListener((Location location) -> {
                            // GPS location can be null if GPS is switched off
                            if (location != null) {
                                if (isBetterLocation(location, mUserLocation))
                                {
                                    mUserLocation = location;
                                }
                            }
                        }
                    )
                    .addOnFailureListener((@NonNull Exception e) -> {
                            Log.d("MapDemoActivity", "Error trying to get last GPS location");
                            e.printStackTrace();
                        }
                    );
        } catch (SecurityException e) { e.printStackTrace(); }
    }

    private void startPositionUpdates() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                    0, this);
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0,
                    0, this);
        }
    }

    private void stopPositionUpdates() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED

                ||
            ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

            mLocationManager.removeUpdates(this);
        }
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapActivity.this,
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    private void activateLocationSettings() {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(this, (LocationSettingsResponse locationSettingsResponse) ->
            onLocationSettingsSatisfied()
        );

        task.addOnFailureListener(this, (@NonNull Exception e) ->
            onLocationSettingsNotSatisfied(e)
        );
    }

    private void centerCameraLocation(LatLng position) {
        if (position != null) {
            if (mMap != null) {
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(position, ZOOM);
                mMap.animateCamera(cameraUpdate);
            }
            else {
                Log.i(TAG, "mMap is null when trying to center in user's location");
            }
        }
        else {
            Log.i(TAG, "Trying to center without knowing user current position");
        }
    }

    private void requestLocationFromGpsCoordinates() {
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(mPos.latitude, mPos.longitude, 10);
        } catch (IOException e) {
            Log.e(TAG, "Exception when retrieving geocode info", e);
        }

        for (Address adr : addresses) {
            if (adr.getLocality() != null && adr.getLocality().length() > 0) {
                mCity =  adr.getLocality();
                break;
            }
        }

        // let's try now with 'sublocality'
        if (mCity == null) {
            for (Address adr : addresses) {
                if (adr.getSubLocality() != null && adr.getSubLocality().length() > 0) {
                    mCity =  adr.getSubLocality();
                    break;
                }
            }
        }

        // let's try now with 'sub admin area'
        if (mCity == null) {
            for (Address adr : addresses) {
                if (adr.getSubAdminArea() != null && adr.getSubAdminArea().length() > 0) {
                    mCity =  adr.getSubAdminArea();
                    break;
                }
            }
        }

        if (mCity == null) {
            Log.e(TAG, "City name couldn't be retrieved");
        }
    }

    private void setMarkerInCenterScreen() {
        if (mMap != null) {
            mPos = mMap.getCameraPosition().target;
            requestLocationFromGpsCoordinates();
            setMarker();
        }
    }

    private void setMarker() {
        if (mPos != null) {
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(mPos));
            drawCircle(mSelectedRadiusKm);
        }
    }

    private boolean checkPermission() {
        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION);

        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED
                &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.btn_ok_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_ok:


                // TODO , check if mCity is != null here. If not, tell the user to pick a place

                Intent i = getIntent();
                i.putExtra(RESULT_POS_TEXT, mCity);
                i.putExtra(RESULT_POS_LAT, mPos.latitude);
                i.putExtra(RESULT_POS_LON, mPos.longitude);
                i.putExtra(RESULT_RADIUS, mSelectedRadiusKm);

                setResult(RESULT_OK, i);
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "New location " + location.getAccuracy());

        if (isBetterLocation(location, mUserLocation)) {
            mUserLocation = location;
            setMarker();
            mNumLocationUpdates++;
        }

        if (mNumLocationUpdates == 5) {
            stopPositionUpdates();
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        Log.d(TAG, "onStatusChanged [" + s + "] i[" + i + "]");
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int radiusKm, boolean b) {
        // SeekBar values go from 0 to 100
        mSelectedRadiusKm = (int)(((float)radiusKm / 100)*mRangeKm + mMinRadiusKm);
        mTextViewRadiusKm.setText(String.format("%s km", mSelectedRadiusKm));
        setMarker();
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void drawCircle(int radiusKm){
        if (!mComesFromProfileActivity)
        {
            CircleOptions circleOptions = new CircleOptions();
            circleOptions.center(mPos);
            circleOptions.radius(radiusKm * 1000);
            circleOptions.strokeColor(Color.BLACK);
            circleOptions.fillColor(getResources().getColor(R.color.colorMapCircle));
            circleOptions.strokeWidth(2);
            mMap.addCircle(circleOptions);
        }
    }
}
