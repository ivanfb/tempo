package com.bandesoft.tempo;

import android.view.View;

import com.bandesoft.tempo.Service.JabberService;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class BaseActivity extends AppCompatActivity {

    protected Toolbar initToolBar() {
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        myToolbar.setNavigationOnClickListener((View v) -> finish());

        return myToolbar; // in case some more changes are needed
    }

    @Override
    protected void onResume() {
        super.onResume();
        JabberService.resetUserActivityTimer();

        if (!JabberService.isServiceRunning(this)) {
            JabberService.startOnResume(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        JabberService.startUserActivityTimer();
    }
}
