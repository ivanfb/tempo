package com.bandesoft.tempo.Service;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.bandesoft.tempo.ChatActivity;
import com.bandesoft.tempo.Remote.AuthManager;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Utils.NotifsManager;
import com.bandesoft.tempo.Utils.Prefs;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.IOException;
import java.util.Map;

/**
 * This class manages the Firebase incoming messages. These messages
 * are used to notify the user that new XMPP messages are available to be read.
 */
public class MyMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyMessagingService";

    private static final int EVENT_EMAIL_VALIDATION = 0;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
//        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        Map<String, String> data = remoteMessage.getData();
        if (data.size() > 0) {
            Log.d(TAG, "StorableMessage data payload: " + data);

            if (data.containsKey("sender_id") &&
                    data.containsKey("sender_name") &&
                    data.containsKey("chat_topic")) {
                if (!ChatActivity.IsVisible) {
                    NotifsManager.showFirebaseNotif(this,
                            data.get("sender_name"),
                            data.get("sender_id"),
                            data.get("chat_topic"));
                }
            } else if (data.containsKey("event_id")) {
                int eventId = Integer.parseInt(data.get("event_id"));
                switch (eventId) {
                    case EVENT_EMAIL_VALIDATION:
                        Prefs.instance.init(getApplicationContext());
                        Prefs.instance.setEmailValidated(true);
                        NotifsManager.showEmailValidatedNotif(this);
                        break;
                }
            }
        }
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the
     * InstanceID token is initially generated so this is where you would retrieve
     * the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);

        Prefs.instance.init(getApplicationContext());
        Prefs.instance.setFirebaseToken(token);

        if (AuthManager.instance.isUserRegistered()) {
            Log.d(TAG, "Uploading token to firebase");
            HttpTempo.getInstance().uploadFirebaseToken(token);
        }
    }

    public static void initializeFirebase(Activity activity) {
        Prefs.instance.init(activity);

        // Enable FCM via enable Auto-init service which generate new token and receive in FCMService
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);

        FirebaseApp.initializeApp(activity);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(activity, instanceIdResult -> {

            String newToken = instanceIdResult.getToken();
            Prefs.instance.setFirebaseToken(newToken);
            HttpTempo.getInstance().uploadFirebaseToken(newToken);
        });

        activity.startService(new Intent(activity, MyMessagingService.class));
    }

    public static void disableFCM() {
        // Disable auto init
        FirebaseMessaging.getInstance().setAutoInitEnabled(false);

        try {
            // Remove InstanceID initiate to unsubscribe all topic
            // TODO: May be a better way to use FirebaseMessaging.getInstance().unsubscribeFromTopic()
            FirebaseInstanceId.getInstance().deleteInstanceId();

            if (AuthManager.instance.isUserLoggedIn()) {
                // clear our the token in backend
                HttpTempo.getInstance().uploadFirebaseToken("");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}