package com.bandesoft.tempo.Service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.bandesoft.tempo.R;
import com.bandesoft.tempo.Remote.AuthManager;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Remote.JabberConnection;
import com.bandesoft.tempo.Utils.NotifsManager;
import com.bandesoft.tempo.Utils.Prefs;

import java.util.Timer;
import java.util.TimerTask;

public class JabberService extends Service {
    public static final String STORE_PENDING_MESSAGES = "com.bandesoft.tempo.Ability.store_pending_messages";
    public static final String SEND_MESSAGE = "com.bandesoft.tempo.Ability.sendmessage";
    public static final String NEW_MESSAGE = "com.bandesoft.tempo.Ability.new_message";
    public static final String BUNDLE_CHAT_TOPIC = "b_chat_topic";
    public static final String BUNDLE_MESSAGE_TYPE = "b_message_type";
    public static final String BUNDLE_MESSAGE_BODY = "b_message_body";
    public static final String BUNDLE_FROM_JID = "b_from_jid";
    public static final String BUNDLE_TO = "b_to";
    public static final String BUNDLE_SENT_TIME = "b_sent_time";
    public static final String BUNDLE_SENDER_NAME = "b_sender_name";
    public static final String BUNDLE_INFO_HANDLED_IN_JABBER_CONN = "b_info_handled_in_jabber_connection";

    private static final String TAG = "JabberService";
    public static JabberConnection.ConnectionState sConnectionState;
    private Thread mThread;
    private Handler mTHandler; //We use this handler to post messages to
    private static JabberConnection mConnection;

    private static final long INACTIVITY_THRESHOLD_MILLIS = 60000;

    public static JabberService mInstance;

    private static Timer mTimer;

    private Looper mThreadLooper;

    public static void startUserActivityTimer() {
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mInstance != null) {
                    Log.d(TAG, "calling disconnect due to inactivity");
                    mInstance.stopDisconnect();
                    mInstance = null;
                }
            }
        }, INACTIVITY_THRESHOLD_MILLIS);
    }

    public static void resetUserActivityTimer() {
        if (mTimer != null) {
            Log.d(TAG, "resetting Activity Timer");
            mTimer.cancel();
            mTimer.purge();
            mTimer = null;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private void establishConnection() {
        Log.d(TAG, "establishConnection");

        if (mThread == null) {
            mThread = new Thread(() -> {

                // Check if this thread doesn't have a previous looper
                if (Looper.myLooper() == null) {

                    Looper.prepare(); // make the thread to get a Looper and a MessageQueue
                    mThreadLooper = Looper.myLooper();

                    if (mTHandler == null) {
                        mTHandler = new Handler(); // gets implicity associated with the thread via the Looper
                    }

                    AuthManager.instance.init(getApplicationContext());
                    boolean lWasUserInJabber = Prefs.instance.isUserRegisteredInJabber();
                    try {
//                        Toast.makeText(this, "Before creating connection", Toast.LENGTH_SHORT).show();
                        if (mConnection == null) {
                            mConnection = new JabberConnection(this);
                        }

                        if (!mConnection.isAuthenticated()) {
//                            Toast.makeText(this, "before connecting", Toast.LENGTH_SHORT).show();
                            mConnection.connect();
                        }

                    } catch (Exception e) {
                        Toast.makeText(this,
                                getString(R.string.error_connecting_to_server),
                                Toast.LENGTH_LONG).show();
                        Log.e(TAG, "Something went wrong while connecting, " +
                                "make sure the credentials are right and try again " + e.getMessage());
                        e.printStackTrace();

                        Context context = getApplicationContext();
                        HttpTempo.getInstance().sendUserError(e.getMessage(),
                                () -> NotifsManager.showErrorNotif(context, e.getMessage()));

                        if (!lWasUserInJabber) { // the user was trying to register for the first time
                            AuthManager.instance.setJabberRegisterFailed(context);
                            // TODO pending to remove the xmpp account if any
                        }
                    }

                    Looper.loop();
                }
            });
        }

        if (!mThread.isAlive()) {
            mThread.setName("Thread_JabberService");
            mThread.start();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");

        JabberService.mInstance = this;
        establishConnection();

        return Service.START_STICKY;
        //RETURNING START_STICKY CAUSES CODE TO STICK AROUND WHEN THE APP ACTIVITY HAS DIED.
    }

    // This will be fired only if stopWithTask is set to false in manifest file
    // For example, this is called after the user kills the app
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d(TAG, "onTaskRemoved");
        stopDisconnect();
    }

    private void stopDisconnect() {
        if (mThread!= null && mThread.isAlive()) {
            if (mTHandler != null) {
                mTHandler.post(() -> { // a "runnable message" is sent here
                    if (mConnection != null) {
                        Log.d(TAG, "Before calling disconnect");

                        mConnection.disconnect();
                        mConnection = null;
                    }

                    if (mThreadLooper != null) {
                        mThreadLooper.quit(); // exit from Looper.loop()
                        mThreadLooper = null;
                    }
                });
            }
        }

        mTimer = null;
        mThread = null;

        stopSelf();
    }

    public static boolean isServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager)
                context.getSystemService(Context.ACTIVITY_SERVICE);

        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service :
                    manager.getRunningServices(Integer.MAX_VALUE)) {
                if (JabberService.class.getName()
                        .equals(service.service.getClassName())) {
                    return true;
                }
            }
        }

        return false;
    }

    public static void startOnResume(Context context) {
        AuthManager.instance.init(context.getApplicationContext());
        if (AuthManager.instance.isUserRegistered()) {
            Log.d(TAG, "starting service OnResume");
            context.startService(new Intent(context, JabberService.class));
        }
    }

    public static void start(Context context) {
        context.startService(new Intent(context, JabberService.class));
    }

    // TODO : test this....
    public static void stopServiceAndRemoveAccount() {
        Log.d(TAG, "stopServiceAndRemoveAccount");

        stopService();

        if (mConnection != null) {
            if (mConnection.removeAccount()) {
                mConnection.disconnect();
                mConnection = null;
            }
        }
    }

    public static void stopService() {
        if (mInstance != null) {
            mInstance.stopDisconnect();
            mInstance = null;
        }
    }
}
