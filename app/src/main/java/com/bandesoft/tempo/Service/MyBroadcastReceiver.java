package com.bandesoft.tempo.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "BootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        switch (intent.getAction()) {
            default: // We consider "ACTION_SHUTDOWN" and "QUICKBOOT_POWEROFF"
                Log.d(TAG, "Stopping service poweroff or shutdown");
                JabberService.stopService();
                break;
        }

    }
}
