package com.bandesoft.tempo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bandesoft.tempo.Model.Service;
import com.bandesoft.tempo.Model.Opinion;
import com.bandesoft.tempo.Model.SQL;
import com.bandesoft.tempo.Model.Worker;
import com.bandesoft.tempo.Model.WorkerViewModel;
import com.bandesoft.tempo.Remote.AuthManager;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Utils.AlertDialogBuilder;
import com.bandesoft.tempo.Utils.Ctes;
import com.bandesoft.tempo.Utils.DynamicLinksMngr;
import com.bandesoft.tempo.Utils.GlideApp;
import com.bandesoft.tempo.Utils.ImgLoader;
import com.bandesoft.tempo.Vista.OpinionsList;
import com.bandesoft.tempo.Vista.WorkerAbilityList;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProvider;

public class WorkerActivity extends BaseActivity
        implements View.OnClickListener {

    private static final String TAG = "WorkerActivity";

    public static String WORKER = "worker";
    public static String WORKER_PIC = "worker_pic.jpeg";
    public static String WORKER_PIC_PASSED = "worker_pic_passed";

    public static final String URL_DEEP_LINK = "URL_DEEP_LINK";

    public static final String EXTRA_ORIGIN = "extra_origin";
    public static final int EXT_ORIGIN_NEEDED_SERVICES = 0;
    public static final int EXT_ORIGIN_OFFERED_SERVICES = 1;
    public static final int EXT_ORIGIN_CONTACT = 2;
    public static final int EXT_ORIGIN_DEEP_LINK = 3;

    private int mOrigin;

    private WorkerViewModel mViewModel;
    private ListView mListOpinions;
    private ListView mListGivenOpinions;

    private SQL mSQL;

    private LinearLayout mWorkerInfoContainer;

    private Bitmap mWorkerBmp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSQL = SQL.getInstance(getApplicationContext());

        setContentView(R.layout.activity_worker);

        mListOpinions = findViewById(R.id.worker_opinions);
        mListGivenOpinions = findViewById(R.id.worker_given_opinions);

        Toolbar myToolbar = super.initToolBar();

        NestedScrollView nestedScrollView = findViewById(R.id.scroll_view);
        nestedScrollView.setOnScrollChangeListener(
                (NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) -> {
                    if (mWorkerInfoContainer != null)
                    {
                        int lHalfWorkerInfoHeight = mWorkerInfoContainer.getHeight() / 2;
                        if (oldScrollY < lHalfWorkerInfoHeight && scrollY >= lHalfWorkerInfoHeight)
                        {
                            if (mWorkerBmp != null) {
                                myToolbar.setLogo(new BitmapDrawable(getResources(),
                                                  ImgLoader.getCroppedBitmap(mWorkerBmp)));
                            }

                            myToolbar.setTitle(" " + mViewModel.getWorkerName());
                        }
                        else if (scrollY < lHalfWorkerInfoHeight && oldScrollY >= lHalfWorkerInfoHeight)
                        {
                            myToolbar.setLogo(null);
                            myToolbar.setTitle(getString(R.string.app_name));
                        }
                    }
                }
        );

        mWorkerInfoContainer = findViewById(R.id.worker_info_linear_layout);

        mViewModel = new ViewModelProvider(this).get(WorkerViewModel.class);

        Intent intent = getIntent();
        mOrigin = intent.getIntExtra(EXTRA_ORIGIN, EXT_ORIGIN_CONTACT);

        Worker lWorker = (Worker) intent.getSerializableExtra(WORKER);
        if (lWorker != null) {

            ImageView lImgView = findViewById(R.id.worker_pic);
            if (intent.getBooleanExtra(WORKER_PIC_PASSED, false)) {
                mWorkerBmp = ImgLoader.getBitmap(this, WORKER_PIC);
                lImgView.setImageBitmap(mWorkerBmp);
            }

            lImgView.setOnClickListener(v -> {
                Intent i = new Intent(this, PicsActivity.class);
                i.putExtra(PicsActivity.UID, mViewModel.getUID());
                i.putExtra(PicsActivity.PICS_CATEGORY, PicsActivity.OTHER_USER_PICS);
                i.putExtra(PicsActivity.WORKER_NAME, mViewModel.getWorkerName());
                startActivity(i);
            });

            setViewModelListeners();
            mViewModel.setWorker(lWorker);
            mViewModel.requestOpinions(this, lWorker.getUid());

            ImgLoader.downloadUserPicIntoView(this, mViewModel.getUID(), lImgView,
                                              mViewModel.getLastDateImageChanged());
        }
        else {
            String lDeepLink = intent.getStringExtra(URL_DEEP_LINK);
            if (lDeepLink != null && !lDeepLink.isEmpty()) {
                handleDeepLink(lDeepLink);
            }
        }
    }

    private void handleDeepLink(String lDeepLink) {
        try {
            String lDecodedDeepLink = java.net.URLDecoder.decode(lDeepLink, "UTF-8");

            String lUrl = lDecodedDeepLink.substring(lDecodedDeepLink.indexOf('?') + 1);

            Map<String, String> lQueryParams = DynamicLinksMngr.getQueryMap(lUrl);

            if (lQueryParams.containsKey(DynamicLinksMngr.QUERY_PARAM_UID)) {

                String uid = lQueryParams.get(DynamicLinksMngr.QUERY_PARAM_UID);

                setViewModelListeners();
                mViewModel.requestAllUserInfo(this, uid);

                String lImgUrl = HttpTempo.URL_DOWNLOAD_IMG + uid + ".jpeg";

                try {
                    ImageView lImgView = findViewById(R.id.worker_pic);

                    RequestOptions lRequestOptions = new RequestOptions();
                    lRequestOptions.placeholder(R.drawable.ic_default_profile_pic);
                    lRequestOptions.error(R.drawable.ic_default_profile_pic);

                    GlideApp.with(this)
                            .setDefaultRequestOptions(lRequestOptions)
                            .load(lImgUrl)
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    // log exception
//                            Log.e(TAG, "Error loading image", e);
                                    return false; // important to return false so the error placeholder can be placed
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource,
                                                               Object model,
                                                               Target<Drawable> target,
                                                               DataSource dataSource,
                                                               boolean isFirstResource) {
                                    return false;
                                }
                            })
                            .fitCenter()
                            .into(lImgView);

                } catch (Exception e) {
                    Log.e(TAG, "Exception using Glide " + e.getMessage());
                }
            }
        }
        catch (UnsupportedEncodingException e) {

            Log.d(TAG, "handleDeepLink 9");
            Log.e(TAG, "Error decoding " + lDeepLink + " " + e.getMessage());
        }
    }

    private void setViewModelListeners() {
        mViewModel.getWorker().observe(this, worker -> {

            TextView lWorkerAbilitiesTitle = findViewById(R.id.services_title);
            TextView lWorkerNeedsTitle = findViewById(R.id.needs_title);

            ArrayList<Service> abilities = worker.getAbilities();
            ListView lWorkerAbilities = findViewById(R.id.worker_abilites);
            lWorkerAbilities.setAdapter(new WorkerAbilityList(this, abilities));

            ArrayList<Service> needs = worker.getNeeds();
            ListView lWorkerNeeds = findViewById(R.id.worker_needs);
            lWorkerNeeds.setAdapter(new WorkerAbilityList(this, needs));

            if (mOrigin == EXT_ORIGIN_NEEDED_SERVICES || abilities.size() == 0) {
                lWorkerAbilities.setVisibility(View.GONE);
                lWorkerAbilitiesTitle.setVisibility(View.GONE);
            }

            if (mOrigin == EXT_ORIGIN_OFFERED_SERVICES || needs.size() == 0) {
                lWorkerNeeds.setVisibility(View.GONE);
                lWorkerNeedsTitle.setVisibility(View.GONE);
            }

            TextView lWorkerLocation = findViewById(R.id.worker_location);
            lWorkerLocation.setText(worker.getLocation().getCityName());

            TextView lWorkerName = findViewById(R.id.worker_name);
            lWorkerName.setText(worker.getName());

            int numVotes = worker.getNumVotes();
            if (numVotes > 0) {
                LinearLayout lAvgMarkContainer = findViewById(R.id.avg_mark_container);
                lAvgMarkContainer.setVisibility(View.VISIBLE);

                RatingBar lAvgMark = findViewById(R.id.avg_mark);
                lAvgMark.setRating(worker.getMark());

                TextView lTxtNumVotes = findViewById(R.id.txt_num_votes);
                lTxtNumVotes.setText("(" + numVotes + ")");
            }
        });

        mViewModel.getGivenOpinions().observe(this, opinions ->
            setOpinionsAdapter(opinions,
                               mListGivenOpinions,
                               R.id.given_opinions_title,
                               R.id.btn_show_more_given_feedbacks)
        );

        mViewModel.getOpinions().observe(this, opinions ->
            setOpinionsAdapter(opinions,
                               mListOpinions,
                               R.id.feedbacks_title,
                               R.id.btn_show_more_received_feedbacks)
        );
    }

    private void setOpinionsAdapter(List<Opinion> opinions,
                                    ListView listView,
                                    int idFeedbacksTitle,
                                    int idBtnShowMore) {
        List<Opinion> ops;

        if (opinions.size() > 2) {
            findViewById(idBtnShowMore).setVisibility(View.VISIBLE);
            ops = opinions.subList(0, 2); // only two items
        }
        else {
            ops = opinions;
        }

        listView.setAdapter(new OpinionsList(this, ops));

        if(opinions.size() > 0) {
            findViewById(idFeedbacksTitle).setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.worker_action_bar, menu);

        if (mOrigin == EXT_ORIGIN_CONTACT) {
            MenuItem lFavouriteHeart = menu.findItem(R.id.menu_favourite);
            lFavouriteHeart.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_item_share:
                DynamicLinksMngr.shortenLink(this,
                                              mViewModel.getUID(),
                        lUriToShare -> {
                            Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                            whatsappIntent.setType("text/plain");
                            whatsappIntent.setPackage("com.whatsapp");
                            whatsappIntent.putExtra(Intent.EXTRA_TEXT,
                                    getString(R.string.i_think_this_could_be_interesting) +
                                            "\n" +
                                            lUriToShare);
                            try {
                                startActivity(whatsappIntent);
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(this,
                                        getString(R.string.please_install_whatsapp),
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                break;

            case R.id.btn_send_message:
            case R.id.menu_favourite:
                AuthManager.instance.askUserForSigningUpIfNeeded(this,
                        () -> {
                            ArrayList<Service> list = null;

                            switch (mOrigin) {
                                case EXT_ORIGIN_NEEDED_SERVICES:
                                    list = mViewModel.getNeeds();
                                    break;

                                case EXT_ORIGIN_OFFERED_SERVICES:
                                    list = mViewModel.getAbilities();
                                    break;

                                case EXT_ORIGIN_CONTACT:
                                    list = new ArrayList<>();
                                    list.addAll(mViewModel.getAbilities());
                                    list.addAll(mViewModel.getNeeds());
                                    break;
                            }

                            if (list != null && list.size() > 1)
                            {
                                ArrayList<String> lWAbilitiesString = new ArrayList<>(list.size());
                                for (Service a : list) {
                                    String category =
                                            Ctes.translateCategoryFromTag(a.getCategory());
                                    if (!lWAbilitiesString.contains(category)) {
                                        lWAbilitiesString.add(category);
                                    }
                                }

                                View view = AlertDialogBuilder.inflateSpecificLayout(this,
                                        R.layout.dialog_select_service_for_chat,
                                        true,
                                        v -> {
                                            Spinner lServices = v.findViewById(R.id.services_list);
                                            startChatActivity_Or_SaveContact(
                                                    R.id.btn_send_message == item.getItemId(),
                                                    lServices.getSelectedItem().toString());
                                        });

                                Spinner lServices = view.findViewById(R.id.services_list);

                                ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                                        android.R.layout.simple_spinner_item, lWAbilitiesString);
                                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                lServices.setAdapter(dataAdapter);
                            }
                            else {
                                String lCategory = "";
                                if (list != null && list.size() == 1) {
                                    lCategory = Ctes.translateCategoryFromTag(list.get(0).getCategory());
                                }

                                startChatActivity_Or_SaveContact(
                                        R.id.btn_send_message == item.getItemId(),
                                        lCategory);
                            }
                        });
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startChatActivity_Or_SaveContact(boolean startChat, String serviceName) {
        boolean lIsServiceANeed = false;
        for (Service s : mViewModel.getNeeds()) {
            if (Ctes.translateCategoryFromTag(s.getCategory()).compareTo(serviceName) == 0) {
                lIsServiceANeed = true;
                break;
            }
        }

        if (lIsServiceANeed) {
            serviceName = "needs:" + serviceName;
        }
        else {
            serviceName = "offers:" + serviceName;
        }

        if (startChat) {
            Intent intent = new Intent(WorkerActivity.this, ChatActivity.class);
            intent.putExtra(ChatActivity.STR_EXTR_CONTACT_UID, mViewModel.getUID());
            intent.putExtra(ChatActivity.STR_EXTR_CONTACT_NAME, mViewModel.getWorkerName());
            intent.putExtra(ChatActivity.STR_EXTR_SERVICE_NAME, serviceName);
            startActivity(intent);
        }
        else {
            mSQL.setChatTopic(mViewModel.getUID(), serviceName, true);
            mSQL.storeContactWithJustName(mViewModel.getUID(), mViewModel.getWorkerName());

            Toast.makeText(this,
                    getString(R.string.added_as_contact),
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        boolean lbReceivedOpinions = view.getId() == R.id.btn_show_more_received_feedbacks;

        ArrayList<Opinion> lOpinions = lbReceivedOpinions ?
                mViewModel.getOpinions().getValue() :
                mViewModel.getGivenOpinions().getValue();

        Intent i = new Intent(this, OpinionsActivity.class);
        i.putExtra(OpinionsActivity.RECEIVED_OPINIONS, lbReceivedOpinions);
        i.putParcelableArrayListExtra(OpinionsActivity.OPINIONS_LIST, lOpinions);
        i.putExtra(OpinionsActivity.USER_NAME, mViewModel.getWorkerName());
        startActivity(i);
    }
}
