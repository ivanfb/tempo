package com.bandesoft.tempo.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;

import com.bandesoft.tempo.MainActivity;
import com.bandesoft.tempo.R;

import androidx.core.app.NotificationCompat;

public class NotifsManager {

    public static final String OPENED_FROM_NOTIFICATION = "opened_from_notification";
    public static final String SENDER_ID = "opened_from_notification_sender_id";
    public static final String SENDER_NAME = "opened_from_notification_sender_name";
    public static final String CHAT_TOPIC = "opened_from_notification_chat_topic";

    private static final String TAG = "NotifsManager";

    private static final int NOTIF_ID_EMAIL_VALIDATED = 0;

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    public static void showFirebaseNotif(Context context,
                                         String senderName,
                                         String senderId,
                                         String chatTopic)
    {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0 /* Request code */,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, "default")
                        .setSmallIcon(R.mipmap.ic_notification_white)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                                R.mipmap.ic_launcher))
                        .setColor(context.getResources().getColor(R.color.colorBlueCool))
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(context.getString(R.string.incoming_message) + " " +
                                senderName != null ? senderName: "")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager == null) {
            Log.e(TAG, "Notification manager is null");
            return;
        }

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default",
                    "default",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);
        }

        Notification notif = notificationBuilder.build();

        Intent notificationIntent = new Intent(context, MainActivity.class);

        notificationIntent.putExtra(OPENED_FROM_NOTIFICATION, true);
        notificationIntent.putExtra(SENDER_ID, senderId);
        notificationIntent.putExtra(SENDER_NAME, senderName);
        notificationIntent.putExtra(CHAT_TOPIC, chatTopic);

        notif.contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationManager.notify(/*notif id*/ Integer.valueOf(senderId), notif);
    }

    public static void showEmailValidatedNotif(Context context)
    {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0 /* Request code */,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, "default")
                        .setSmallIcon(R.mipmap.ic_notification_white)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                                R.mipmap.ic_launcher))
                        .setColor(context.getResources().getColor(R.color.colorBlueCool))
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(context.getString(R.string.email_account_validated))
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager == null) {
            Log.e(TAG, "Notification manager is null");
            return;
        }

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default",
                    "default",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);
        }

        Notification notif = notificationBuilder.build();
        notificationManager.notify(NOTIF_ID_EMAIL_VALIDATED, notif);
    }

    public static void showErrorNotif(Context context, String message) {

        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0 /* Request code */,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(context, "default")
                        .setSmallIcon(R.mipmap.ic_notification_white)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                                R.mipmap.ic_launcher))
                        .setColor(context.getResources().getColor(R.color.colorBlueCool))
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(context.getString(R.string.sorry_error_occured))
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager == null) {
            Log.e(TAG, "Notification manager is null");
            return;
        }

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default",
                    "default",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);
        }

        Notification notif = notificationBuilder.build();
        notificationManager.notify(NOTIF_ID_EMAIL_VALIDATED, notif);

    }

    /**
     * This is indendet to be called when the ContactsFragment is shown
     * @param context
     */
    public static void clearNotifs(Context context) {
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
    }
}
