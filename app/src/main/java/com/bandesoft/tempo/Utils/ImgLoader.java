package com.bandesoft.tempo.Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bandesoft.tempo.R;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.ObjectKey;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;

import androidx.annotation.Nullable;

public class ImgLoader extends AsyncTask<InputStream, Void, Bitmap> {
    private static final String TAG = "ImgLoader";

    private final WeakReference<ImageView> mImgRef;

    private AsyncResponse mAsyncResponse;

    public interface I_OnCompletedHandler {
        void completed();
    }

    public interface AsyncResponse {
        void processFinish(Bitmap outBmp);
    }

    private ImgLoader(ImageView imgView, AsyncResponse asyncResp) {
        mImgRef = imgView != null ? new WeakReference<>(imgView) : null;
        mAsyncResponse = asyncResp;
    }

    public static boolean saveImageToInternalStorage(Context context,
                                                     Bitmap image,
                                                     String fileName) {
        try {
// Use the compress method on the Bitmap object to write image to
// the OutputStream
            FileOutputStream fos = context.openFileOutput(fileName,
                    Context.MODE_PRIVATE);

// Writing the bitmap to the output stream
            image.compress(Bitmap.CompressFormat.JPEG, 65, fos);
            fos.close();

            return true;
        } catch (Exception e) {
            Log.e("saveToInternalStorage()", e.getMessage());
            return false;
        }
    }

    @Override
    protected Bitmap doInBackground(InputStream... iss) {
        return BitmapFactory.decodeStream(iss[0]);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }

        if (mImgRef != null) {
            ImageView imgView = mImgRef.get();
            if (imgView != null) {

                if (bitmap != null) { // the bitmap was decoded correctly
                    imgView.setImageBitmap(bitmap);
                }
                else { // try to set some default pic
                    Drawable defaultPic = imgView.getContext()
                            .getResources().getDrawable(R.drawable.ic_default_profile_pic);
                    imgView.setImageDrawable(defaultPic);
                }
            }
        }

        if (mAsyncResponse != null) {
            mAsyncResponse.processFinish(bitmap);
        }
    }

    public static Bitmap getBitmap(Context context, String fileName) {
        try {
            InputStream is = context.openFileInput(fileName);
            return BitmapFactory.decodeStream(is);
        }
        catch (FileNotFoundException fne) {
            Log.e(TAG, "getBitmap file not found" + fne.getMessage());
            return null;
        }
    }

    public static Bitmap getCroppedBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                bitmap.getWidth() / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    public static void downloadFromUrl(Context context,
                                       String lImgUrl,
                                       CustomTarget<Bitmap> targetBmp,
                                       String lastDateImgModified)
    {
        try {
            GlideApp.with(context)
                    .asBitmap()
                    .load(lImgUrl)
                    .placeholder(R.drawable.ic_default_profile_pic)
                    .signature(new ObjectKey(lastDateImgModified))
                    .into(targetBmp);
        } catch (Exception e) {
            Log.e(TAG, "Exception using Glide " + e.getMessage());
        }
    }

    public static void downloadFromURL(Context context,
                                        String lImgUrl,
                                        ImageView v,
                                        String lastDateImgModified) {
        try {
            GlideApp.with(context)
                    .load(lImgUrl)
                    .placeholder(R.drawable.ic_default_profile_pic)
                    .signature(new ObjectKey(lastDateImgModified))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e,
                                                    Object model,
                                                    Target<Drawable> target,
                                                    boolean isFirstResource) {
                            // log exception
//                            Log.e("TAG", "Error loading image", e);
                            return false; // important to return false so the error placeholder can be placed
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource,
                                                       Object model,
                                                       Target<Drawable> target,
                                                       DataSource dataSource,
                                                       boolean isFirstResource) {
                            v.setVisibility(View.VISIBLE);
                            return false;
                        }
                    })
                    .fitCenter()
                    .into(v);
        } catch (Exception e) {
            Log.e(TAG, "Exception using Glide " + e.getMessage());
        }
    }

    public static void downloadUserPicIntoView(Context context,
                                               String lUID,
                                               ImageView v,
                                               String lastDateImgModified) {
        String lImgUrl = HttpTempo.URL_DOWNLOAD_IMG + lUID + ".jpeg";
        downloadFromURL(context, lImgUrl, v, lastDateImgModified);
    }

    public interface I_onDownloadCompleted {
        void action();
    }

    public static void downloadMyPicIntoUserPicFile(Activity activity,
                                                    String lUID,
                                                    I_onDownloadCompleted onDownloadCompleted) {
        ThreadPool.instance.runTask(() -> {

            String lImgUrl = HttpTempo.URL_DOWNLOAD_IMG + lUID + ".jpeg";

            try {
                URL u = new URL(lImgUrl);
                URLConnection conn = u.openConnection();
                int contentLength = conn.getContentLength();

                DataInputStream stream = new DataInputStream(u.openStream());

                byte[] buffer = new byte[contentLength];
                stream.readFully(buffer);
                stream.close();

                OutputStream os = activity.openFileOutput(
                        activity.getString(R.string.my_profile_pic_name),
                        Context.MODE_PRIVATE);

                DataOutputStream fos = new DataOutputStream(os);

                fos.write(buffer);
                fos.flush();
                fos.close();
            }
            catch (Exception e) {
                Log.e(TAG, "Exception " + e.getMessage());
            }
            finally {
                onDownloadCompleted.action();
            }
        });
    }

    public static void downloadPic(Context context,
                                   String lPic_GCS_URL,
                                   I_OnCompletedHandler onCompletedHandler) {
        ThreadPool.instance.runTask(() -> {

            try {
                String fileName = lPic_GCS_URL.substring(lPic_GCS_URL.lastIndexOf("/") + 1, lPic_GCS_URL.lastIndexOf(".jpeg"));

                String lImgUrl = HttpTempo.URL_DOWNLOAD_IMG + fileName + ".jpeg";

                URL u = new URL(lImgUrl);
                URLConnection conn = u.openConnection();
                int contentLength = conn.getContentLength();

                DataInputStream stream = new DataInputStream(u.openStream());

                byte[] buffer = new byte[contentLength];
                stream.readFully(buffer);
                stream.close();

                OutputStream os = context.openFileOutput(
                        fileName, Context.MODE_PRIVATE);

                DataOutputStream fos = new DataOutputStream(os);

                fos.write(buffer);
                fos.flush();
                fos.close();

                onCompletedHandler.completed();
            }
            catch (Exception e) {
                Log.e(TAG, "Exception " + e.getMessage());
            }
        });
    }

    public static boolean loadPic(ImageView imageView,
                               Context context, Object picSource, AsyncResponse asyncResp) {
        try {
            InputStream is = null;
            if (picSource instanceof Uri) {
                is = context.getContentResolver().openInputStream((Uri) picSource);
            }
            else if (picSource instanceof String) {
                is = context.openFileInput((String) picSource);
            }
            new ImgLoader(imageView, asyncResp).execute(is);
        } catch (FileNotFoundException fne) {
            Log.e(TAG, "getBitmap file not found" + fne.getMessage());
            return false;
        } catch (IllegalStateException ise) {
            // That exc might occur if user goes to "NeededServicesFragment" and rapidly
            // goes to "OfferedServicesFragment", for instance.
            Log.e(TAG, "Maybe fragment not attached to activity" + ise.getMessage());
            return false;
        }

        return true;
    }

    public static void deleteMyPic(Context context, String picName) {
        context.deleteFile(picName);
    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}
