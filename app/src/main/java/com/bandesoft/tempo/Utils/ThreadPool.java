package com.bandesoft.tempo.Utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPool {


    private ExecutorService mExecService;

    public static ThreadPool instance = new ThreadPool();

    private ThreadPool() {
        /* Creates a new thread when there is a task in the queue.
         * When there is no tasks in the queue for 60 seconds, the idle threads
         * will be terminated. */
        mExecService = Executors.newCachedThreadPool();
    }

    public void runTask(Runnable task) {
        mExecService.execute(task);
    }
}
