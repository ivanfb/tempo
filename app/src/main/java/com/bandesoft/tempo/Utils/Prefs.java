package com.bandesoft.tempo.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.bandesoft.tempo.Model.LonLatCity;
import com.bandesoft.tempo.R;
import com.bandesoft.tempo.Remote.AuthManager;

import java.util.Calendar;
import java.util.Date;

public class Prefs {
    public static final int REQUEST_CHECK_SETTINGS = 0;


    // User ids
    private static final String PREF_USER_ID = "pref_user_id";
    private static final String PREF_IS_USER_REGISTERED_IN_JABBER = "pref_is_user_reg_in_jabber";

    // Firebase
    private static final String PREF_FIREBASE_TOKEN = "firebase_token";

    // Other
    private static final String PREF_EMAIL_ADDRESS_VALIDATED = "pref_email_address_validated";
    private static final String PREF_MY_LAST_ACCESS_TIME = "pref_my_last_access_time";

    // Search needs or offers
    private static final String PREF_SEARCH_NEEDED_OR_OFFERED = "pref_search_needs_or_offered";
    private static final int OFFERED_SEARCH_TYPE = 0;
    private static final int NEEDED_SEARCH_TYPE = 1;

    // offered
    private static final String PREF_SEARCH_CITY_OFFERED = "pref_search_city_offered_services";
    private static final String PREF_SEARCH_LAT_OFFERED = "pref_search_lat_offered_services";
    private static final String PREF_SEARCH_LON_OFFERED = "pref_search_lon_offered_services";
    private static final String PREF_SEARCH_RADIUS_KM_OFFERED = "pref_search_radius_km_offered_services";
    private static final String PREF_SEARCH_CATEGORY_OFFERED = "pref_search_category_offered_services";
    private static final String PREF_SEARCH_TAG_CATEGORY_OFFERED = "pref_search_tag_category_offered_services";

    // needed
    private static final String PREF_SEARCH_CITY_NEEDED = "pref_search_city_needed_services";
    private static final String PREF_SEARCH_LAT_NEEDED = "pref_search_lat_needed_services";
    private static final String PREF_SEARCH_LON_NEEDED = "pref_search_lon_needed_services";
    private static final String PREF_SEARCH_RADIUS_KM_NEEDED = "pref_search_radius_km_needed_services";
    private static final String PREF_SEARCH_CATEGORY_NEEDED = "pref_search_category_needed_services";
    private static final String PREF_SEARCH_TAG_CATEGORY_NEEDED = "pref_search_tag_category_needed_services";

    private SharedPreferences mMyPrefs;

    private LonLatCity mSearchLocation = new LonLatCity();

    public static Prefs instance = new Prefs();

    private Context mContext;

    private Prefs() {
    }

    public void init(Context context) {
        mMyPrefs = context.getSharedPreferences(context.getString(R.string.my_prefs_file), Context.MODE_PRIVATE);
        mContext = context;
    }

    public void setSearchLocation(double lat, double lon, String city, int radiusKm) {
        SharedPreferences.Editor editor = mMyPrefs.edit();

        if (isSearchOfferedServicesActive()) {
            editor.putFloat(PREF_SEARCH_LAT_OFFERED, (float) lat);
            editor.putFloat(PREF_SEARCH_LON_OFFERED, (float) lon);
            editor.putString(PREF_SEARCH_CITY_OFFERED, city);
            editor.putInt(PREF_SEARCH_RADIUS_KM_OFFERED, radiusKm);
        }
        else {
            editor.putFloat(PREF_SEARCH_LAT_NEEDED, (float) lat);
            editor.putFloat(PREF_SEARCH_LON_NEEDED, (float) lon);
            editor.putString(PREF_SEARCH_CITY_NEEDED, city);
            editor.putInt(PREF_SEARCH_RADIUS_KM_NEEDED, radiusKm);
        }

        editor.apply();
        editor.commit();
    }

    public LonLatCity getSearchLoc() {
        if (isSearchOfferedServicesActive()) {
            mSearchLocation.setLon(mMyPrefs.getFloat(PREF_SEARCH_LON_OFFERED, 0));
            mSearchLocation.setLat(mMyPrefs.getFloat(PREF_SEARCH_LAT_OFFERED, 0));
            mSearchLocation.setCityName(mMyPrefs.getString(PREF_SEARCH_CITY_OFFERED,
                    mContext.getString(R.string.location)));
            mSearchLocation.setRadiusKm(mMyPrefs.getInt(PREF_SEARCH_RADIUS_KM_OFFERED,
                    mContext.getResources().getInteger(R.integer.min_radius_km)));
        }
        else {
            mSearchLocation.setLon(mMyPrefs.getFloat(PREF_SEARCH_LON_NEEDED, 0));
            mSearchLocation.setLat(mMyPrefs.getFloat(PREF_SEARCH_LAT_NEEDED, 0));
            mSearchLocation.setCityName(mMyPrefs.getString(PREF_SEARCH_CITY_NEEDED,
                    mContext.getString(R.string.location)));
            mSearchLocation.setRadiusKm(mMyPrefs.getInt(PREF_SEARCH_RADIUS_KM_NEEDED,
                    mContext.getResources().getInteger(R.integer.min_radius_km)));
        }
        return mSearchLocation;
    }

    public void setSearchCategory(String category, String cat_tag) {
        SharedPreferences.Editor editor = mMyPrefs.edit();
        if (isSearchOfferedServicesActive()) {
            editor.putString(PREF_SEARCH_CATEGORY_OFFERED, category);
            editor.putString(PREF_SEARCH_TAG_CATEGORY_OFFERED, cat_tag);
        }
        else {
            editor.putString(PREF_SEARCH_CATEGORY_NEEDED, category);
            editor.putString(PREF_SEARCH_TAG_CATEGORY_NEEDED, cat_tag);
        }
        editor.apply();
        editor.commit();
    }

    public String getSearchCategory() {
        if (isSearchOfferedServicesActive()) {
            return mMyPrefs.getString(PREF_SEARCH_CATEGORY_OFFERED,
                    mContext.getString(R.string.category_whatever));
        }
        else {
            return mMyPrefs.getString(PREF_SEARCH_CATEGORY_NEEDED,
                    mContext.getString(R.string.category_whatever));
        }
    }

    public String getSearchTagCategory() {
        if (isSearchOfferedServicesActive()) {
            return mMyPrefs.getString(PREF_SEARCH_TAG_CATEGORY_OFFERED,
                    mContext.getString(R.string.tag_cat_any));
        }
        else {
            return mMyPrefs.getString(PREF_SEARCH_TAG_CATEGORY_NEEDED,
                    mContext.getString(R.string.tag_cat_any));
        }
    }

    public int getNumCurrentlyAppliedFilters() {
        int lRet = 0;

        if (getSearchTagCategory().compareTo(mContext.getString(R.string.tag_cat_any)) != 0) {
            lRet++;
        }

        String lUndefCity = mContext.getString(R.string.location);
        if (isSearchOfferedServicesActive()) {
            if (mMyPrefs.getString(PREF_SEARCH_CITY_OFFERED, lUndefCity).compareTo(lUndefCity) != 0) {
                lRet++;
            }
        }
        else {
            if (mMyPrefs.getString(PREF_SEARCH_CITY_NEEDED, lUndefCity).compareTo(lUndefCity) != 0) {
                lRet++;
            }
        }
        return lRet;
    }

    public void clearSearchData() {
        SharedPreferences.Editor editor = mMyPrefs.edit();
        if (isSearchOfferedServicesActive()) {
            editor.remove(PREF_SEARCH_TAG_CATEGORY_OFFERED);
            editor.remove(PREF_SEARCH_CATEGORY_OFFERED);
            editor.remove(PREF_SEARCH_CITY_OFFERED);
            editor.remove(PREF_SEARCH_LAT_OFFERED);
            editor.remove(PREF_SEARCH_LON_OFFERED);
            editor.remove(PREF_SEARCH_RADIUS_KM_OFFERED);
        }
        else {
            editor.remove(PREF_SEARCH_TAG_CATEGORY_NEEDED);
            editor.remove(PREF_SEARCH_CATEGORY_NEEDED);
            editor.remove(PREF_SEARCH_CITY_NEEDED);
            editor.remove(PREF_SEARCH_LAT_NEEDED);
            editor.remove(PREF_SEARCH_LON_NEEDED);
            editor.remove(PREF_SEARCH_RADIUS_KM_NEEDED);
        }
        editor.apply();
        editor.commit();
    }

    public void clearCategorySearchData() {
        SharedPreferences.Editor editor = mMyPrefs.edit();
        if (isSearchOfferedServicesActive()) {
            editor.remove(PREF_SEARCH_TAG_CATEGORY_OFFERED);
            editor.remove(PREF_SEARCH_CATEGORY_OFFERED);
        }
        else {
            editor.remove(PREF_SEARCH_TAG_CATEGORY_NEEDED);
            editor.remove(PREF_SEARCH_CATEGORY_NEEDED);
        }
        editor.apply();
        editor.commit();
    }

    public void clearLocationSearchData() {
        SharedPreferences.Editor editor = mMyPrefs.edit();
        if (isSearchOfferedServicesActive()) {
            editor.remove(PREF_SEARCH_CITY_OFFERED);
            editor.remove(PREF_SEARCH_LAT_OFFERED);
            editor.remove(PREF_SEARCH_LON_OFFERED);
            editor.remove(PREF_SEARCH_RADIUS_KM_OFFERED);
        }
        else {
            editor.remove(PREF_SEARCH_CITY_NEEDED);
            editor.remove(PREF_SEARCH_LAT_NEEDED);
            editor.remove(PREF_SEARCH_LON_NEEDED);
            editor.remove(PREF_SEARCH_RADIUS_KM_NEEDED);
        }
        editor.apply();
        editor.commit();
    }

    public boolean isEmailAddressValidated() {
        return mMyPrefs.getInt(PREF_EMAIL_ADDRESS_VALIDATED, 0) == 1;
    }

    public void swapNeedOffer() {
        int lCurrent = mMyPrefs.getInt(PREF_SEARCH_NEEDED_OR_OFFERED, OFFERED_SEARCH_TYPE);

        if (lCurrent == OFFERED_SEARCH_TYPE) {
            lCurrent = NEEDED_SEARCH_TYPE;
        }
        else {
            lCurrent = OFFERED_SEARCH_TYPE;
        }

        SharedPreferences.Editor editor = mMyPrefs.edit();
        editor.putInt(PREF_SEARCH_NEEDED_OR_OFFERED, lCurrent);
        editor.apply();
        editor.commit();
    }

    public boolean isSearchOfferedServicesActive() {
        return mMyPrefs.getInt(PREF_SEARCH_NEEDED_OR_OFFERED, OFFERED_SEARCH_TYPE) == OFFERED_SEARCH_TYPE;
    }

    public void setEmailValidated(boolean validated) {
        SharedPreferences.Editor editor = mMyPrefs.edit();
        editor.putInt(PREF_EMAIL_ADDRESS_VALIDATED, validated ? 1:0);
        editor.apply();
        editor.commit();
    }

    public void updateMyLastAccessTime() {
        Date date = new Date(System.currentTimeMillis());
        SharedPreferences.Editor editor = mMyPrefs.edit();
        editor.putLong(PREF_MY_LAST_ACCESS_TIME, date.getTime());
        editor.apply();
        editor.commit();
    }

    public Date getMyLastAccessTime() {
        Long timeMillis = mMyPrefs.getLong(PREF_MY_LAST_ACCESS_TIME, -1);
        if (timeMillis == -1) {
            final Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);
            return cal.getTime();
        }
        else {
            return new Date(timeMillis);
        }
    }

    public String getUserId() {
        return mMyPrefs.getString(PREF_USER_ID, AuthManager.INVALID_USER);
    }

    public void clear() {
        SharedPreferences.Editor editor = mMyPrefs.edit();
        editor.clear();
        editor.apply();
    }

    public void saveUserIdAndUserIsInJabber(String uid) {
        SharedPreferences.Editor editor = mMyPrefs.edit();
        editor.putBoolean(PREF_IS_USER_REGISTERED_IN_JABBER, true);
        editor.putString(PREF_USER_ID, uid);
        editor.apply();
        editor.commit();
    }

    public boolean isUserRegisteredInJabber() {
        return mMyPrefs.getBoolean(PREF_IS_USER_REGISTERED_IN_JABBER, false);
    }

    public void setFirebaseToken(String token) {
        SharedPreferences.Editor editor = mMyPrefs.edit();
        editor.putString(PREF_FIREBASE_TOKEN, token);
        editor.apply();
        editor.commit();
    }

    public String getFirebaseToken() {
        return mMyPrefs.getString(PREF_FIREBASE_TOKEN, "");
    }
}
