package com.bandesoft.tempo.Utils;

import android.content.Context;
import android.util.Pair;

import com.bandesoft.tempo.R;

import java.util.HashMap;

public class Ctes {
    private static String mYesterday;
    private static String mSomeDaysAgo;
    private static String mThreeWeeksAgo;
    private static String mAlmostOneMonth;
    private static String mSomeMonthsAgo;
    private static String mAlmostOneYear;
    private static String mMoreThanOneYear;

    public static void init(Context context) {
        mYesterday = context.getString(R.string.yesterday);
        mSomeDaysAgo = context.getString(R.string.some_days_ago);
        mThreeWeeksAgo = context.getString(R.string.three_weeks_ago);
        mAlmostOneMonth = context.getString(R.string.almost_one_month_ago);
        mSomeMonthsAgo = context.getString(R.string.some_months_ago);
        mAlmostOneYear = context.getString(R.string.almost_a_year);
        mMoreThanOneYear = context.getString(R.string.more_than_one_year_ago);

        setCategoriesTranslations(context);
    }

    public static String getYesterday() {
        return mYesterday;
    }

    public static String getSomeDaysAgo() {
        return mSomeDaysAgo;
    }

    public static String getThreeWeeksAgo() {
        return mThreeWeeksAgo;
    }

    public static String getAlmostOneMonth() {
        return mAlmostOneMonth;
    }

    public static String getSomeMonthsAgo() {
        return mSomeMonthsAgo;
    }

    public static String getAlmostOneYear() {
        return mAlmostOneYear;
    }

    public static String getMoreThanOneYear() {
        return mMoreThanOneYear;
    }

    // Category translation logic
    // each item has a pair with (tag_translation, tag_drawable_id)
    private static HashMap<String, Pair<String, Integer>> mCategoryTranslation = new HashMap<>();

    // TODO make it more flexible , for cases of adding new categories
    private static void setCategoriesTranslations(Context context) {
        mCategoryTranslation.put(context.getString(R.string.tag_cat_any),
                new Pair<>(context.getString(R.string.category_whatever),
                R.drawable.ic_light_bulb));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_computer),
                new Pair<>(context.getString(R.string.category_computer),
                        R.drawable.ic_computer));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_cook),
                new Pair<>(context.getString(R.string.category_cook),
                        R.drawable.ic_cook));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_teaching),
                new Pair<>(context.getString(R.string.category_teaching),
                        R.drawable.ic_teaching));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_mechanics),
                new Pair<>(context.getString(R.string.category_mechanics),
                        R.drawable.ic_mechanical));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_electrician),
                new Pair<>(context.getString(R.string.category_electrician),
                        R.drawable.ic_electrician));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_plumber),
                new Pair<>(context.getString(R.string.category_plumber),
                        R.drawable.ic_plumber));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_trainer),
                new Pair<>(context.getString(R.string.category_trainer),
                        R.drawable.ic_trainer));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_gardener),
                new Pair<>(context.getString(R.string.category_gardener),
                        R.drawable.ic_gardener));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_transportation),
                new Pair<>(context.getString(R.string.category_transportation),
                        R.drawable.ic_transport));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_caring),
                new Pair<>(context.getString(R.string.category_caring),
                        R.drawable.ic_care));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_housework),
                new Pair<>(context.getString(R.string.category_housework),
                        R.drawable.ic_iron));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_paint),
                new Pair<>(context.getString(R.string.category_paint),
                        R.drawable.ic_rodillo_black));

        mCategoryTranslation.put(context.getString(R.string.tag_cat_computer),
                new Pair<>(context.getString(R.string.category_computer),
                        R.drawable.ic_computer));
    }

    /**
     * Returns the associated category string for the given tag id
     *
     * @param tagCatId The possible values of this parameter are defined in 'tag_categories.xml' file
     * @return
     */
    public static String translateCategoryFromTag(String tagCatId)
    {
        if (mCategoryTranslation.containsKey(tagCatId))
        {
            Pair<String, Integer> lItem = mCategoryTranslation.get(tagCatId);
            return lItem.first;
        }

        return "any";
    }

    public static int getDrawableFromTagCategory(String tagCatId) {
        if (mCategoryTranslation.containsKey(tagCatId))
        {
            Pair<String, Integer> lItem = mCategoryTranslation.get(tagCatId);
            return lItem.second;
        }

        return R.drawable.ic_light_bulb;
    }

    public static String getReadableChatTopic(Context context, String chatTopic) {
        try {
            if (chatTopic.contains(":")) {
                // we will have sth like "needs:Cook"

                int colonIndex = chatTopic.indexOf(":");
                String needsOrOffers = chatTopic.substring(0, colonIndex);

                if (needsOrOffers.compareTo("needs") == 0) {
                    return context.getString(R.string.needs) + ": " +
                            chatTopic.substring(colonIndex + 1);
                }
                else {
                    return context.getString(R.string.offers) + ": " +
                            chatTopic.substring(colonIndex + 1);
                }
            }
            else {
                return chatTopic;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /** Changes "needs:Informático" to "offers:Informático" and vice-versa */
    public static String swapChatTopic(String chatTopic) {
        if (chatTopic.contains(":")) {
            if (chatTopic.contains("needs")) {
                chatTopic = chatTopic.replace("needs", "offers");
            }
            else if (chatTopic.contains("offers")) {
                chatTopic = chatTopic.replace("offers", "needs");
            }
        }
        return chatTopic;
    }
}
