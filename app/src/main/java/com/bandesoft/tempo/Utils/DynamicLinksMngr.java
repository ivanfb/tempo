package com.bandesoft.tempo.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.bandesoft.tempo.BuildConfig;
import com.bandesoft.tempo.R;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import java.util.HashMap;
import java.util.Map;

public class DynamicLinksMngr {

    private static final String TAG = "DynamicLinksMngr";

    public static final String QUERY_PARAM_UID = "param_contact_id";

    public interface I_ActionShareLink {
        void share(Uri lUriToShare);
    }

    public static void shortenLink(Context context, String contactID, I_ActionShareLink action) {
        String lLink = "https://" +
                context.getString(R.string.dynamic_link_tempo) +
                "/users?" + QUERY_PARAM_UID + "=" + contactID;

        DynamicLink lLongDynamicLink =
                FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse(lLink))
                .setDomainUriPrefix(context.getString(R.string.config_scheme) + "://" +
                                    context.getString(R.string.dynamic_link_domain))
                .setAndroidParameters(
                        new DynamicLink.AndroidParameters.Builder(context.getPackageName())
                                .setMinimumVersion(BuildConfig.MIN_SDK_VERSION)
                                .build())
//                .setIosParameters(DynamicLink.IosParameters.Builder("ibi").build())
                .buildDynamicLink();

        FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(lLongDynamicLink.getUri())
                .buildShortDynamicLink()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Uri shortLink = task.getResult().getShortLink();
                        action.share(shortLink);
                    }
                    else { // share the lonkg link in that case
                        Log.e(TAG, "Error mounting firebase dynamic link " +
                                task.getException().getMessage());
                        action.share(lLongDynamicLink.getUri());
                    }
                });
    }

    public interface I_handleDynamicLinkOK {
        void onOk(PendingDynamicLinkData data);
    }

    public static void setDynamicLinkListener(Activity activity,
                                              Intent intent,
                                              I_handleDynamicLinkOK onOkHandler) {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(intent)
                .addOnSuccessListener(activity, pendingDynamicLinkData -> {
                    // Get deep link from result (may be null if no link is found)
                    if (pendingDynamicLinkData != null) {
                        onOkHandler.onOk(pendingDynamicLinkData);
                    }
                })
                .addOnFailureListener(activity, e ->
                    Log.w(TAG, "getDynamicLink:onFailure", e)
                );
    }

    public static Map<String, String> getQueryMap(String query)
    {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<>();
        for (String param : params)
        {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }
}
