package com.bandesoft.tempo.Utils;

import com.bandesoft.tempo.Model.Worker;

import java.util.ArrayList;
import java.util.Random;

public class Mock {

    /**
     * Returns a list with 1000 random workers.
     */
    public static ArrayList<Worker> getWorkers() {
        Random r = new Random();
        ArrayList<Worker> lRet = new ArrayList<Worker>();

        lRet.add( new Worker("1", "name",
                "a " + "abcdefghijklmnño" + " abcdefghijklmnño",
                0, 0,0,0, ""));
        lRet.add( new Worker("2", "name", "abcdefghijklmnño",
                0, 0, 0, 0, ""));

        for (int i = 2; i < 1002; i++) {

            String lName = "";
            int lNumWordsInName = r.nextInt(3) + 1;
            for(int ii = 0; ii < lNumWordsInName; ii++) {
                int lNameLenght = r.nextInt(11) + 1;
                for (int j = 0; j < lNameLenght; j++) {
                    lName += (char) (Math.random() * 26 + 97);
                }
                lName += " ";
            }

            int lNumWords = r.nextInt(50) + 1;
            int lWordLength = r.nextInt(8) + 1;
            String lComment = "";
            for (int nw = 0; nw < lNumWords; nw++) {
                for (int j = 0; j < lWordLength; j++) {
                    lComment += (char) (Math.random() * 26 + 97);
                }
                lComment += " ";
            }

            float lMark = (float) r.nextDouble() * 5;
            int lNumVotes = r.nextInt(200);

            Worker w = new Worker (Integer.toString(i), lName, "", lMark, lNumVotes,
                               0, 0, "");
            lRet.add(w);
        }

        return lRet;
    }
}
