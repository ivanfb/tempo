package com.bandesoft.tempo.Utils;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.bandesoft.tempo.R;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.content.res.AppCompatResources;

public class AlertDialogBuilder {

    public interface I_CategoryDialog extends View.OnClickListener {
        // only the activities implementing this can show the Category Dialog
        void showCategoryDialog();
    }

    public interface I_OkDialogHandler {
        void onAction();
    }

    public interface I_OkComplexDialogHandler {
        void onAction(View v); // allows to return the whole dialog view
    }

    public static void createAndShowAlertDialog(Activity activity,
                                          int titleId,
                                          int messageId,
                                          I_OkDialogHandler a) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(titleId);
        builder.setMessage(messageId);
        builder.setPositiveButton(android.R.string.yes, (dialog, id) -> {
            a.onAction();
            dialog.dismiss();
        });
        builder.setNegativeButton(android.R.string.cancel, (dialog, id) -> {
            dialog.dismiss();
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static View inflateSpecificLayout(Activity activity, int layoutId,
                                             boolean addPositiveAndNegativeButtons,
                                             I_OkComplexDialogHandler a) {
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(layoutId, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        if (addPositiveAndNegativeButtons) {
            builder.setPositiveButton(android.R.string.yes, (dialog, id) -> {
                a.onAction(v);
                dialog.dismiss();
            });
            builder.setNegativeButton(android.R.string.cancel, (dialog, id) -> {
                dialog.dismiss();
            });
        }
        else {
            builder.setPositiveButton(R.string.close, (dialog, i) -> {
                dialog.dismiss();
            });
        }

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_corner);
        dialog.setView(v);
        dialog.show();

        return v;
    }

    public static AlertDialog showGridCategory(I_CategoryDialog categorySelectorAct) {
        if (categorySelectorAct instanceof Activity) {
            Activity activity = (Activity) categorySelectorAct;

            LayoutInflater inflater = activity.getLayoutInflater();

            View v = inflater.inflate(R.layout.dialog_categories_grid, null);

            setDrawable(v, activity, R.id.btn_category_painter);
            setDrawable(v, activity, R.id.btn_category_housework);
            setDrawable(v, activity, R.id.btn_category_caring);
            setDrawable(v, activity, R.id.btn_category_transportation);
            setDrawable(v, activity, R.id.btn_category_gardener);
            setDrawable(v, activity, R.id.btn_category_trainer);
            setDrawable(v, activity, R.id.btn_category_plumber);
            setDrawable(v, activity, R.id.btn_category_electrician);
            setDrawable(v, activity, R.id.btn_category_mechanics);
            setDrawable(v, activity, R.id.btn_category_teaching);
            setDrawable(v, activity, R.id.btn_category_cook);
            setDrawable(v, activity, R.id.btn_category_computer);

            Button lBtnOther = v.findViewById(R.id.btn_any_category);

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setView(v);

            AlertDialog lCategoryDialog = builder.create();
            lCategoryDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

            lCategoryDialog.show();

            lBtnOther.setOnClickListener(categorySelectorAct);

            return lCategoryDialog;
        }
        else
        {
            return null;
        }

    }

    private static void setDrawable(View v, Activity activity, int button) {
        Button lBtn = v.findViewById(button);

        Integer lDrawable = Ctes.getDrawableFromTagCategory((String) lBtn.getTag());

        lBtn.setCompoundDrawablesRelativeWithIntrinsicBounds(null,
                AppCompatResources.getDrawable(activity.getApplicationContext(), lDrawable),
                null, null);
    }
}
