package com.bandesoft.tempo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.bandesoft.tempo.Model.OpinionsViewModel;
import com.bandesoft.tempo.Vista.OpinionsList;

import androidx.lifecycle.ViewModelProvider;

/** Lets show the given or received feedbacks */
public class OpinionsActivity extends BaseActivity {

    public static final String OPINIONS_LIST = "opinions_list";
    public static final String RECEIVED_OPINIONS = "received_opinions";
    public static final String USER_NAME = "user_name";

    private OpinionsViewModel mViewModel;

    private ListView mListOpinions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_opinions);

        super.initToolBar();

        mListOpinions = findViewById(R.id.opinions);

        Intent i = getIntent();
        boolean lShowReceivedOpinionsList = i.getBooleanExtra(RECEIVED_OPINIONS, false);

        TextView lTitle = findViewById(R.id.feedbacks_title);
        lTitle.setText(lShowReceivedOpinionsList ? getString(R.string.worker_feedbacks) :
                getString(R.string.worker_given_feedbacks));

        ((TextView) findViewById(R.id.user_name)).setText(i.getStringExtra(USER_NAME));

        mViewModel = new ViewModelProvider(this).get(OpinionsViewModel.class);
        mViewModel.getOpinions().observe(this, opinions ->
            mListOpinions.setAdapter(new OpinionsList(this, opinions))
        );
        mViewModel.setOpinions(i.getParcelableArrayListExtra(OPINIONS_LIST));

    }
}
