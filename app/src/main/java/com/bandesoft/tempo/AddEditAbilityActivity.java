package com.bandesoft.tempo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bandesoft.tempo.Model.Service;
import com.bandesoft.tempo.Model.SQL;
import com.bandesoft.tempo.Model.Worker;
import com.bandesoft.tempo.Utils.AlertDialogBuilder;
import com.bandesoft.tempo.Utils.Ctes;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;

public class AddEditAbilityActivity extends BaseActivity
        implements TextWatcher, AlertDialogBuilder.I_CategoryDialog
{
    public static final String ACTION_EDIT = "edit";

    public static final String EXTRA_EDIT_TYPE = "edit_type"; // can be either service or need
    public static final int TYPE_NEED = 0;
    public static final int TYPE_NEED_FROM_FRAGMENT = 1; // intended to change the message to the user
    public static final int TYPE_SERVICE = 2;
    public static final int TYPE_SERVICE_FROM_FRAGMENT = 3;

    public static final String PREV_TAG_CATEGORY = "PREV_TAG_CATEGORY";
    public static final String TAG_CATEGORY = "TAG_CATEGORY";
    public static final String DETAILED_DESCR = "DETAILED_DESCR";

    private int mSource;

    private TextView mCategory;
    private EditText mDetailedDescr;
    private TextView mDetailedDescrPendingChars;

    private Activity mActivity;

    private String mPrevCategory; // for edition purposes

    private AlertDialog mCategoryDialog;

    private ArrayList<Service> mMyCurrentServices; // either needs or abilities

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_ability);

        mActivity = this;

        super.initToolBar();

        findViewById(R.id.btn_pic_category)
                .setOnClickListener(v -> showCategoryDialog() );

        mCategory = findViewById(R.id.txt_category);
        mCategory.setOnClickListener(v -> showCategoryDialog());

        mDetailedDescr = findViewById(R.id.detailed_descr);
        mDetailedDescr.addTextChangedListener(this);

        mDetailedDescrPendingChars = findViewById(R.id.penging_chars_in_detailed_descr);

        Intent i = getIntent();
        if (i.getAction() != null && i.getAction().equals(ACTION_EDIT)) {
            mDetailedDescr.setText(i.getStringExtra(DETAILED_DESCR));
        }

        String tagCategory = i.getStringExtra(TAG_CATEGORY);
        if (tagCategory != null) {
            mPrevCategory = tagCategory;
            mCategory.setText(Ctes.translateCategoryFromTag(tagCategory));
            mCategory.setTag(tagCategory);
        }

        Worker me = SQL.getInstance(this).getMe();

        TextView lTitle = findViewById(R.id.edit_title);
        mSource = i.getIntExtra(EXTRA_EDIT_TYPE, TYPE_NEED);
        switch (mSource)
        {
            case TYPE_NEED:
                lTitle.setText(getString(R.string.need_edition));
                mMyCurrentServices = me.getNeeds();
                break;

            case TYPE_NEED_FROM_FRAGMENT:
                lTitle.setText(getString(R.string.add_need_to_profile_message));
                mMyCurrentServices = me.getNeeds();
                break;

            case TYPE_SERVICE:
                lTitle.setText(getString(R.string.service_edition));
                mMyCurrentServices = me.getAbilities();
                break;

            case TYPE_SERVICE_FROM_FRAGMENT:
                lTitle.setText(getString(R.string.add_service_to_profile_message));
                mMyCurrentServices = me.getAbilities();
                break;
        }

        updatePendingCharsTexts();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.btn_ok_action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_ok:
                String lDetailedDescr = mDetailedDescr.getText().toString().trim();

                if (mCategory.getText().toString().isEmpty())
                {
                    Toast.makeText(mActivity, getString(R.string.please_select_a_category),
                            Toast.LENGTH_SHORT).show();
                }
                else if (lDetailedDescr.isEmpty()) {
                    Toast.makeText(mActivity, getString(R.string.please_provide_valid_descr),
                            Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Bundle b = new Bundle();
                    b.putString(TAG_CATEGORY, (String) mCategory.getTag());
                    b.putString(DETAILED_DESCR, lDetailedDescr);

                    if (getIntent().getAction() != null &&
                        getIntent().getAction().equals(ACTION_EDIT) &&
                        mPrevCategory != null)
                    {
                        b.putString(PREV_TAG_CATEGORY, mPrevCategory);
                    }

                    setResult(Activity.RESULT_OK, getIntent().putExtras(b));

                    if (mSource == TYPE_NEED_FROM_FRAGMENT) {
                        Toast.makeText(mActivity, R.string.new_need_added_to_my_profile,
                                Toast.LENGTH_LONG).show();
                    }
                    else if (mSource == TYPE_SERVICE_FROM_FRAGMENT) {
                        Toast.makeText(mActivity, R.string.new_offering_added_to_my_profile,
                                Toast.LENGTH_LONG).show();
                    }

                    finish();
                }

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        updatePendingCharsTexts();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void updatePendingCharsTexts() {
        // update pending chars for detailed descr
        String lDetailedDescString = mDetailedDescr.getText().toString();
        if (lDetailedDescString.compareTo(getString(R.string.ability_detailed_descr)) != 0) {
            int numPendingChars = getResources().getInteger(R.integer.max_length_detailed_descr) - lDetailedDescString.length();
            mDetailedDescrPendingChars.setText(Integer.toString(numPendingChars));
        }
        else {
            mDetailedDescrPendingChars.setText(Integer.toString(R.integer.max_length_detailed_descr));
        }
    }

    @Override
    public void onClick(View v) {
        if (v instanceof TextView) {
            String lNewCategory = (String) v.getTag();

            boolean lAlreadyExists = false;
            for (Service service : mMyCurrentServices) {
                if (service.getCategory().equals(lNewCategory)) {
                    lAlreadyExists = true;
                }
            }

            if (lAlreadyExists)
            {
                switch (mSource)
                {
                    case TYPE_NEED:
                    case TYPE_NEED_FROM_FRAGMENT:
                        Toast.makeText(this,
                                getString(R.string.already_looking_for_that_category),
                                Toast.LENGTH_LONG).show();
                        break;

                    case TYPE_SERVICE:
                    case TYPE_SERVICE_FROM_FRAGMENT:
                        Toast.makeText(this,
                                getString(R.string.already_offering_on_that_category),
                                Toast.LENGTH_LONG).show();
                        break;
                }
            }
            else {
                TextView lClickableTextView = (TextView) v;
                mCategory.setText(lClickableTextView.getText());
                mCategory.setTag(lNewCategory); // the country-independent category name is saved in the tag

                if (mCategoryDialog != null) {
                    mCategoryDialog.dismiss();
                }
            }
        }
    }

    @Override
    public void showCategoryDialog() {
        mCategoryDialog = AlertDialogBuilder.showGridCategory(this);
    }
}
