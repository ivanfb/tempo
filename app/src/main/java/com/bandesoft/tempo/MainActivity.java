package com.bandesoft.tempo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bandesoft.tempo.Model.SQL;
import com.bandesoft.tempo.Remote.AuthManager;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Service.JabberService;
import com.bandesoft.tempo.Service.MyMessagingService;
import com.bandesoft.tempo.Utils.AlertDialogBuilder;
import com.bandesoft.tempo.Utils.Ctes;
import com.bandesoft.tempo.Utils.DynamicLinksMngr;
import com.bandesoft.tempo.Utils.NotifsManager;
import com.bandesoft.tempo.Utils.Prefs;
import com.bandesoft.tempo.Utils.ThreadPool;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Calendar;
import java.util.Date;

import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends BaseActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener,
        ToggleButton.OnClickListener {

    private static final String TAG = "MainActivity";

    private static final String LAST_SELECTED_FRAGMENT = "last_selected_fragment";
    private static final String LAST_SELECTED_SERVICES_FRAGMENT = "last_selected_services_fragment";

    private enum eFragments {
        FRAGMENT_OFFERED_SERVICES,
        FRAGMENT_NEEDED_SERVICES,
        FRAGMENT_CHATS
    }

    private static eFragments mLastFragmentShown = eFragments.FRAGMENT_OFFERED_SERVICES;

    // This should only contemplate either offered or demanded services list
    private static eFragments mLastServicesListShown = eFragments.FRAGMENT_OFFERED_SERVICES;

    public static boolean IsVisible = false;

    public LinearLayoutCompat mBtnNavigator;
    private ToggleButton mServicesBtn, mContactsBtn;

    public MenuItem mMyProfileMenuItem = null;
    public MenuItem mSuggestionsMenuItem = null;
    public MenuItem mRemoveAccountMenuItem = null;
    public MenuItem mLogOutMenuItem = null;

    private Fragment mFragment;

    private FragmentManager mFragManager;

    private SQL mSql;

    private BroadcastReceiver mBroadcastReceiver;

    private void goToFragment(eFragments fragmentId, Bundle args) {
        switch (fragmentId) {
            case FRAGMENT_OFFERED_SERVICES:
            case FRAGMENT_NEEDED_SERVICES:
                mServicesBtn.setChecked(true);
                mContactsBtn.setChecked(false);
                mFragment = new ServicesFragment();

                break;

            case FRAGMENT_CHATS:
                mServicesBtn.setChecked(false);
                mContactsBtn.setChecked(true);
                mFragment = new ContactsFragment();
                break;
        }

        mFragment.setArguments(args);
        mLastFragmentShown = fragmentId;

        final FragmentTransaction transaction = mFragManager.beginTransaction();
        transaction.replace(R.id.fragment_container, mFragment).commit();
    }

    @Override
    public void onClick(View v) {
        boolean lbGoToNextFragment = true;
        eFragments lNextFragmentToLoad = eFragments.FRAGMENT_OFFERED_SERVICES;

        switch (v.getId()) {
            case R.id.btn_services:
                if (mLastFragmentShown == eFragments.FRAGMENT_CHATS) {
                    lNextFragmentToLoad = mLastServicesListShown;
                }
                else {
                    lbGoToNextFragment = false; // don't refresh the same services fragment again
                    mServicesBtn.setChecked(true);
                }
                break;
            case R.id.btn_contacts:
                lNextFragmentToLoad = eFragments.FRAGMENT_CHATS;
                break;
        }

        if (lbGoToNextFragment) {
            goToFragment(lNextFragmentToLoad, null);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Prefs.instance.init(this);
        AuthManager.instance.init(this);
        Ctes.init(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mServicesBtn = findViewById(R.id.btn_services);
        mServicesBtn.setOnClickListener(this);
        mContactsBtn = findViewById(R.id.btn_contacts);
        mContactsBtn.setOnClickListener(this);

        // needed to hide the buttons when clicking on the magnifying glass
        mBtnNavigator = findViewById(R.id.buttons_navigator_container);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        mSql = SQL.getInstance(getApplicationContext());
        mSql.queryMeInBackground();

        mFragManager = getSupportFragmentManager();

        mLastServicesListShown = mLastFragmentShown = eFragments.FRAGMENT_OFFERED_SERVICES;
        if (savedInstanceState != null) {
            mLastFragmentShown = (eFragments) savedInstanceState.getSerializable(LAST_SELECTED_FRAGMENT);
            mLastServicesListShown = (eFragments) savedInstanceState.getSerializable(LAST_SELECTED_SERVICES_FRAGMENT);
        }

        goToFragment(mLastFragmentShown, null);

        if (AuthManager.instance.isUserRegistered()) {
            MyMessagingService.initializeFirebase(this);

            mBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action == null) {
                        return;
                    }

                    switch (action) {
                        case JabberService.NEW_MESSAGE:
                            // this is to tell the user that there are pending messages
                            mContactsBtn.setCompoundDrawablesRelativeWithIntrinsicBounds(
                                    R.drawable.agenda_pending_msgs_color_selector,
                                    0, 0, 0);
                            break;
                    }
                }
            }; // register before starting the service
            registerReceiver(mBroadcastReceiver,
                    new IntentFilter(JabberService.NEW_MESSAGE));

            updateMyLastAccessTime();

            if (!Prefs.instance.isEmailAddressValidated())
            {
                AuthManager.instance.askUserToValidateEmail(this);
            }
        }

        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // needed to handle intents (like from message Notif's) if the app is already running
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        // check if activity was opened from FCM message
        if (intent.getBooleanExtra(NotifsManager.OPENED_FROM_NOTIFICATION, false)) {

            if (!JabberService.isServiceRunning(getApplicationContext())) {
                Log.d(TAG, "Starting service from notification");
                JabberService.start(getApplicationContext());
            }

            Bundle args = new Bundle();
            args.putString(NotifsManager.SENDER_ID, intent.getStringExtra(NotifsManager.SENDER_ID));
            args.putString(NotifsManager.SENDER_NAME, intent.getStringExtra(NotifsManager.SENDER_NAME));
            args.putString(NotifsManager.CHAT_TOPIC, intent.getStringExtra(NotifsManager.CHAT_TOPIC));

            goToFragment(eFragments.FRAGMENT_CHATS, args);
        }
        else {
            DynamicLinksMngr.setDynamicLinkListener(this,
                    intent,
                    data -> {
                        Uri deepLink = data.getLink();
                        Intent i = new Intent(this, WorkerActivity.class);
                        i.putExtra("URL_DEEP_LINK", deepLink.toString());
                        i.putExtra(WorkerActivity.EXTRA_ORIGIN, WorkerActivity.EXT_ORIGIN_DEEP_LINK);
                        startActivity(i);
                    });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_action_bar, menu);

        if(menu instanceof MenuBuilder){
            MenuBuilder menuBuilder = (MenuBuilder) menu;
            menuBuilder.setOptionalIconsVisible(true);
        }

        mMyProfileMenuItem = menu.findItem(R.id.btn_my_profile);
        mSuggestionsMenuItem = menu.findItem(R.id.btn_suggestions);
        mRemoveAccountMenuItem = menu.findItem(R.id.btn_remove_account);
        mLogOutMenuItem = menu.findItem(R.id.btn_log_out);

        updateMenuTexts();

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_my_profile:
                AuthManager.instance.askUserForSigningUpIfNeeded(this,
                        () -> startActivity(new Intent(this,
                                                       MyProfileActivity.class)));
                break;

            case R.id.btn_log_out:
                if (AuthManager.instance.isUserRegistered())
                {
                    AlertDialogBuilder.createAndShowAlertDialog(this,
                            R.string.logging_out_question,
                            R.string.empty,
                            () -> {
                                JabberService.stopService();
                                removeAllMyData();
                            });
                }
                break;

            case R.id.btn_remove_account:
                if (AuthManager.instance.isUserRegistered())
                {
                    AlertDialogBuilder.createAndShowAlertDialog(this,
                            R.string.remove_account_question_tittle,
                            R.string.remove_account_question_message,
                            () ->
                            // The user accepts to remove the account
                            HttpTempo.getInstance()
                                    .removeUserAccountRequest(this,
                                            mSql.getMe().getUid(),
                                        () -> {
                                            // request correctly sent
                                            JabberService.stopServiceAndRemoveAccount();
                                            removeAllMyData();
                                            Toast.makeText(this,
                                                    R.string.account_correctly_removed_message,
                                                    Toast.LENGTH_LONG).show();
                                        },
                                        () ->
                                            // sth wrong occurred
                                            Toast.makeText(this,
                                                    R.string.account_not_removed_message,
                                                    Toast.LENGTH_LONG).show()
                                    )
                    );
                }

                break;

            case R.id.btn_suggestions:
                AuthManager.instance.askUserForSigningUpIfNeeded(
                        this,
                        () -> startActivity(
                                new Intent(this,
                                        SuggestionsActivity.class))
                );

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateMenuTexts() {
        if (mMyProfileMenuItem != null) {
            if (!AuthManager.instance.isUserLoggedIn()) {
                mMyProfileMenuItem.setTitle(getString(R.string.log_in));
            }
            else {
                mMyProfileMenuItem.setTitle(getString(R.string.my_profile));
            }
        }

        if (mSuggestionsMenuItem != null) {
                mSuggestionsMenuItem.setVisible(
                        AuthManager.instance.isUserRegistered());
        }

        if (mRemoveAccountMenuItem != null && mLogOutMenuItem != null) {
            if (!AuthManager.instance.isUserRegistered()) {
                mRemoveAccountMenuItem.setVisible(false);
                mLogOutMenuItem.setVisible(false);
            }
            else {
                mRemoveAccountMenuItem.setVisible(true);
                mLogOutMenuItem.setVisible(true);
            }
        }
    }

    /** Called right after the user gets logged in. Interesting for refreshing the contacts list*/
    public void forceOnResumeServicesFragment() {
        if (mFragment instanceof ServicesFragment) {
            mFragment.onResume(); // force 'onResume' to refresh the list
        }
    }

    private void removeAllMyData() {

        Activity lActivity = this;

        ThreadPool.instance.runTask(() ->
        {
            MyMessagingService.disableFCM();

            mSql.removeEverything();
            AuthManager.instance.clearLocalData(lActivity);

            runOnUiThread(() -> {
                Toast.makeText(lActivity,
                        getString(R.string.correctly_logged_out),
                        Toast.LENGTH_SHORT).show();

                updateMenuTexts();

                forceOnResumeServicesFragment();
            });
        });

        setNoPendingMessages();
        Prefs.instance.clear();
        goToFragment(eFragments.FRAGMENT_OFFERED_SERVICES, null);
    }

    @Override
    protected void onResume() {
        super.onResume();



        IsVisible = true;

        updateContactsColor();
        updateServicesBtnText();
        updateMenuTexts();
    }

    @Override
    protected void onPause() {
        super.onPause();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);

        boolean screenOn;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            screenOn = pm != null ? pm.isInteractive() : false;
        } else {
            screenOn = pm != null ? pm.isScreenOn() : false;
        }

        if (screenOn) { // avoid showing notif when "MainActivity" was shown and screen switches off.
            IsVisible = false;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(LAST_SELECTED_FRAGMENT, mLastFragmentShown);
        outState.putSerializable(LAST_SELECTED_SERVICES_FRAGMENT, mLastServicesListShown);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (mFragment != null) {
            mFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(mBroadcastReceiver);
        } catch(IllegalArgumentException e) {
            Log.w(TAG, "Exception destroying " + e.getMessage());
        }

        super.onDestroy();
    }

    public void updateServicesBtnText() {
        if (Prefs.instance.isSearchOfferedServicesActive()) {
            mServicesBtn.setText(getString(R.string.offered));
            mServicesBtn.setTextOn(getString(R.string.offered));
            mServicesBtn.setTextOff(getString(R.string.offered));

        }
        else {
            mServicesBtn.setText(getString(R.string.needed));
            mServicesBtn.setTextOn(getString(R.string.needed));
            mServicesBtn.setTextOff(getString(R.string.needed));
        }

        if (Prefs.instance.getNumCurrentlyAppliedFilters() > 0) {
            mServicesBtn.setButtonDrawable(R.drawable.rodillo_filtered_color_selector);
        }
        else {
            mServicesBtn.setButtonDrawable(R.drawable.rodillo_color_selector);
        }
    }

    /** Updates the Contacts colors depending on the # of pending messages */
    private void updateContactsColor() {
        if (mSql.getNumChatsWithPendingMessages() > 0) {
            mContactsBtn.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.agenda_pending_msgs_color_selector, 0, 0, 0);
        }
        else {
            setNoPendingMessages();
        }
    }

    public void setNoPendingMessages() {
        mContactsBtn.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.agenda_color_selector,0 ,0 ,0);
    }

    private void updateMyLastAccessTime() {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(new Date());
        cal2.setTime(Prefs.instance.getMyLastAccessTime());
        boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);

        if (!sameDay) {
            HttpTempo.getInstance().updateMyLastAccessDate(this,
                    AuthManager.instance.getUserId(),
                    () -> {}, // TODO: retry in case of failure
                    () -> {});

            Prefs.instance.updateMyLastAccessTime();
        }
    }
}
