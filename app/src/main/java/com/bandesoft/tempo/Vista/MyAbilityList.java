package com.bandesoft.tempo.Vista;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bandesoft.tempo.Model.Service;
import com.bandesoft.tempo.MyProfileActivity;
import com.bandesoft.tempo.R;

import java.util.ArrayList;

import androidx.annotation.NonNull;

public class MyAbilityList extends ArrayAdapter<Service> {

    static class ViewHolder { // this is needed to reduce the # calls to 'findViewById'
        TextView category;
    }

    private final MyProfileActivity mMyProfileActivity;
    private final Activity mContext;
    private ArrayList<Service> mList;

    public MyAbilityList(MyProfileActivity activity, ArrayList<Service> list) {
        super(activity, R.layout.ability_item, list);
        this.mContext = activity;
        this.mList = list;
        this.mMyProfileActivity = activity;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = mContext.getLayoutInflater();
            convertView = inflater.inflate(R.layout.ability_item,
                    parent, false);

            holder = new ViewHolder();
            holder.category = convertView.findViewById(R.id.ability_category);
            convertView.setTag(holder);

            ImageButton lEditBtn = convertView.findViewById(R.id.edit_ability);
            lEditBtn.setOnClickListener((v) -> {
                String category = ((TextView)v.getTag()).getText().toString();
                mMyProfileActivity.handleBtnClick(MyProfileActivity.eEventType.EDIT_ABILITY,
                        category);
            });

            ImageButton lRemoveBtn = convertView.findViewById(R.id.remove_ability);
            lRemoveBtn.setOnClickListener((v) -> {
                String category = ((TextView)v.getTag()).getText().toString();
                mMyProfileActivity.handleBtnClick(MyProfileActivity.eEventType.REMOVE_ABILITY,
                        category);
            });

            // this is needed to properly handle the click events
            lEditBtn.setTag(holder.category);
            lRemoveBtn.setTag(holder.category);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Service a = this.mList.get(position);
        holder.category.setText(a.getCategory());

        return convertView;
    }
}
