package com.bandesoft.tempo.Vista;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bandesoft.tempo.Model.Worker;
import com.bandesoft.tempo.R;
import com.bandesoft.tempo.Utils.ImgLoader;
import com.bandesoft.tempo.Utils.Prefs;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import androidx.annotation.NonNull;

public class WorkersList extends ArrayAdapter<Worker> {

    private static final String TAG = "WorkersList";

    static class ViewHolder { // this is needed to reduce the # calls to 'findViewById'
        public TextView workerName;
        public TextView comment;
        public ImageView pic;
        public RatingBar ratingBar;
        public TextView numVotesRx; // number of received votes
        public LinearLayout ratingLayout;
        public LinearLayout locationAndRatingLayout;
        public TextView location;
    }

    private final Activity mContext;
    private ArrayList<Worker> mList;
    private boolean mIsSearchNeedActive;

    public WorkersList(Activity context, ArrayList<Worker> list) {
        super(context, R.layout.worker_item, list);
        this.mContext = context;
        this.mList = list;
        this.mIsSearchNeedActive = Prefs.instance.isSearchOfferedServicesActive();

        RequestOptions lRequestOptions = new RequestOptions();
        lRequestOptions.placeholder(R.drawable.ic_default_profile_pic);
        lRequestOptions.error(R.drawable.ic_default_profile_pic);
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        Worker w = this.mList.get(position);

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = mContext.getLayoutInflater();
            convertView = inflater.inflate(R.layout.worker_item,
                    parent, false);

            holder = new ViewHolder();

            holder.workerName = convertView.findViewById(R.id.worker_name);
            holder.comment = convertView.findViewById(R.id.comment);
            holder.pic = convertView.findViewById(R.id.worker_item_pic);
            holder.ratingBar = convertView.findViewById(R.id.avg_mark_worker_item);
            holder.numVotesRx = convertView.findViewById(R.id.txt_num_votes_worker_item);
            holder.ratingLayout = convertView.findViewById(R.id.ratingItem);
            holder.location = convertView.findViewById(R.id.worker_location);
            holder.locationAndRatingLayout = convertView.findViewById(R.id.location_and_rating);
            holder.comment.setHorizontallyScrolling(true);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.workerName.setText(w.getName());
        holder.comment.setText(w.getServDescriptions(mIsSearchNeedActive));

        if (!w.getLocation().getCityName().isEmpty()) {
            holder.location.setText(w.getLocation().getCityName());
            holder.locationAndRatingLayout.setVisibility(View.VISIBLE);
        }
        else {
            holder.location.setText("");
        }

        if (w.getNumVotes() > 0) {
            holder.ratingBar.setRating(w.getMark());
            holder.numVotesRx.setText("(" + Integer.toString(w.getNumVotes()) + ")");
            holder.ratingLayout.setVisibility(View.VISIBLE);
            holder.locationAndRatingLayout.setVisibility(View.VISIBLE);
        }
        else {
            holder.ratingLayout.setVisibility(View.GONE);
        }

        ImgLoader.downloadUserPicIntoView(mContext, w.getUid(), holder.pic, w.getLastDateImageChanged());

        return convertView;
    }
}
