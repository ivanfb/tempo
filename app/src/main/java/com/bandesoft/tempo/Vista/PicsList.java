package com.bandesoft.tempo.Vista;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bandesoft.tempo.Model.OtherPic;
import com.bandesoft.tempo.R;

import java.util.ArrayList;

public class PicsList extends ArrayAdapter<OtherPic> {

    static class ViewHolder {
        ImageView pic;
        ImageButton btn;
    }

    private final Activity mContext;
    private ArrayList<OtherPic> mList;
    private View.OnClickListener mClickListener;

    public static final int OTHER_PIC_ID = 0x123321;

    private boolean mOtherUserPics = false;

    public PicsList(Activity context, ArrayList<OtherPic> list, View.OnClickListener clickListener) {
        super(context, R.layout.my_other_pic_item, list);
        this.mList = list;
        this.mContext = context;
        this.mClickListener = clickListener;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = mContext.getLayoutInflater();
            convertView = inflater.inflate(R.layout.my_other_pic_item,
                    parent, false);

            holder = new ViewHolder();

            holder.pic = convertView.findViewById(R.id.other_pic);
            holder.btn = convertView.findViewById(R.id.remove_other_pic_btn);

            holder.pic.setClickable(true);
            holder.pic.setOnClickListener(mClickListener);

            if(mOtherUserPics) {
                convertView.findViewById(R.id.remove_item_btn_container).setVisibility(View.GONE);
            }
            else {
                holder.btn.setOnClickListener(mClickListener);
            }

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.pic.setImageBitmap(mList.get(position).getBitmap());
        holder.pic.setTag(position);
        holder.pic.setId(OTHER_PIC_ID);
        holder.btn.setTag(position);

        return convertView;
    }

    public void deleteAt(int index) {
        if (index >= 0 && index < mList.size()){
            mList.remove(index);
            notifyDataSetChanged();
        }
    }

    public ArrayList<String> getAllFileNames() {
        ArrayList<String> lRet = new ArrayList<>();
        for (OtherPic otherPic: mList) {
            lRet.add(otherPic.getFileName());
        }
        return lRet;
    }

    public void setOtherUserPics(boolean otherUserPics) {
        this.mOtherUserPics = otherUserPics;
    }
}
