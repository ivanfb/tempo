package com.bandesoft.tempo.Vista;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bandesoft.tempo.Model.Contact;
import com.bandesoft.tempo.Model.Worker;
import com.bandesoft.tempo.R;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Utils.Ctes;
import com.bandesoft.tempo.Utils.ImgLoader;
import com.bandesoft.tempo.WorkerActivity;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

public class ContactsList extends ArrayAdapter<Contact> {

    private static final String TAG = "ContactsList";

    static class ViewHolder { // this is needed to reduce the # calls to 'findViewById'
        public ImageView envelopeImg;
        public ImageView selectedImg; // the checkbox that indicates "item selected"
        public TextView workerName;
        public TextView topic;
        public TextView lastTime;
        public TextView lastMessage;
        public ImageView pic;
        public LinearLayout contactImgNameContainer;
    }

    private final Activity mContext;
    private ArrayList<Contact> mList;

    private HashMap<String, Bitmap> mUserImages = new HashMap<>();

    public ContactsList(Activity context, ArrayList<Contact> list) {
        super(context, R.layout.contact_item, list);
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        Contact contact = this.mList.get(position);

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = mContext.getLayoutInflater();
            convertView = inflater.inflate(R.layout.contact_item,
                    parent, false);

            holder = new ViewHolder();

            holder.envelopeImg = convertView.findViewById(R.id.imgPendingMessages);
            holder.selectedImg = convertView.findViewById(R.id.imgItemSelected);
            holder.workerName = convertView.findViewById(R.id.worker_name);
            holder.topic = convertView.findViewById(R.id.topic);
            holder.pic = convertView.findViewById(R.id.contact_item_pic);
            holder.contactImgNameContainer = convertView.findViewById(R.id.contact_img_name_container);
            holder.lastTime = convertView.findViewById(R.id.last_time);
            holder.lastMessage = convertView.findViewById(R.id.last_message);

            holder.topic.setHorizontallyScrolling(true);

            final String lUID = contact.getUid();
            ImgLoader.downloadFromUrl(mContext,
                    HttpTempo.URL_DOWNLOAD_IMG + lUID + ".jpeg",
                    new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource,
                                                    @Nullable Transition<? super Bitmap> transition) {
                            mUserImages.put(lUID, resource);
                            if (resource != null) {
                                holder.pic.setImageBitmap(mUserImages.get(contact.getUid()));
                            }
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {

                        }
                    },
                    contact.getLastDateImageChanged());

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.envelopeImg.setVisibility(contact.hasPendingMsgs() ? View.VISIBLE:View.INVISIBLE);
        holder.selectedImg.setVisibility(contact.isSelected() ? View.VISIBLE:View.INVISIBLE);
        convertView.setBackgroundColor(ContextCompat.getColor(mContext,
                contact.isSelected() ? R.color.colorGreySoft: R.color.white));
        holder.workerName.setText(contact.getName());
        holder.topic.setText(Ctes.getReadableChatTopic(mContext, contact.getServiceTopic()));
        holder.lastTime.setText(contact.getLastDayTime());
        holder.lastMessage.setText(contact.getLastMessage());

        Bitmap contactBmp = mUserImages.get(contact.getUid());
        if (contactBmp != null) {
            holder.pic.setImageBitmap(mUserImages.get(contact.getUid()));
        }
        else {
            holder.pic.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_default_profile_pic));
        }

        holder.contactImgNameContainer.setOnClickListener( v -> {
            Worker selectedWorker = this.mList.get(position);
            Intent i = new Intent(mContext, WorkerActivity.class);

            i.putExtra(WorkerActivity.EXTRA_ORIGIN, WorkerActivity.EXT_ORIGIN_CONTACT);

            Drawable drawable = holder.pic.getDrawable();
            if (drawable instanceof BitmapDrawable) {
                // passing the image through a temporary file
                ImgLoader.saveImageToInternalStorage(mContext,
                        ((BitmapDrawable) drawable).getBitmap(), WorkerActivity.WORKER_PIC);
                i.putExtra(WorkerActivity.WORKER_PIC_PASSED, true);
            }

            i.putExtra(WorkerActivity.WORKER, selectedWorker);
            mContext.startActivity(i);
        });

        return convertView;
    }
}
