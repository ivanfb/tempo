package com.bandesoft.tempo.Vista;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bandesoft.tempo.Model.Service;
import com.bandesoft.tempo.R;
import com.bandesoft.tempo.Utils.Ctes;
import com.bandesoft.tempo.WorkerActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;

/**
 * ArrayAdapter for allocating the worker candidate's abilities.
 */
public class WorkerAbilityList extends ArrayAdapter<Service> {

    static class ViewHolder {
        TextView textViewCategory;
        String category;
        TextView detailedDesc;
    }

    private WorkerActivity mActivity;
    private ArrayList<Service> mList;

    public WorkerAbilityList(WorkerActivity activity, ArrayList<Service> list) {
        super(activity, R.layout.worker_ability_item, list);
        this.mActivity = activity;
        this.mList = list;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        WorkerAbilityList.ViewHolder holder;

        Service service = this.mList.get(position);

        if (convertView == null) {
            LayoutInflater inflater = mActivity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.worker_ability_item,
                    parent, false);

            holder = new ViewHolder();
            holder.textViewCategory = convertView.findViewById(R.id.worker_ability_category);
            holder.detailedDesc = convertView.findViewById(R.id.worker_ability_detailed_descr);
            holder.category = Ctes.translateCategoryFromTag(service.getCategory());

            convertView.setTag(holder);
        } else {
            holder = (WorkerAbilityList.ViewHolder) convertView.getTag();
        }

        holder.textViewCategory.setText(holder.category);
        holder.detailedDesc.setText(service.getDetailedDescr());

        return convertView;
    }
}
