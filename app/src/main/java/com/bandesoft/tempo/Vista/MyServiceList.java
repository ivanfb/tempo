package com.bandesoft.tempo.Vista;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bandesoft.tempo.Model.Service;
import com.bandesoft.tempo.MyProfileActivity;
import com.bandesoft.tempo.R;
import com.bandesoft.tempo.Utils.Ctes;

import java.util.ArrayList;

import androidx.annotation.NonNull;

public class MyServiceList extends ArrayAdapter<Service> {

    static class ViewHolder { // this is needed to reduce the # calls to 'findViewById'
        TextView textViewCategory;
    }

    public enum eListType {
        NEED, OFFER
    }
    private eListType mListType;

    private final MyProfileActivity mMyProfileActivity;
    private final Activity mContext;
    private ArrayList<Service> mList;

    public MyServiceList(MyProfileActivity activity, ArrayList<Service> list, eListType listType) {
        super(activity, R.layout.ability_item, list);
        this.mContext = activity;
        this.mList = list;
        this.mMyProfileActivity = activity;
        this.mListType = listType;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        Service service = this.mList.get(position);

        if (convertView == null) {
            LayoutInflater inflater = mContext.getLayoutInflater();
            convertView = inflater.inflate(R.layout.ability_item,
                    parent, false);

            holder = new ViewHolder();
            holder.textViewCategory = convertView.findViewById(R.id.ability_category);
            convertView.setTag(holder);

            ImageButton lEditBtn = convertView.findViewById(R.id.edit_ability);
            lEditBtn.setOnClickListener((v) -> {

                switch (mListType) {
                    case OFFER:
                        mMyProfileActivity.handleBtnClick(MyProfileActivity.eEventType.EDIT_ABILITY,
                                (String) v.getTag());
                        break;
                    case NEED:
                        mMyProfileActivity.handleBtnClick(MyProfileActivity.eEventType.EDIT_NEED,
                                (String) v.getTag());
                        break;
                }
            });

            ImageButton lRemoveBtn = convertView.findViewById(R.id.remove_ability);
            lRemoveBtn.setOnClickListener((v) -> {

                switch (mListType) {
                    case OFFER:
                        mMyProfileActivity.handleBtnClick(MyProfileActivity.eEventType.REMOVE_ABILITY,
                                (String) v.getTag());
                        break;
                    case NEED:
                        mMyProfileActivity.handleBtnClick(MyProfileActivity.eEventType.REMOVE_NEED,
                                (String) v.getTag());
                        break;
                }
            });

            // this is needed to properly handle the click events
            lEditBtn.setTag(service.getCategory());
            lRemoveBtn.setTag(service.getCategory());
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.textViewCategory.setText(Ctes.translateCategoryFromTag(service.getCategory()));

        return convertView;
    }
}
