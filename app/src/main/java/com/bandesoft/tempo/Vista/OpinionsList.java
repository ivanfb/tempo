package com.bandesoft.tempo.Vista;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bandesoft.tempo.Model.Opinion;
import com.bandesoft.tempo.R;

import java.util.List;

import androidx.annotation.NonNull;

public class OpinionsList extends ArrayAdapter<Opinion> {
    private static final String TAG = "OpinionsList";

    static class ViewHolder { // this is needed to reduce the # calls to 'findViewById'
        public TextView comment;
        public TextView evaluatorName;
        public RatingBar ratingBar;
        public TextView date;
    }

    private final Activity mContext;
    private List<Opinion> mList;

    public OpinionsList(Activity context, List<Opinion> list) {
        super(context, R.layout.opinion_item, list);
        this.mContext = context;
        this.mList = list;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = mContext.getLayoutInflater();
            convertView = inflater.inflate(R.layout.opinion_item,
                    parent, false);

            holder = new ViewHolder();

            holder.comment = convertView.findViewById(R.id.comment);
            holder.date = convertView.findViewById(R.id.date);
            holder.evaluatorName = convertView.findViewById(R.id.evaluator_name);
            holder.ratingBar = convertView.findViewById(R.id.opinion_rating_bar);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Opinion op = this.mList.get(position);

        holder.comment.setText(op.getComment());
        holder.date.setText(op.getDate());
        holder.evaluatorName.setText(op.getEvaluatorName());
        holder.ratingBar.setRating(op.getRating());

        return convertView;
    }
}
