package com.bandesoft.tempo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bandesoft.tempo.Model.Service;
import com.bandesoft.tempo.Model.LonLatCity;
import com.bandesoft.tempo.Model.MyProfileViewModel;
import com.bandesoft.tempo.Model.SQL;
import com.bandesoft.tempo.Model.Worker;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Utils.AlertDialogBuilder;
import com.bandesoft.tempo.Utils.ImgLoader;
import com.bandesoft.tempo.Vista.MyServiceList;

import java.io.File;

import androidx.lifecycle.ViewModelProvider;

public class MyProfileActivity extends BaseActivity
        implements ViewTreeObserver.OnGlobalLayoutListener {

    private static final String TAG = "MyProfileActivity";

    private static final int REQ_EDIT_PICS = 1;
    public static final int REQ_ADD_NEW_ABILITY = 3;
    public static final int REQ_EDIT_ABILITY = 4;
    private static final int REQ_EDIT_LOCATION = 5;
    public static final int REQ_ADD_NEW_NEED = 6;
    private static final int REQ_EDIT_NEED = 7;

    private ListView mListMyAbilities, mListMyNeeds;
    private MyProfileViewModel mViewModel;

    private TextView mName;
    private TextView mCity;
    private ImageView mMyProfilePic;
    private SQL mSQL;

    private MyServiceList mMyServiceListAdapter, mMyAbilityListAdapter;

    private boolean mPendingToUpload = false; // TODO , perform retrials if upload didn't succeed.
    private boolean mGoUpWhenEditing = true;

    private ScrollView mScrollView;

    public enum eEventType {
        EDIT_ABILITY, REMOVE_ABILITY, EDIT_LOCATION, REMOVE_LOCATION,
                             EDIT_NEED, REMOVE_NEED}

    @Override
    public void onGlobalLayout() {
        if (mGoUpWhenEditing) {
            mScrollView.fullScroll(View.FOCUS_UP);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        super.initToolBar();

        mScrollView = findViewById(R.id.scroll_view);
        // Wait until my scrollView is ready
        mScrollView.getViewTreeObserver().addOnGlobalLayoutListener(this);

        Animation lFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        mSQL = SQL.getInstance(getApplicationContext());
        mMyProfilePic = findViewById(R.id.my_profile_pic);

        mViewModel = new ViewModelProvider(this).get(MyProfileViewModel.class);

        mListMyAbilities = findViewById(R.id.my_abilities_list);
        mListMyNeeds = findViewById(R.id.my_needs_list);

        Worker me = mSQL.getMe();
        mViewModel.init(me);

        mViewModel.getMe().observe(this, w -> {
            String newName = w.getName();
            String currentName = mName.getText().toString();
            if (newName.compareTo(currentName) != 0) {
                mName.setText(newName);
            }

            mCity.setText(w.getLocation().getCityName());

            if (w.getPic() != null) {
                mMyProfilePic.setImageBitmap(w.getPic());
            }

            // TODO, try to avoid calling 'new' so often
            mMyAbilityListAdapter = new MyServiceList(this, me.getAbilities(), MyServiceList.eListType.OFFER);
            mListMyAbilities.setAdapter(mMyAbilityListAdapter);

            mMyServiceListAdapter = new MyServiceList(this, me.getNeeds(), MyServiceList.eListType.NEED);
            mListMyNeeds.setAdapter(mMyServiceListAdapter);
        });

        if (mViewModel.getMyPic() == null)
        {
            File lFile = new File(getApplicationContext().getFilesDir(),
                    getString(R.string.my_profile_pic_name));
            if (lFile.exists()) {
                loadMyPic();
            }
            else { // Then, let's try to download
                ImgLoader.downloadMyPicIntoUserPicFile(this,
                        mSQL.getMe().getUid(),
                        () -> runOnUiThread(() -> loadMyPic()));
            }
        }
        else {
            mMyProfilePic.clearAnimation();
            mMyProfilePic.setVisibility(View.VISIBLE);
            mMyProfilePic.startAnimation(lFadeInAnimation);
        }

        mMyProfilePic.setOnClickListener(v -> {
                    Intent i = new Intent(this, PicsActivity.class);
                    i.putExtra(PicsActivity.PICS_CATEGORY, PicsActivity.MY_PICS);
                    startActivityForResult(i, REQ_EDIT_PICS);
                }
        );

        mName = findViewById(R.id.name);
        mCity = findViewById(R.id.city);

        findViewById(R.id.btn_edit_my_name).setOnClickListener(v -> createRenameDialog());

        findViewById(R.id.edit_location).setOnClickListener(v -> {
                Intent i = new Intent(this, MapActivity.class);

                Worker wme = mViewModel.getMe().getValue();
                i.putExtra(MapActivity.EXTRA_CITY_NAME, wme.getLocation().getCityName());
                i.putExtra(MapActivity.EXTRA_LAT, wme.getLocation().getLat());
                i.putExtra(MapActivity.EXTRA_LON, wme.getLocation().getLon());
                i.putExtra(MapActivity.EXTRA_PREVIOUS_ACTIVITY_IS_PROFILE, true);

                startActivityForResult(i, REQ_EDIT_LOCATION);
            }
        );

        findViewById(R.id.btn_add_ability).setOnClickListener(v -> {
                Intent i = new Intent(this, AddEditAbilityActivity.class);
                i.putExtra(AddEditAbilityActivity.EXTRA_EDIT_TYPE, AddEditAbilityActivity.TYPE_SERVICE);
                startActivityForResult(i, REQ_ADD_NEW_ABILITY);
            }
        );

        findViewById(R.id.btn_add_need).setOnClickListener(v -> {
                Intent i = new Intent(this, AddEditAbilityActivity.class);
                i.putExtra(AddEditAbilityActivity.EXTRA_EDIT_TYPE, AddEditAbilityActivity.TYPE_NEED);
                startActivityForResult(i, REQ_ADD_NEW_NEED);
            }
        );
    }

    private void createRenameDialog() {
        View v = AlertDialogBuilder.inflateSpecificLayout(this,
                R.layout.dialog_change_my_name,
                true,
                (view) -> {
                    TextView lMyName = view.findViewById(R.id.my_name);
                    mViewModel.setMyName(lMyName.getText().toString());
                    mSQL.updateMyName(lMyName.getText().toString());
                    saveMyInfoRemotely();
                });

        EditText lMyName = v.findViewById(R.id.my_name);
        lMyName.setText(mName.getText());
    }

    private void loadMyPic() {
        try {
            if (!ImgLoader.loadPic(mMyProfilePic, this,
                    getString(R.string.my_profile_pic_name),
                    bmpOut -> mViewModel.setMyPic(bmpOut)))
            { // my pic wasn't loaded properly. Then, load the default one.
                mMyProfilePic.setImageDrawable(getResources().getDrawable(R.drawable.ic_default_profile_pic));
            }
        } catch (IllegalStateException ise) {
            // That exc might occur if user goes to "NeededServicesFragment" and rapidly
            // goes to "OfferedServicesFragment", for instance.
            Log.e(TAG, "Maybe fragment not attached to activity" + ise.getMessage());
        }
    }

    private void saveMyInfoRemotely() {
        MyProfileActivity activity = this;

        HttpTempo.getInstance().sendUserInformation(mSQL.getMe(),
                () -> {
                    mPendingToUpload = false;
                    Toast.makeText(activity, R.string.changes_saved_message,
                            Toast.LENGTH_SHORT).show();
                });
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        if (data != null && data.getExtras() != null) {
            Bundle b = data.getExtras();
            if (requestCode == REQ_ADD_NEW_ABILITY)
            {
                mGoUpWhenEditing = false;

                String lCategory = b.getString(AddEditAbilityActivity.TAG_CATEGORY);
                String lDetailedDescr = b.getString(AddEditAbilityActivity.DETAILED_DESCR);

                mSQL.addMyAbility(new Service(lCategory, lDetailedDescr, false));
                mMyAbilityListAdapter.notifyDataSetChanged();
            }
            else if (requestCode == REQ_EDIT_ABILITY)
            {
                String lPrevTagCategory = b.getString(AddEditAbilityActivity.PREV_TAG_CATEGORY);
                String lTagCategory = b.getString(AddEditAbilityActivity.TAG_CATEGORY);
                String lDetailedDescr = b.getString(AddEditAbilityActivity.DETAILED_DESCR);

                Service a = new Service(lTagCategory, lDetailedDescr, false);
                mViewModel.editMyAbility(lPrevTagCategory, a);
                mSQL.editMyAbility(lPrevTagCategory, a);
            }
            else if (requestCode == REQ_ADD_NEW_NEED)
            {
                mGoUpWhenEditing = false;

                String lTagCategory = b.getString(AddEditAbilityActivity.TAG_CATEGORY);
                String lDetailedDescr = b.getString(AddEditAbilityActivity.DETAILED_DESCR);

                mSQL.addMyNeed(new Service(lTagCategory, lDetailedDescr, true));
                mMyServiceListAdapter.notifyDataSetChanged();
            }
            else if (requestCode == REQ_EDIT_NEED)
            {
                String lPrevTagCategory = b.getString(AddEditAbilityActivity.PREV_TAG_CATEGORY);
                String lTagCategory = b.getString(AddEditAbilityActivity.TAG_CATEGORY);
                String lDetailedDescr = b.getString(AddEditAbilityActivity.DETAILED_DESCR);

                Service need = new Service(lTagCategory, lDetailedDescr, true);
                mViewModel.editMyNeed(lPrevTagCategory, need);
                mSQL.editMyNeed(lPrevTagCategory, need);
            }
            else if (requestCode == REQ_EDIT_LOCATION)
            {
                String lCity = b.getString(MapActivity.RESULT_POS_TEXT);
                Double lLat = b.getDouble(MapActivity.RESULT_POS_LAT);
                Double lLon = b.getDouble(MapActivity.RESULT_POS_LON);

                LonLatCity lonLatCity = new LonLatCity(lLon, lLat, lCity);
                mViewModel.setMyLocation(lonLatCity);
                mSQL.updateMyLocation(lonLatCity);
            }
        }

        if (requestCode == REQ_EDIT_PICS) {
            loadMyPic();
        }

        mPendingToUpload = true;
        saveMyInfoRemotely();
    }

    public void handleBtnClick(eEventType eventType, String category) {
        switch (eventType) {
            case EDIT_ABILITY:
                Service myService = mViewModel.getMyAbility(category);
                Intent i = new Intent(this, AddEditAbilityActivity.class);
                i.setAction(AddEditAbilityActivity.ACTION_EDIT);
                i.putExtra(AddEditAbilityActivity.TAG_CATEGORY, category);
                i.putExtra(AddEditAbilityActivity.DETAILED_DESCR,
                        myService != null ? myService.getDetailedDescr():"");
                i.putExtra(AddEditAbilityActivity.EXTRA_EDIT_TYPE, AddEditAbilityActivity.TYPE_SERVICE);
                startActivityForResult(i, REQ_EDIT_ABILITY);
                break;
            case REMOVE_ABILITY:
                AlertDialogBuilder.createAndShowAlertDialog(this,
                        R.string.remove_ability_question,
                        R.string.empty,
                        () -> {
                            mGoUpWhenEditing = false;
                            mViewModel.clearAbility(category);
                            mSQL.removeMyAbility(category);
                            mPendingToUpload = true;
                            saveMyInfoRemotely();
                        }
                );
                break;
            case EDIT_NEED:
                Service myNeed = mViewModel.getMyNeed(category);
                Intent intent = new Intent(this, AddEditAbilityActivity.class);
                intent.setAction(AddEditAbilityActivity.ACTION_EDIT);
                intent.putExtra(AddEditAbilityActivity.TAG_CATEGORY, category);
                intent.putExtra(AddEditAbilityActivity.DETAILED_DESCR,
                        myNeed != null ? myNeed.getDetailedDescr():"");
                intent.putExtra(AddEditAbilityActivity.EXTRA_EDIT_TYPE, AddEditAbilityActivity.TYPE_NEED);
                startActivityForResult(intent, REQ_EDIT_NEED);
                break;
            case REMOVE_NEED:
                AlertDialogBuilder.createAndShowAlertDialog(this,
                        R.string.remove_need_question,
                        R.string.empty,
                        () -> {
                            mGoUpWhenEditing = false;
                            mViewModel.clearNeed(category);
                            mSQL.removeMyNeed(category);
                            mPendingToUpload = true;
                            saveMyInfoRemotely();
                        }
                );
                break;
        }
    }
}
