package com.bandesoft.tempo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;
import com.bandesoft.tempo.Model.JsonMessage;
import com.bandesoft.tempo.Model.SQL;
import com.bandesoft.tempo.Model.StorableMessage;
import com.bandesoft.tempo.Remote.AuthManager;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Remote.JabberConnection;
import com.bandesoft.tempo.Service.JabberService;
import com.bandesoft.tempo.Utils.AlertDialogBuilder;
import com.bandesoft.tempo.Utils.Ctes;
import com.bandesoft.tempo.Utils.ThreadPool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;

public class ChatActivity extends BaseActivity {
    private static final String TAG = "ChatActivity";

    private ChatView mChatView;
    private BroadcastReceiver mBroadcastReceiver;
    private String mFullContactJid; // form uid@domain
    private String mContactName;
    private MenuItem mMenuItem;
    private SQL mSQL;
    private String mContactUID; // also, chatId
    private int mTotalNumOfMessages = 0, mTotalNumSentMessages = 0;
    private String mChatTopic;

    public static boolean IsVisible = false;
    public static final String STR_EXTR_CONTACT_UID = "CONTACT_UID";
    public static final String STR_EXTR_CONTACT_NAME = "CONTACT_NAME";
    public static final String STR_EXTR_SERVICE_NAME = "SERVICE_NAME";

    private interface IHandleAlertDialogAccept {
        void onAccept(String comment, float mark);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mSQL = SQL.getInstance(this);

        mChatView = findViewById(R.id.chat_view);

        Intent intent = getIntent();

        mContactUID = intent.getStringExtra(STR_EXTR_CONTACT_UID);
        mFullContactJid = mContactUID + "@" + JabberConnection.DOMAIN;

        mContactName = intent.getStringExtra(STR_EXTR_CONTACT_NAME);
        mChatTopic = intent.getStringExtra(STR_EXTR_SERVICE_NAME);

        TextView lTextViewServiceTopic = findViewById(R.id.service_topic);

        if (mChatTopic != null && mChatTopic.compareTo("") != 0) {
            lTextViewServiceTopic.setText(Ctes.getReadableChatTopic(this, mChatTopic));
        }
        else {
            lTextViewServiceTopic.setVisibility(View.GONE);
        }

        super.initToolBar();
        getSupportActionBar().setTitle(mContactName);

        mChatView.setOnSentMessageListener(chatMessage ->
            {
//                you may return true or false. true will
//                update the ChatView with the message bubble and false will do the opposite.
                sendMessage(JsonMessage.NORMAL, chatMessage.getMessage());
                mTotalNumOfMessages++;
                mTotalNumSentMessages++;
                return true;
            }
        );

        readLastMessages();

        registerNewMessageReceiver();
    }

    /** Converts the 'chat topic' attribute into a readable one */
    private void readLastMessages() {
        Log.d(TAG, "reading last messages");
        ThreadPool.instance.runTask(() -> {
            ArrayList<StorableMessage> lastStorableMessages = mSQL.getLastMessages(40, mContactUID);
            if (lastStorableMessages != null) {
                mTotalNumOfMessages = lastStorableMessages.size();
                for (int j = lastStorableMessages.size() - 1; j >= 0; j--) {
                    StorableMessage m = lastStorableMessages.get(j);
                    ChatMessage lMsg = new ChatMessage(m.rawBody,
                            m.timestamp,
                            m.am_I_the_Sender ? ChatMessage.Type.SENT :
                                    ChatMessage.Type.RECEIVED);

                    Log.d(TAG, "adding message [" + m.rawBody + "]");
                    runOnUiThread(() -> mChatView.addMessage(lMsg));
                }
            }
        });
    }

    private void registerNewMessageReceiver() {
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case JabberService.NEW_MESSAGE:
                        int messageType = intent.getIntExtra(JabberService.BUNDLE_MESSAGE_TYPE, JsonMessage.NORMAL);
                        String body = intent.getStringExtra(JabberService.BUNDLE_MESSAGE_BODY);
                        String from = intent.getStringExtra(JabberService.BUNDLE_FROM_JID);
                        String sentTime = intent.getStringExtra(JabberService.BUNDLE_SENT_TIME);
                        String senderName = intent.getStringExtra(JabberService.BUNDLE_SENDER_NAME);
                        String chatTopic = intent.getStringExtra(JabberService.BUNDLE_CHAT_TOPIC);
                        boolean infoHandledAlready = intent.getBooleanExtra(JabberService.BUNDLE_INFO_HANDLED_IN_JABBER_CONN, false);

                        String uid = from;
                        if (uid.contains("@")) {
                            uid = uid.split("@")[0];
                        }

                        if (messageType == JsonMessage.AGREEMENT_REQUEST)
                        {
                            mMenuItem.setIcon(R.drawable.ic_before_agreement);
                            mSQL.setChatInfo(uid, false,
                                    SQL.eChatStatus.STATUS_ONE_PERSON_STARTED_THE_DEAL, mChatTopic);

                            body = getString(R.string.body_msg_agreement_req_received);
                        }
                        else if (messageType == JsonMessage.AGREEMENT_ACCEPT) {
                            mMenuItem.setIcon(R.drawable.ic_star);
                            mSQL.updateChatStatus(uid, SQL.eChatStatus.STATUS_AGREEMENT_REACHED);

                            body = getString(R.string.body_msg_agreement_req_accepted) + mContactName;
                        }
                        else if (messageType == JsonMessage.ASSESSMENT_DONE) {
                            body = String.format(getString(R.string.body_msg_assessment_received),
                                    mContactName);
                        }

                        ChatMessage lMsg = new ChatMessage(body,
                                Long.parseLong(sentTime),
                                ChatMessage.Type.RECEIVED);

                        // pending messages to be read
                        boolean pendingMessages = false;

                        Log.d(TAG, "receiving new message");

                        if (from.equals(mFullContactJid)) { // message received from the current chat user
                            Log.d(TAG, "adding message [" + body + "]");
                            mChatView.addMessage(lMsg);
                            mTotalNumOfMessages++;
                        }
                        else {
                            Log.d(TAG,"Got a message from jid :" + from);
                            pendingMessages = true;
                        }

                        if (!infoHandledAlready) {
                            // persisting messages
                            mSQL.addMessage(uid, body, sentTime, false, chatTopic, pendingMessages);
                            mSQL.storeContactWithJustName(uid, senderName);
                        }
                }
            }
        };

        IntentFilter filter = new IntentFilter(JabberService.NEW_MESSAGE);
        registerReceiver(mBroadcastReceiver,filter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IsVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        IsVisible = false;

        sendBroadcast(new Intent(JabberService.STORE_PENDING_MESSAGES));

        if (mTotalNumSentMessages > 0) {
            mTotalNumSentMessages = 0;
            // To make appear notif to the contact.
            HttpTempo.getInstance()
                    .sendPendingMsgsPushNotificationToUser(this,
                                                            mContactUID,
                                                           mSQL.getMe().getName(),
                                                           mChatTopic);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(mBroadcastReceiver);
        } catch(IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chat_action_bar, menu);
        this.mMenuItem = menu.findItem(R.id.btn_deal_actions);

        switch (mSQL.getChatStatus(mContactUID)) {
            case STATUS_BEFORE_ANY_ACTION:
                this.mMenuItem.setIcon(R.drawable.ic_rocket);
                break;

            case STATUS_ONE_PERSON_STARTED_THE_DEAL:
                this.mMenuItem.setIcon(R.drawable.ic_before_agreement);
                break;

            case STATUS_AGREEMENT_REACHED:
                this.mMenuItem.setIcon(R.drawable.ic_star);
                break;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.btn_deal_actions) {

            if (mTotalNumOfMessages == 0) {
                Toast.makeText(this, R.string.say_sth_before_any_agreement,
                        Toast.LENGTH_LONG).show();
                return super.onOptionsItemSelected(item);
            }

            LayoutInflater inflater = getLayoutInflater();
            View v = inflater.inflate(R.layout.dialog_chat_activity, null);
            TextView lTitle = v.findViewById(R.id.title);
            TextView lCommentPendingChars = v.findViewById(R.id.penging_chars_in_comment_assessment);
            TextView lCommentToPublish = v.findViewById(R.id.comment_to_publish);
            TextView lExtraInfo = v.findViewById(R.id.extra_information);
            AppCompatButton lBtnMoreInfo = v.findViewById(R.id.btn_more_info);
            RatingBar lMark = v.findViewById(R.id.mark);
            AppCompatButton lBtnOk = v.findViewById(R.id.btn_ok);
            AppCompatButton lBtnCancel = v.findViewById(R.id.btn_cancel);

            IHandleAlertDialogAccept action = null;

            int newIconResource = R.drawable.ic_rocket;
            SQL.eChatStatus currentStatus = mSQL.getChatStatus(mContactUID);
            SQL.eChatStatus newStatus = currentStatus;
            switch (currentStatus) {
                case STATUS_BEFORE_ANY_ACTION:
                    newStatus = SQL.eChatStatus.STATUS_ONE_PERSON_STARTED_THE_DEAL;
                    newIconResource = R.drawable.ic_before_agreement;
                    lTitle.setText(R.string.tittle_dialog_agreement_request);
                    lExtraInfo.setText(R.string.extra_info_agreement_request);
                    action = (String comment, float mark) -> {

                        String body = getString(R.string.body_msg_agreement_req_sent);

                        ChatMessage lMsg = new ChatMessage(body, System.currentTimeMillis(),
                                ChatMessage.Type.SENT);

                        mChatView.addMessage(lMsg);
                        sendMessage(JsonMessage.AGREEMENT_REQUEST, body);

                        mSQL.setChatInfo(mContactUID, true,
                                SQL.eChatStatus.STATUS_ONE_PERSON_STARTED_THE_DEAL, mChatTopic);
                    };
                    lCommentPendingChars.setVisibility(View.GONE);
                    lCommentToPublish.setVisibility(View.GONE);
                    lMark.setVisibility(View.GONE);
                    break;

                case STATUS_ONE_PERSON_STARTED_THE_DEAL:
                    if (!mSQL.am_I_agreementInitiator(mContactUID)) {
                        // I can accept the agreement then
                        newStatus = SQL.eChatStatus.STATUS_AGREEMENT_REACHED;
                        newIconResource = R.drawable.ic_star;
                        lTitle.setText(R.string.tittle_dialog_agreement_acceptance);
                        lExtraInfo.setText(R.string.extra_info_agreement_acceptance);
                        action = (String comment, float mark) ->
                            {
                                String body = getString(R.string.body_msg_agreement_req_accepted) + mContactName;
                                ChatMessage lMsg = new ChatMessage(body, System.currentTimeMillis(),
                                        ChatMessage.Type.SENT);

                                mChatView.addMessage(lMsg);

                                // body is built in reception
                                sendMessage(JsonMessage.AGREEMENT_ACCEPT, "");
                            };

                        lCommentPendingChars.setVisibility(View.GONE);
                        lCommentToPublish.setVisibility(View.GONE);
                        lMark.setVisibility(View.GONE);
                    }
                    else {
                        Toast.makeText(this, getString(R.string.you_already_sent_agreement_request),
                                Toast.LENGTH_LONG).show();
                        return super.onOptionsItemSelected(item);
                    }
                    break;

                case STATUS_AGREEMENT_REACHED:
                    lCommentToPublish.addTextChangedListener(new TextWatcher() {
                        public void onTextChanged(CharSequence cs, int a, int b, int c)
                        {
                            lCommentPendingChars.setText(
                                    Integer.toString(getResources().getInteger(R.integer.max_length_assessment)
                                    - lCommentToPublish.length()));
                        }

                        public void beforeTextChanged(CharSequence cs, int a, int b, int c) {}

                        public void afterTextChanged(Editable e) {
                        }
                    });

                    newStatus = currentStatus;
                    newIconResource = R.drawable.ic_star; // keep the same
                    lTitle.setText(R.string.tittle_dialog_public_assessment);
                    lExtraInfo.setVisibility(View.GONE);
                    action = (String comment, float mark) -> {
                        if (comment.trim().isEmpty()) {
                            Toast.makeText(this,
                                    getString(R.string.please_write_public_feedback),
                                    Toast.LENGTH_LONG).show();
                        }
                        else {
                            sendFeedback(comment, mark);
                        }
                    };

                    lBtnMoreInfo.setVisibility(View.GONE);
                    break;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(v);

            AlertDialog lChatDialog = builder.create();
            lChatDialog.setCanceledOnTouchOutside(false);
            lChatDialog.setCancelable(false);
            lChatDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            lChatDialog.show();

            final int finalIconRes = newIconResource;
            final IHandleAlertDialogAccept finalAction = action;
            final SQL.eChatStatus finalNewStatus = newStatus;

            lBtnOk.setOnClickListener(v1 -> {
                // topic, mark only needed when user sends feedback
                String comment = lCommentToPublish.getText().toString();
                float mark = lMark.getRating();

                finalAction.onAccept(comment, mark);
                this.mMenuItem.setIcon(finalIconRes);
                mSQL.updateChatStatus(mContactUID, finalNewStatus);
                lChatDialog.dismiss();
            });

            lBtnCancel.setOnClickListener(v1 -> lChatDialog.dismiss());

            ChatActivity lActivity = this;
            lBtnMoreInfo.setOnClickListener(v1 -> {
                AlertDialogBuilder.inflateSpecificLayout(lActivity,
                        R.layout.dialog_assessment_procedure_info,
                        false, v2 -> { /* do nothing */});
            });
        }

        return super.onOptionsItemSelected(item);
    }

    /** Sends a broadcast that will be caught by JabberConnection */
    private void sendMessage(int messageType, String body) {
        // Handle incoming agreement events
        String sendTime = Long.toString(System.currentTimeMillis());

        mSQL.addMessage(mContactUID, body, sendTime, true, mChatTopic, false);
        mSQL.storeContactWithJustName(mContactUID, mContactName);

        Intent i = new Intent(JabberService.SEND_MESSAGE);
        i.putExtra(JabberService.BUNDLE_MESSAGE_TYPE, messageType);
        i.putExtra(JabberService.BUNDLE_MESSAGE_BODY, body);
        i.putExtra(JabberService.BUNDLE_TO, mFullContactJid);
        i.putExtra(JabberService.BUNDLE_SENT_TIME, sendTime);
        i.putExtra(JabberService.BUNDLE_SENDER_NAME, mContactName);
        i.putExtra(JabberService.BUNDLE_CHAT_TOPIC, mChatTopic);
        sendBroadcast(i);
    }

    private void sendFeedback(String comment, float mark) {
        StringRequest deleteUserRequest = new StringRequest(Request.Method.POST,
                HttpTempo.URL_INDEX,
                (response) -> {
                    String respWithoutCR = response.replaceAll("\\n", "");
                    if (respWithoutCR.contains("OK"))
                    {
                        Toast.makeText(this, R.string.assesment_sent_correctly, Toast.LENGTH_SHORT).show();
                        this.mMenuItem.setIcon(R.drawable.ic_rocket); // set the starting point
                        mSQL.updateChatStatus(mContactUID, SQL.eChatStatus.STATUS_BEFORE_ANY_ACTION);

                        String body = getString(R.string.body_msg_assessment_sent);

                        ChatMessage lMsg = new ChatMessage(body, System.currentTimeMillis(),
                                ChatMessage.Type.SENT);

                        mChatView.addMessage(lMsg);

                        // in reception, the 'body' is ignored for that type of messages
                        // and it gets built there because both users may have different locale
                        sendMessage(JsonMessage.ASSESSMENT_DONE, body);
                    }
                    else {
                        Toast.makeText(this, R.string.the_assesment_could_not_be_sent,
                                Toast.LENGTH_LONG).show();
                    }
                }, (volleyError) ->  {

            if (volleyError instanceof TimeoutError) {
                Log.e(TAG, "Error getting JSONArray (sendFeedback)",
                        volleyError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("controller", "User");
                headers.put("action", "add_assessment");
                headers.put("uid", mContactUID);
                headers.put("evaluator_id", AuthManager.instance.getUserId());
                headers.put("evaluator_name", mSQL.getMe().getName());
                headers.put("comment", comment);
                headers.put("mark", Float.toString(mark));
                return headers;
            }

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.IMMEDIATE;
            }
        };

        HttpTempo.getInstance().getRequestQueue(this).add(deleteUserRequest);
    }
}
