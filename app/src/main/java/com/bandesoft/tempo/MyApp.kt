package com.bandesoft.tempo

import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import net.gotev.uploadservice.UploadService

class MyApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        instance = this

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        // The 'gotev' thing is needed to upload files to server.
        // setup the broadcast action namespace string which will
        // be used to notify upload status.
        // Gradle automatically generates proper variable as below.
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID
        UploadService.NAMESPACE = "com.bandesoft.tempo"
    }

    override fun attachBaseContext(context: Context) {
        super.attachBaseContext(context)
        MultiDex.install(this)
    }

    companion object {

        var instance: MyApp? = null
            private set

        val context: Context
            get() = instance!!.applicationContext
    }
}
