package com.bandesoft.tempo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bandesoft.tempo.Model.OtherPic;
import com.bandesoft.tempo.Remote.AuthManager;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Utils.AlertDialogBuilder;
import com.bandesoft.tempo.Utils.ImgLoader;
import com.bandesoft.tempo.Vista.PicsList;

import java.io.File;
import java.util.ArrayList;

import androidx.appcompat.widget.Toolbar;

/**
 * This class is used to both edit the current user profile, but also
 * to simply see the pictures of other user, without editing them.
 */
public class PicsActivity extends BaseActivity
        implements ImageButton.OnClickListener {

    private static final String TAG = "PicsActivity";

    private static final int REQ_RC_GALLERY_INTENT_CALLED_PROFILE_PIC = 1;
    private static final int REQ_RC_GALLERY_KITKAT_INTENT_CALLED_PROFILE_PIC = 2;
    private static final int REQ_RC_GALLERY_INTENT_CALLED_OTHER_PICS = 3;
    private static final int REQ_RC_GALLERY_KITKAT_INTENT_CALLED_OTHER_PICS = 4;

    private Bitmap mProfileBmp = null;
    private ImageView mProfilePic;
    private PicsList mOtherPicList;
    private ArrayList<String> mInitialSetOtherPics = new ArrayList<>();

    private boolean mMyPicHasChanged = false;

    public static final String UID = "uid";
    public static final String WORKER_NAME = "worker_name";
    public static final String PICS_CATEGORY = "pics_category";
    public static final int MY_PICS = 0, OTHER_USER_PICS = 1;
    private int mPicsCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pics);

        Intent callingIntent = getIntent();
        mPicsCategory = callingIntent.getIntExtra(PICS_CATEGORY, MY_PICS);

        Toolbar myToolbar = super.initToolBar();
        if (mPicsCategory == OTHER_USER_PICS) {
            myToolbar.setTitle(callingIntent.getStringExtra(WORKER_NAME));
        }

        mProfilePic = findViewById(R.id.profile_pic);
        mProfilePic.setOnClickListener(this);

        mOtherPicList = new PicsList(this, new ArrayList<>(), this);
        ListView lOtherPics = findViewById(R.id.my_pics_list);
        lOtherPics.setAdapter(mOtherPicList);

        TextView lOtherPicsTitle = findViewById(R.id.other_pic_title);

        if (mPicsCategory == MY_PICS) {
            findViewById(R.id.edit_profile_pic).setOnClickListener(this);
            findViewById(R.id.clear_profile_pic).setOnClickListener(this);
            findViewById(R.id.btn_add_pic).setOnClickListener(this);

            ImgLoader.loadPic(mProfilePic, this, getString(R.string.my_profile_pic_name), outBmp -> {
                mProfileBmp = outBmp;
            });

            // Getting my images names
            for (File file : getFilesDir().listFiles()) {
                if (file.getName().startsWith(AuthManager.instance.getUserId())) {
                    mInitialSetOtherPics.add(file.getName());

                    ImgLoader.loadPic(null, this, file.getName(),
                            (Bitmap outBmp) -> {
                                Bitmap decoded = ImgLoader.getResizedBitmap(outBmp, 512);

                                Log.i(TAG, "file size " + outBmp.getByteCount() + " decoded: " + decoded.getByteCount());
                                mOtherPicList.add(new OtherPic(file.getName(), decoded));
                            });
                }
            }

            if (mInitialSetOtherPics.size() > 0) {
                lOtherPicsTitle.setVisibility(View.VISIBLE);
            }
        }
        else if (mPicsCategory == OTHER_USER_PICS) {
            mOtherPicList.setOtherUserPics(true);

            findViewById(R.id.btn_edition_container).setVisibility(View.GONE);
            findViewById(R.id.btn_add_pic).setVisibility(View.GONE);

            String lOtherWorkerId = callingIntent.getStringExtra(UID);

            String lImgUrl = HttpTempo.URL_DOWNLOAD_IMG + lOtherWorkerId + ".jpeg";
            ImgLoader.downloadPic(this, lImgUrl,
                    () -> {
                        String fileName =
                                lImgUrl.substring(lImgUrl.lastIndexOf("/") + 1,
                                        lImgUrl.lastIndexOf(".jpeg"));

                        ImgLoader.loadPic(null, this, fileName,
                                (Bitmap outBmp) -> {
                                    mProfileBmp = ImgLoader.getResizedBitmap(outBmp, 512);
                                    mProfilePic.setImageBitmap(mProfileBmp);
                                });
                    });

            HttpTempo.getInstance().getUserPicNames(this, lOtherWorkerId,
                    list -> {
                        if (list.size() > 0) {
                            lOtherPicsTitle.setVisibility(View.VISIBLE);
                        }

                        for (String file: list) {

                            ImgLoader.downloadPic(this, file,
                                    () -> {
                                        String fileName =
                                                file.substring(file.lastIndexOf("/") + 1,
                                                        file.lastIndexOf(".jpeg"));

                                        ImgLoader.loadPic(null, this, fileName,
                                                (Bitmap outBmp) -> {
                                                    Bitmap decoded = ImgLoader.getResizedBitmap(outBmp, 512);

                                                    Log.i(TAG, "file size " + outBmp.getByteCount() + " decoded: " + decoded.getByteCount());
                                                    mOtherPicList.add(new OtherPic(fileName, decoded));
                                                });
                                    });
                        }
                    });
        }
        else {
            Toast.makeText(this, "Wrong pics category", Toast.LENGTH_LONG).show();
        }

        ScrollView scrollView = findViewById(R.id.scroll_view);
        // Wait until my scrollView is ready
        scrollView.getViewTreeObserver().addOnGlobalLayoutListener(() ->
                scrollView.fullScroll(View.FOCUS_UP)
        );
    }

    /** This is only aplicable to my user pics */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_profile_pic:
                startImageActivityBrowser(REQ_RC_GALLERY_INTENT_CALLED_PROFILE_PIC,
                        REQ_RC_GALLERY_KITKAT_INTENT_CALLED_PROFILE_PIC);
                break;

            case R.id.btn_add_pic:
                startImageActivityBrowser(REQ_RC_GALLERY_INTENT_CALLED_OTHER_PICS,
                        REQ_RC_GALLERY_KITKAT_INTENT_CALLED_OTHER_PICS);
                break;

            case R.id.clear_profile_pic:
                AlertDialogBuilder.createAndShowAlertDialog(this,
                        R.string.reset_profile_pic_question,
                        R.string.empty,
                        () -> {
                            mProfilePic.setImageDrawable(
                                    getResources().getDrawable(
                                            R.drawable.ic_default_profile_pic));
                            mProfileBmp = null; // to force user pic deletion if user commits the changes
                        }
                );
                break;

            case R.id.remove_other_pic_btn:
                int lPosition = (int) v.getTag();
                AlertDialogBuilder.createAndShowAlertDialog(this,
                        R.string.delete_other_pic_question,
                        R.string.empty,
                        () -> mOtherPicList.deleteAt(lPosition));
                break;

            case R.id.profile_pic: // the profile pic was clicked. Then, show it.
                ImageViewerActivity.mBitmapToShow = mProfileBmp;
                ImgLoader.getBitmap(this, getString(R.string.my_profile_pic_name));
                startActivity(new Intent(this, ImageViewerActivity.class));
                break;

            case PicsList.OTHER_PIC_ID: // other pic was clicked. Then, show it.
                int position = (int) v.getTag();
                OtherPic otherPic = mOtherPicList.getItem(position);

                if (otherPic != null) {
                    ImageViewerActivity.mBitmapToShow = otherPic.getBitmap();
                    startActivity(new Intent(this, ImageViewerActivity.class));
                }
                break;
        }
    }

    /** This is only aplicable to my user pics */
    private void startImageActivityBrowser(int reqIdOlderSdks, int reqIdNewerSdks) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");

        Intent i = Intent.createChooser(intent,
                getResources().getString(R.string.select_profile_pic));

        if (Build.VERSION.SDK_INT < 19) {
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(i, reqIdOlderSdks);
        } else {
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(i, reqIdNewerSdks);
        }
    }

    /** This is only aplicable to my user pics */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btn_ok:
                saveImages();
                setResult(Activity.RESULT_OK);
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mPicsCategory == MY_PICS) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.btn_ok_action_bar, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    /** This is only aplicable to my user pics */
    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK || data == null) {
            return;
        }

        Uri originalUri = null;
        if (requestCode == REQ_RC_GALLERY_INTENT_CALLED_PROFILE_PIC ||
                requestCode == REQ_RC_GALLERY_INTENT_CALLED_OTHER_PICS)
        {
            originalUri = data.getData();
        } else if (requestCode == REQ_RC_GALLERY_KITKAT_INTENT_CALLED_PROFILE_PIC ||
                requestCode == REQ_RC_GALLERY_KITKAT_INTENT_CALLED_OTHER_PICS) {
            originalUri = data.getData();
            final int takeFlags = data.getFlags() & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            if (originalUri != null) {
                getContentResolver().
                        takePersistableUriPermission(originalUri, takeFlags);
            }
//        TODO: https://stackoverflow.com/questions/19834842/android-gallery-on-android-4-4-kitkat-returns-different-uri-for-intent-action
        }

        if (requestCode == REQ_RC_GALLERY_INTENT_CALLED_PROFILE_PIC ||
                requestCode == REQ_RC_GALLERY_KITKAT_INTENT_CALLED_PROFILE_PIC)
        {
            ImgLoader.loadPic(mProfilePic, this, originalUri,
                    (Bitmap outBmp) ->
                        {
                            mProfileBmp = outBmp;
                            mMyPicHasChanged = true;
                        }
                    );
        } else if (requestCode == REQ_RC_GALLERY_INTENT_CALLED_OTHER_PICS ||
                requestCode == REQ_RC_GALLERY_KITKAT_INTENT_CALLED_OTHER_PICS) {

            ImgLoader.loadPic(null, this, originalUri,
                    (Bitmap outBmp) -> {
                        long unixTime = System.currentTimeMillis() / 1000L;
                        String lFileName = AuthManager.instance.getUserId() + "_" + Long.toString(unixTime);
                        mOtherPicList.add(new OtherPic(lFileName, ImgLoader.getResizedBitmap(outBmp, 512)));
                    });
        }
    }

    /** This is only aplicable to my user pics */
    private void saveImages() {
        HttpTempo lHttp = HttpTempo.getInstance();

        // Profile pic management
        String lLocalFileName = getString(R.string.my_profile_pic_name);
        if (mProfileBmp != null) {
            if(mMyPicHasChanged) {
                if (ImgLoader.saveImageToInternalStorage(this, mProfileBmp, lLocalFileName))
                {
                    lHttp.uploadMyImageToServer(this, AuthManager.instance.getUserId(), lLocalFileName,
                            () -> {
                                // my pic was correctly uploaded.
                            });
                } else {
                    Toast.makeText(this, getString(R.string.couldnt_copy_image),
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
        else { // the user pic was reset, then remove it local and remotely
            ImgLoader.deleteMyPic(this, lLocalFileName);

            lHttp.removeUserPic(this, AuthManager.instance.getUserId(), () -> {
                // The user pic was correctly removed remotely
            });
        }

        // Other pics management
        ArrayList<String> lAllFileNames = mOtherPicList.getAllFileNames();

        // Remove images, if any.
        for (String lPicName: mInitialSetOtherPics) {
            if (!lAllFileNames.contains(lPicName)) {
                // remove locally
                ImgLoader.deleteMyPic(this, lPicName);

                // remove remotely
                lHttp.removeUserPic(this, lPicName, () -> {

                });
            }
        }

        // Updating new images getting the list of new images to be stored local and remotely.
        int i = 0;
        for (String lPicName: lAllFileNames) {
            if (!mInitialSetOtherPics.contains(lPicName)) {

                OtherPic otherPic = mOtherPicList.getItem(i);
                if (otherPic != null)
                {
                    String lFileName = otherPic.getFileName();
                    if (ImgLoader.saveImageToInternalStorage(this,
                            otherPic.getBitmap(),
                            otherPic.getFileName())) {
                        lHttp.uploadMyImageToServer(this, lFileName, lFileName, () -> {
                            // the image was correctly uploaded
                        });
                    } else {
                        Toast.makeText(this,
                                getString(R.string.couldnt_copy_image),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
            i++;
        }
    }
}
