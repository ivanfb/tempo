package com.bandesoft.tempo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bandesoft.tempo.Model.LonLatCity;
import com.bandesoft.tempo.Model.SearchFilterSnapshot;
import com.bandesoft.tempo.Model.Worker;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Utils.AlertDialogBuilder;
import com.bandesoft.tempo.Utils.Ctes;
import com.bandesoft.tempo.Utils.Prefs;

import java.io.Serializable;
import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

public class SearchFilterActivity extends BaseActivity
        implements AlertDialogBuilder.I_CategoryDialog {

    public static final String EXTRA_SERVICES = "services";

    private static final int REQ_EDIT_LOCATION = 0;

    private final int MAX_SIZE_BTN_TEXT = 8;
    private final int MAX_SIZE_BTN_CATEGORY_TEXT = 16;

    Button mBtnSeeResults;
    Button mBtnSelectedCategory;
    Button mBtnSelectedLocation;
    TextView mTxtSelectedNeedOrOffer;

    AlertDialog mCategoryDialog;

    ArrayList<Worker> mCurrentFilteredList;

    SearchFilterSnapshot mInitialSearchFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Prefs.instance.init(getApplicationContext());
        saveCurrentSearchPrefs();

        setContentView(R.layout.search_filter_activity);

        Toolbar myToolbar = super.initToolBar();
        myToolbar.setNavigationOnClickListener( v -> {
            setResult(RESULT_CANCELED, new Intent());
            restoreBackSearchPrefs();
            finish();
        });
        getSupportActionBar().setTitle(getString(R.string.title_activity_search_filter));

        mBtnSelectedCategory = findViewById(R.id.selected_category);
        mBtnSelectedCategory.setOnClickListener(
                v -> showCategoryDialog()
        );
        setBtnSearchCategory(Prefs.instance.getSearchTagCategory());

        findViewById(R.id.btn_clear_category_filter).setOnClickListener(
                v -> clearFilter(v));

        mBtnSelectedLocation = findViewById(R.id.btn_select_location);
        mBtnSelectedLocation.setOnClickListener(v -> startMapActivityForResult());
        mBtnSelectedLocation.setCompoundDrawablesWithIntrinsicBounds(null, null,
                getResources().getDrawable(R.drawable.ic_placeholder), null);

        setBtnLocationFromPrefs();

        findViewById(R.id.btn_clear_location_filter).setOnClickListener(
                v -> clearFilter(v));

        findViewById(R.id.btn_clear_all_filters).setOnClickListener(
                v -> clearFilter(v));

        mTxtSelectedNeedOrOffer = findViewById(R.id.txt_i_need_or_offer);
        setNeerOrOfferTextFromPrefs();

        findViewById(R.id.btn_selected_need_or_offer).setOnClickListener(v ->
                {
                    Prefs.instance.swapNeedOffer();
                    setNeerOrOfferTextFromPrefs();

                    setBtnLocationFromPrefs();

                    if (mBtnSelectedCategory != null) {
                        setBtnSearchCategory(Prefs.instance.getSearchTagCategory());
                    }

                    updateFilteredResultsList();
                });

        mBtnSeeResults = findViewById(R.id.btn_see_results);
        mBtnSeeResults.setOnClickListener(v -> {
            Intent i = new Intent();
            i.putExtra(EXTRA_SERVICES, (Serializable) mCurrentFilteredList);
            setResult(Activity.RESULT_OK, i);
            finish();
        });

        updateFilteredResultsList();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        restoreBackSearchPrefs();
    }

    @Override
    public void onClick(View v) {
        if (v instanceof TextView) {
            setBtnSearchCategory((String) v.getTag());

            Prefs.instance.setSearchCategory(mBtnSelectedCategory.getText().toString(),
                    (String) v.getTag());

            updateFilteredResultsList();

            if (mCategoryDialog != null) {
                mCategoryDialog.dismiss();
            }
        }
    }

    @Override
    public void showCategoryDialog() {
        mCategoryDialog = AlertDialogBuilder.showGridCategory(this);
    }

    // temporary save search prefs in case need to rollback (user presses back button)
    private void saveCurrentSearchPrefs() {
        mInitialSearchFilter =
                new SearchFilterSnapshot(Prefs.instance.isSearchOfferedServicesActive(),
                        Prefs.instance.getSearchTagCategory(),
                        Prefs.instance.getSearchCategory(),
                        Prefs.instance.getSearchLoc().getCityName(),
                        Prefs.instance.getSearchLoc().getLat(),
                        Prefs.instance.getSearchLoc().getLon(),
                        Prefs.instance.getSearchLoc().getRadiusKm());
    }

    private void restoreBackSearchPrefs() {
        if (mInitialSearchFilter != null) // can be set to null when clearing filters
        {
            boolean lWasPrevSearchOfferedActive = mInitialSearchFilter.isSearchOfferedActive();
            if (lWasPrevSearchOfferedActive && !Prefs.instance.isSearchOfferedServicesActive()) {
                Prefs.instance.swapNeedOffer();
            }

            Prefs.instance.setSearchCategory(mInitialSearchFilter.getCategorySearch(),
                    mInitialSearchFilter.getTagCategorySearch());

            Prefs.instance.setSearchLocation(mInitialSearchFilter.getLatSearch(),
                    mInitialSearchFilter.getLonSearch(),
                    mInitialSearchFilter.getCitySearch(),
                    mInitialSearchFilter.getRadiusKm());
        }
    }

    private void updateFilteredResultsList() {
        mCurrentFilteredList = new ArrayList<>();

        LonLatCity lSearchLoc = Prefs.instance.getSearchLoc();
        String lSearchTagCat = Prefs.instance.getSearchTagCategory();
        boolean lIOfferSearch = !Prefs.instance.isSearchOfferedServicesActive();

        Activity lActivity = this;

        HttpTempo.getInstance().requestNearServices(this,
                lSearchLoc.getLon(), lSearchLoc.getLat(),
                lSearchTagCat, "", lSearchLoc.getRadiusKm(), lIOfferSearch,
                services -> {
                    mCurrentFilteredList = services;

                    lActivity.runOnUiThread(() -> {
                        mBtnSeeResults.setEnabled(mCurrentFilteredList.size() > 0);
                        int lNumResults = mCurrentFilteredList.size();

                        if (lNumResults > 0) {
                            mBtnSeeResults.setText(getString(R.string.see_results) + " ( " + lNumResults + " )");
                            mBtnSeeResults.setTextColor(getResources().getColor(R.color.colorGreyDark));
                            mBtnSeeResults.setBackgroundColor(getResources().getColor(R.color.colorSalmon));
                        }
                        else {
                            mBtnSeeResults.setText(getString(R.string.text_btn_no_results));
                            mBtnSeeResults.setTextColor(getResources().getColor(R.color.colorGrey));
                            mBtnSeeResults.setBackgroundColor(getResources().getColor(R.color.colorGreySoft));
                        }
                    });
                });
    }

    private void setNeerOrOfferTextFromPrefs() {
        String lNewText = Prefs.instance.isSearchOfferedServicesActive() ?
                getString(R.string.i_need) : getString(R.string.i_offer);

        mTxtSelectedNeedOrOffer.setText(lNewText + " ...");
    }

    private void setBtnSearchCategory(String lTagCategory) {
        String lCategory = Ctes.translateCategoryFromTag(lTagCategory);

        Integer lDrawableId = Ctes.getDrawableFromTagCategory(lTagCategory);
        mBtnSelectedCategory.setCompoundDrawablesWithIntrinsicBounds(null, null,
                getResources().getDrawable(lDrawableId), null);

        if (lCategory.length() > MAX_SIZE_BTN_CATEGORY_TEXT) {
            lCategory = lCategory.substring(0, MAX_SIZE_BTN_CATEGORY_TEXT) + ".";
        }

        mBtnSelectedCategory.setText(lCategory);
    }

    private void setBtnLocationFromPrefs() {
        LonLatCity lLoc = Prefs.instance.getSearchLoc();
        setBtnLocationText(lLoc.getCityName(), lLoc.getRadiusKm());
    }

    private void clearFilter(View v) {
        switch(v.getId())
        {
            case R.id.btn_clear_all_filters:
                setBtnSearchCategory(getString(R.string.tag_cat_any));
                mBtnSelectedLocation.setText(getString(R.string.location));
                Prefs.instance.clearSearchData();
                break;

            case R.id.btn_clear_category_filter:
                setBtnSearchCategory(getString(R.string.tag_cat_any));
                Prefs.instance.clearCategorySearchData();
                break;

            case R.id.btn_clear_location_filter:
                mBtnSelectedLocation.setText(getString(R.string.location));
                Prefs.instance.clearLocationSearchData();
                break;
        }

        updateFilteredResultsList();
    }

    private void startMapActivityForResult() {
        LonLatCity lSearchLocation = Prefs.instance.getSearchLoc();

        Intent i = new Intent(this, MapActivity.class);
        i.putExtra(MapActivity.EXTRA_CITY_NAME, lSearchLocation.getCityName());
        i.putExtra(MapActivity.EXTRA_LAT, lSearchLocation.getLat());
        i.putExtra(MapActivity.EXTRA_LON, lSearchLocation.getLon());
        i.putExtra(MapActivity.EXTRA_RADIUS, lSearchLocation.getRadiusKm());

        startActivityForResult(i, REQ_EDIT_LOCATION);
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK || data == null) {
            return;
        }

        if (requestCode == REQ_EDIT_LOCATION) {
            Bundle b = data.getExtras();
            if (b != null) {
                String lCity = b.getString(MapActivity.RESULT_POS_TEXT);
                Double lLat = b.getDouble(MapActivity.RESULT_POS_LAT);
                Double lLon = b.getDouble(MapActivity.RESULT_POS_LON);
                int lRadiusKm = b.getInt(MapActivity.RESULT_RADIUS);

                setBtnLocationText(lCity, lRadiusKm);

                Prefs.instance.setSearchLocation(lLat, lLon, lCity, lRadiusKm);
                updateFilteredResultsList();
            }
        }
    }

    private void setBtnLocationText(String lCityName, int lRadiusKm) {

        if (mBtnSelectedLocation != null
                &&
                getString(R.string.location).compareTo(lCityName) != 0) {
            // only add "(km)" if we have a city != than default text

            if (lCityName.length() > MAX_SIZE_BTN_TEXT) {
                lCityName = lCityName.substring(0, MAX_SIZE_BTN_TEXT) + ".";
            }

            mBtnSelectedLocation.setText(lCityName + " (" + lRadiusKm + "km)");
        }
    }
}