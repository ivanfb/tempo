package com.bandesoft.tempo.Remote;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bandesoft.tempo.Model.Contact;
import com.bandesoft.tempo.Model.Opinion;
import com.bandesoft.tempo.Model.Service;
import com.bandesoft.tempo.Model.Worker;
import com.bandesoft.tempo.Utils.ThreadPool;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import androidx.collection.LruCache;

public class HttpTempo {
    private static final String TAG = "HttpTempo";

    private static HttpTempo mInstance;
    private RequestQueue mRequestQueue;

    // TODO serialization version
    private final int mSerializationVersion = 1;

//    private static final String HOST = "http://35.242.198.191/"; // this is the Compute Engine instance
    private static final String HOST = "https://tempo-213421.appspot.com/"; // this is the AppEngine instance

    public static final String URL_DOWNLOAD_IMG = "https://storage.googleapis.com/staging.tempo-213421.appspot.com/users_pics/";

    public static final String URL_SEARCH_SERVICES = HOST +
            "index.php?action=get_close_services&controller=Search";

    public static final String URL_SEARCH_OPINIONS = HOST +
            "index.php?action=get_user_opinions&controller=Search";

    public static final String URL_GET_USER_INFO = HOST +
            "index.php?action=get_user_info&controller=User";

    public static final String URL_GET_CONTACTS_LIST = HOST +
            "index.php?action=get_my_contacts_list&controller=User";

    public static final String URL_GET_USER_OTHER_PICS_NAMES = HOST +
            "index.php?action=get_user_other_pic_names&controller=User";

    private static final String URL_SEARCH_FIREBASE_TOKEN_ID = HOST +
            "index.php?action=get_firebase_token_id&controller=Search";

    public static final String URL_INDEX = HOST + "index.php";

    private static final String URL_SAVE_USER_INFO = HOST + "saveUserInfo.php";
    private static final String URL_SAVE_CONTACT_INFO = HOST + "saveContactInfo.php";
    private static final String URL_REMOVE_CONTACTS_INFO = HOST + "removeContacts.php";
    private static final String URL_SAVE_USER_PIC = HOST + "saveUserPic.php";
    private static final String URL_SAVE_FIREBASE_TOKEN = HOST + "saveFirebaseToken.php";
    private static final String URL_SAVE_USER_ERROR = HOST + "saveUserError.php";

    private HttpTempo() {
        new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final static int SIZE = 1024 * 1024; // 1MB
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(SIZE);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    public static synchronized HttpTempo getInstance() throws NullPointerException {
        if (mInstance == null) {
            mInstance = new HttpTempo();
        }

        return mInstance;
    }

    public interface HttpRespOkHandler {
        void onRespOk();
    }

    public interface HttpRespErrorHandler {
        void onRespErr();
    }

    public interface HttpRespOkJSONArrayHandler {
        void onRespOk(ArrayList<String> list);
    }

    public interface HttpRespWorkersList {
        void onNearServicesOk(ArrayList<Worker> services);
    }

    /**
     * This request should be done at the beginning and asks for
     * the services offered by people close to the user.
     */
    public void requestNearServices(Context context,
                                    double lon, double lat, String tagCategory,
                                    String textToSearch, int radiusKm, boolean iOfferSearch,
                                    HttpRespWorkersList respNearServHandler) {
        // For debugging purposes
//        StringRequest lStringReq = new StringRequest(HttpTempo.URL_SEARCH_SERVICES +
//                "&user_id=" + AuthManager.instance.getUserId() +
//                "&lon=" + Double.toString(lon) +
//                "&lat=" + Double.toString(lat) +
//                "&bool_i_offer=" + (iOfferSearch ? "1":"0") +
//                "&category=" + tagCategory +
//                "&kilometers=30" +
//                "&text_to_search=" + textToSearch,
//                response -> {
//                    String a = response;
//                },
//                error -> {
//                    String a = error.getMessage();
//                }
//          );
//        HttpTempo.getInstance().getRequestQueue().add(lStringReq);

        String GET_URL = HttpTempo.URL_SEARCH_SERVICES +
                "&user_id=" + AuthManager.instance.getUserId() +
                "&lon=" + lon +
                "&lat=" + lat +
                "&bool_i_offer=" + (iOfferSearch ? "1":"0") +
                "&category=" + tagCategory +
                "&kilometers=" + radiusKm +
                "&text_to_search=" + textToSearch.trim().replaceAll(" ", "%20");

        JsonArrayRequest jsonArrayRequest =
                new JsonArrayRequest(Request.Method.GET,
                        GET_URL,
                        null,
                        response ->
                        {
                            ArrayList<Worker> lRet = new ArrayList<>();

                            int lNumWorkers = response.length();
                            try {
                                for (int i = 0; i < lNumWorkers; i++) {
                                    JSONObject jo = response.getJSONObject(i);
                                    Worker w = new Worker(jo.getString("uid"),
                                            jo.getString("name"),
                                            jo.getString("location"), // TODO
                                            (float) jo.getDouble("average_mark"),
                                            jo.getInt("num_votes"), 0, 0,
                                            jo.getString("pics_change_time"));

                                    JSONArray services = jo.getJSONArray("services");

                                    for (int a = 0; a < services.length(); a++) {
                                        JSONObject s = services.getJSONObject(a);

                                        boolean isANeed = s.getInt("is_a_need") == 1;

                                        Service service =
                                                new Service(s.getString("category"),
                                                        s.getString("detailed_descr"),
                                                        isANeed);
                                        if (isANeed) {
                                            w.addNeed(service);
                                        }
                                        else {
                                            w.addService(service);
                                        }
                                    }

                                    lRet.add(w);
                                }
                            } catch (org.json.JSONException je) {
                                Log.e(TAG, "Error parsing response (requestNearServices)", je);
                            }

                            respNearServHandler.onNearServicesOk(lRet);

                        }, error ->
                        Log.e(TAG, "Error getting JSONArray (requestNearServices)", error)
                );

        // Access the RequestQueue through your singleton class.
        HttpTempo.getInstance().getRequestQueue(context).add(jsonArrayRequest);
    }

    /** Sends my information to be stored in database
     * @param me */
    public void sendUserInformation(Worker me,
                                    HttpRespOkHandler okRespHandler) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("user_id", me.getUid());
            jsonObject.put("user_name", me.getName());
            jsonObject.put("location", me.getLocation().getCityName());
            jsonObject.put("lon", Double.toString(me.getLocation().getLon()));
            jsonObject.put("lat", Double.toString(me.getLocation().getLat()));

            JSONArray services = new JSONArray();
            for (Service a : me.getAbilities()) {
                JSONObject jsonAbility = new JSONObject();

                jsonAbility.put("category", a.getCategory());
                jsonAbility.put("detailed_desc", a.getDetailedDescr());
                jsonAbility.put("is_a_need", "0");

                services.put(jsonAbility);
            }

            for (Service a : me.getNeeds()) {
                JSONObject jsonAbility = new JSONObject();

                jsonAbility.put("category", a.getCategory());
                jsonAbility.put("detailed_desc", a.getDetailedDescr());
                jsonAbility.put("is_a_need", "1");

                services.put(jsonAbility);
            }

            jsonObject.put("services", services);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendPostRequest(URL_SAVE_USER_INFO, jsonObject, () -> okRespHandler.onRespOk());
    }

    public void sendUserError(String errorMessage, HttpRespOkHandler okRespHandler) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("user_id", AuthManager.instance.getUserId());
            jsonObject.put("message", errorMessage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendPostRequest(URL_SAVE_USER_ERROR, jsonObject, () -> okRespHandler.onRespOk());
    }

    public void saveMyContacts(String lUID, Collection<Contact> lContacts,
                               HttpRespOkHandler okRespHandler) {
        updateMyContacts(true, lUID, lContacts, okRespHandler);
    }

    public void removeContacts(String lUID, Collection<Contact> lContacts,
                               HttpRespOkHandler okRespHandler) {
        updateMyContacts(false, lUID, lContacts, okRespHandler);
    }

    /**
     * @param lSave lSave == false, the contact list will be removed. Else, contacts are added.
     * @param lUID
     * @param lContacts
     * @param okRespHandler
     */
    private void updateMyContacts(boolean lSave, String lUID,
                                  Collection<Contact> lContacts,
                              HttpRespOkHandler okRespHandler) {
        JSONObject jsonObject = new JSONObject();

        Log.d(TAG, "sendMyContacts");
        try {
            jsonObject.put("user_id", lUID);

            Log.d(TAG, "user_id: " + lUID);

            JSONArray contacts = new JSONArray();
            for (Contact lContact : lContacts) {
                if (lSave) {
                    JSONObject jo = new JSONObject();
                    jo.put("contact_id", lContact.getUid());
                    jo.put("service", lContact.getServiceTopic());

                    Log.d(TAG, "contact id: " + lContact.getUid());
                    contacts.put(jo);
                }
                else {
                    contacts.put(lContact.getUid());
                }
            }

            jsonObject.put("contacts", contacts);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (lSave) {
            sendPostRequest(URL_SAVE_CONTACT_INFO, jsonObject, () -> okRespHandler.onRespOk());
        }
        else {
            sendPostRequest(URL_REMOVE_CONTACTS_INFO, jsonObject, () -> okRespHandler.onRespOk());
        }
    }

    public void uploadFirebaseToken(String token) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("user_id", AuthManager.instance.getUserId()); // Tempo user id
            jsonObject.put("token", token); // Firebase token id
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // TODO maybe worth checking if token wasn't uploaded properly
        sendPostRequest(URL_SAVE_FIREBASE_TOKEN, jsonObject, null);
    }

    public void sendPendingMsgsPushNotificationToUser(Context context,
                                                      String uid, String name, String chatTopic) {
        StringRequest notifyUserByFirebase = new StringRequest(Request.Method.POST,
                HttpTempo.URL_INDEX,
                (response) -> {
                    String respWithoutCR = response.replaceAll("\\n", "");
                    int i = 0;
                }, (volleyError) ->  {

            if (volleyError instanceof TimeoutError) {
                Log.e(TAG, "Error getting JSONArray (sendFeedback)",
                        volleyError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("uid", uid);
                headers.put("controller", "User");
                headers.put("action", "send_firebase_notif");
                headers.put("sender_id", AuthManager.instance.getUserId());
                headers.put("sender_name", name);
                headers.put("chat_topic", chatTopic);
                return headers;
            }

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.IMMEDIATE;
            }
        };

        HttpTempo.getInstance().getRequestQueue(context).add(notifyUserByFirebase);
    }

    public void removeUserAccountRequest(Context context,
                                         String uid,
                                         HttpRespOkHandler respHandler,
                                         HttpRespErrorHandler respErrorHandler) {

        StringRequest removeAccountRequest = new StringRequest(Request.Method.POST,
                HttpTempo.URL_INDEX,
                (response) -> {
                    String respWithoutCR = response.replaceAll("\\n", "");
                    if (respWithoutCR.contains("OK"))
                    {
                        respHandler.onRespOk();
                    }
                    else {
                        respErrorHandler.onRespErr();
                    }
                }, (volleyError) ->  respErrorHandler.onRespErr())
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("controller", "User");
                headers.put("action", "remove_account");
                headers.put("uid", uid);
                return headers;
            }

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.IMMEDIATE;
            }
        };

        HttpTempo.getInstance().getRequestQueue(context).add(removeAccountRequest);
    }

    public void removeUserPic(Context context,
                              String picName, HttpRespOkHandler okRespHandler) {
        StringRequest removeAccountRequest = new StringRequest(Request.Method.POST,
                HttpTempo.URL_INDEX,
                (response) -> {
                    String respWithoutCR = response.replaceAll("\\n", "");
                    if (respWithoutCR.contains("OK"))
                    {
                        okRespHandler.onRespOk();
                    }
                }, (volleyError) ->  {/*respErrorHandler.onRespErr()*/})
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("controller", "User");
                headers.put("action", "remove_user_pic");
                headers.put("pic_name", picName);
                return headers;
            }

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.IMMEDIATE;
            }
        };

        HttpTempo.getInstance().getRequestQueue(context).add(removeAccountRequest);
    }

    public void getUserPicNames(Context context,
                                String uid, HttpRespOkJSONArrayHandler respOkHandler) {
        String GET_URL = URL_GET_USER_OTHER_PICS_NAMES + "&uid=" + uid;

//        StringRequest lStringReq = new StringRequest(GET_URL,
//                response -> {
//                    String a = response;
//                },
//                error -> {
//
//                }
//        );
//        HttpTempo.getInstance().getRequestQueue().add(lStringReq);

        JsonObjectRequest jsonArrayRequest =
                new JsonObjectRequest(Request.Method.GET,
                        GET_URL,
                        null,
                        response ->
                        {
                            try {
                                ArrayList<String> lRet = new ArrayList<>();

                                // received opinions
                                JSONArray lFiles =
                                        (JSONArray)
                                                response.get("files");

                                ArrayList<Opinion> userOpinions = new ArrayList<>();
                                int lNumFiles = lFiles.length();
                                for (int i = 0; i < lNumFiles; i++) {
                                    lRet.add(lFiles.getString(i));
                                }

                                respOkHandler.onRespOk(lRet);
                            }
                            catch (JSONException e) {
                                Log.e(TAG, "Error parsing opinions response [" +
                                        e.getMessage() + " ]");
                            }

                        }, error ->
                        Log.e(TAG, "Error getting JSONArray (requestOpinions)", error)
                );

        // Access the RequestQueue through your singleton class.
        HttpTempo.getInstance().getRequestQueue(context).add(jsonArrayRequest);
    }

    private void sendPostRequest(String targetUrl,
                                 JSONObject inJson,
                                 HttpRespOkHandler respHandler) {

        ThreadPool.instance.runTask(() ->
        {
            OutputStream os = null;
            InputStream is = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL(targetUrl);

                String message = inJson.toString();

                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /*milliseconds*/);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setFixedLengthStreamingMode(message.getBytes().length);

                conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
                conn.connect();

                //setup send
                os = new BufferedOutputStream(conn.getOutputStream());
                os.write(message.getBytes());
                //clean up
                os.flush();

                //do somehting with response
                is = conn.getInputStream();
                //String contentAsString = readIt(is,len);

                int responseCode = conn.getResponseCode();

                switch (responseCode) {
                    case 200:
                        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        if (response.indexOf("OK") == 0) {
                            if (respHandler != null) {
                                respHandler.onRespOk();
                            }
                        }
                        else
                        {
                            Log.e(TAG, "sendPostRequest [" + response + "]");
                        }

                        in.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (os != null) {
                        os.close();
                    }
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (conn != null) {
                    conn.disconnect();
                }
            }

        });
    }

    public RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }

    /** Uploads the user personal pic to server */
    public void uploadMyImageToServer(Context context,
                                      String remoteFileName,
                                      String localFileName,
                                      HttpRespOkHandler okRespHandler) {
        try {
            String uploadId = UUID.randomUUID().toString();

            new MultipartUploadRequest(context, uploadId, URL_SAVE_USER_PIC)
                    .addFileToUpload(context.getFilesDir().getAbsolutePath() + "/" +
                            localFileName,
                            "my_pic_file")
                    .addParameter("file_name", remoteFileName)
                    .addParameter("user_id", AuthManager.instance.getUserId())
                    .setMaxRetries(2)
                    .setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onError(Context context, UploadInfo uploadInfo,
                                            ServerResponse serverResponse, Exception exception) {

                            Log.e(TAG, "Error uploading user pic file");
                        }

                        @Override
                        public void onCompleted(Context context, UploadInfo uploadInfo,
                                                ServerResponse serverResponse) {
                            Log.i(TAG, "completed");
                            if (serverResponse.getHttpCode() == 200 &&
                                serverResponse.getBodyAsString().contains("OK"))
                            {
                              //TODO  mPendingUpdateUserInfo = false;
                                okRespHandler.onRespOk();
                            }
                            else {
                                Log.i(TAG, serverResponse.getBodyAsString());
                            }
                        }

                        @Override
                        public void onProgress(Context context, UploadInfo uploadInfo) {
                        }

                        @Override
                        public void onCancelled(Context context, UploadInfo uploadInfo) {
                            Log.e(TAG, "Upload pic cancelled");
                        }
                    })
                    .startUpload();

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    private static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public static boolean hasInternetAccess(Context context) {
        if (isNetworkAvailable(context)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection)
                        (new URL("https://clients3.google.com/generate_204")
                                .openConnection());
                urlc.setRequestProperty("User-Agent", "Android");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 204 &&
                        urlc.getContentLength() == 0);
            } catch (Exception e) {
                Log.e(TAG, "Error checking internet connection " + e.getMessage());
            }
        } else {
            Log.d(TAG, "No network available!");
        }
        return false;
    }

    public void updateMyLastAccessDate(Context context,
                                       String uid,
                                       HttpRespOkHandler respHandler,
                                       HttpRespErrorHandler respErrorHandler) {

        StringRequest updateMyLastAccessTime = new StringRequest(Request.Method.POST,
                HttpTempo.URL_INDEX,
                (response) -> {
                    String respWithoutCR = response.replaceAll("\\n", "");
                    if (respWithoutCR.contains("OK"))
                    {
                        respHandler.onRespOk();
                    }
                    else {
                        respErrorHandler.onRespErr();
                    }
                }, (volleyError) ->  respErrorHandler.onRespErr())
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("controller", "User");
                headers.put("action", "update_last_time_access");
                headers.put("uid", uid);
                return headers;
            }

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.IMMEDIATE;
            }
        };

        HttpTempo.getInstance().getRequestQueue(context).add(updateMyLastAccessTime);
    }
}
