package com.bandesoft.tempo.Remote;

import com.bandesoft.tempo.Model.NotDeliveredMessage;
import com.bandesoft.tempo.Model.SQL;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class MessageDeliveryCache {

    private static final int MAX_NUM_RX_MESSAGES = 10; // only have control of the latest messages

    // Keep a control of the messages that didn't generated
    // a call to 'JabberConnection.onReceiptReceived', i.e. the
    // received didn't receive the message
    // Key: The stanzaId of the message that wasn't delivered properly
    private HashMap<String, NotDeliveredMessage> mNotDeliveredMsgs = new HashMap<>();

    // Per each user id, contains a list of the last messages IDs
    // (stanzas IDs). This is needed to make sure not to show the
    // same message multiple times
    // Key: The "other" User Id
    private HashMap<String, LinkedList<String>> mReceivedMsgs = new HashMap<>();

    private SQL mSQL; // instance to the sqlite

    private MessageDeliveryCache() {}

    public MessageDeliveryCache(SQL sql) {
        mSQL = sql;
    }

    public boolean isMessageAlreadyReceived(String uid, String stanzaId) {
        return mReceivedMsgs.containsKey(uid) && mReceivedMsgs.get(uid).contains(stanzaId);
    }

    public void addNewIncomingMessage(String uid, String stanzaId) {
        if (!mReceivedMsgs.containsKey(uid)) {
            mReceivedMsgs.put(uid, new LinkedList<>());
        }

        LinkedList<String> stanzas = mReceivedMsgs.get(uid);
        stanzas.add(stanzaId);
        if (stanzas.size() > MAX_NUM_RX_MESSAGES) {
            stanzas.removeFirst();
        }
    }

    public void persistMessagesIntoSQL() {
        mSQL.clearAllRxXmppMessages();
        for (String uid: mReceivedMsgs.keySet()) {
            for (String stanzaId : mReceivedMsgs.get(uid)) {
                mSQL.addRxXmppMessage(uid, stanzaId);
            }
        }

        mSQL.clearAllTxXmppMessages();
        for (Map.Entry<String, NotDeliveredMessage> m :
                mNotDeliveredMsgs.entrySet()) {
            String stanzaId = m.getKey();
            NotDeliveredMessage ndm = m.getValue();

            mSQL.addTxXmppMessage(stanzaId, ndm.getRawJson(), ndm.getToJid(), ndm.getSendTime());
        }
    }

    public void retrieveDataDromSQL() {
        mReceivedMsgs = mSQL.getAllRxXmppMessages();
        mNotDeliveredMsgs = mSQL.getAllTxXmppMessages();
    }

    public void addNewSentMessage(String rawJson, String toJid, String sendTime, String stanzaId) {
        mNotDeliveredMsgs.put(stanzaId, new NotDeliveredMessage(rawJson, toJid, sendTime));
    }

    public void considerMessageAsDelivered(String stanzaId) {
        mNotDeliveredMsgs.remove(stanzaId);
    }

    public HashMap<String, NotDeliveredMessage> getNotDeliveredMsgs() {
        return mNotDeliveredMsgs;
    }
}
