package com.bandesoft.tempo.Remote;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.bandesoft.tempo.MainActivity;
import com.bandesoft.tempo.Model.Service;
import com.bandesoft.tempo.Model.ContactsViewModel;
import com.bandesoft.tempo.Model.LonLatCity;
import com.bandesoft.tempo.Model.SQL;
import com.bandesoft.tempo.R;
import com.bandesoft.tempo.Service.JabberService;
import com.bandesoft.tempo.Service.MyMessagingService;
import com.bandesoft.tempo.Utils.ImgLoader;
import com.bandesoft.tempo.Utils.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidParameterSpecException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;

public class AuthManager {
    private static final String TAG = "AuthManager";

    private static final String EMAIL_FILE = "email_file";
    private static final String PASSWORD_FILE = "paswd_file";

    public static final String INVALID_USER = "-1";

    private byte[] MASTER_PASSWD = {0, -88, 1, 19, 21, -101, -32, 67, -18, -71, 44, (byte) 192, (byte) 190, -61, -33, -98};
    private byte[] mEncryptedEmail = null;
    private byte[] mEncryptedPassword = null;

    private static final int MIN_PASS_LENGHT = 8;

    /** Identifies the user. It's obtained from credentials table
     * from encrypted mail/password  */
    private String mUserId = INVALID_USER;

    private boolean mSignUpProcessStarted = false; // to avoid multiple simultaneous requests

    private String myName;

    private SQL mSQL;

    private boolean mInitialized = false;

    public static AuthManager instance = new AuthManager();

    private AlertDialog mRegisterDialog, mLoginDialog, mForgotPasswordDialog, mRestorePasswordDialog;

    private MainActivity mMainActivity;

    // intended to perform any desirable action that needs to check user registration previously
    public interface I_CallbackSuccessfulRegistration {
        void run();
    }

    private AuthManager() { }

    public void init (Context context) {
        if (context instanceof MainActivity) {
            mMainActivity = (MainActivity) context;
        }

        if (!mInitialized) {
            Prefs.instance.init(context);
            mUserId = Prefs.instance.getUserId();
            mEncryptedEmail = getEncryptedEmail(context);
            mEncryptedPassword = getEncryptedPassword(context);
            mSQL = SQL.getInstance(context);

            mInitialized = true;
        }
    }

    private void askUserForSignUp(Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_signing, null);
        TextView lName = v.findViewById(R.id.user_name);
        TextView lEmail = v.findViewById(R.id.email_address);
        EditText lPassword = v.findViewById(R.id.password);

        ImageButton lBtnShowHidePass = v.findViewById(R.id.btn_show_hide_pass);
        lBtnShowHidePass.setTag(Boolean.valueOf(false) /*clicked*/);

        AppCompatButton lBtnOk = v.findViewById(R.id.btn_ok_sign_up);
        AppCompatButton lBtnCancel = v.findViewById(R.id.btn_cancel_sign_up);
        AppCompatButton lBtnOrLogin = v.findViewById(R.id.or_login_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(v);

        mRegisterDialog = builder.create();
        mRegisterDialog.setCanceledOnTouchOutside(false);
        mRegisterDialog.setCancelable(false);
        mRegisterDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        mRegisterDialog.show();

        lBtnOk.setOnClickListener(view ->
            {
                myName = lName.getText().toString();
                String m = lEmail.getText().toString();
                String p = lPassword.getText().toString();

                if (mSignUpProcessStarted) {
                    Toast.makeText(activity,
                            activity.getString(R.string.sign_up_request_already_made),
                            Toast.LENGTH_SHORT).show();
                }
                else if (myName.isEmpty()) {
                    Toast.makeText(activity,
                            activity.getString(R.string.please_provide_valid_name),
                            Toast.LENGTH_SHORT).show();
                }
                else if (m.isEmpty() || !m.contains("@")) {
                    Toast.makeText(activity,
                            activity.getString(R.string.please_provide_valid_email),
                            Toast.LENGTH_SHORT).show();
                }
                else if (m.contains(" ") || p.contains(" ")) {
                    Toast.makeText(activity,
                            activity.getString(R.string.please_make_sure_you_do_not_use_spaces),
                            Toast.LENGTH_SHORT).show();
                }
                else if (p.length() < MIN_PASS_LENGHT) {
                    Toast.makeText(activity,
                            activity.getString(R.string.please_provide_longer_password),
                            Toast.LENGTH_SHORT).show();
                }
                else {
                    if(encryptEmailAndPassword(m, p)) {
                        signUpUser(activity);
                    }
                }
            });

        lBtnCancel.setOnClickListener(v1 -> {
            mRegisterDialog.dismiss();
            mSignUpProcessStarted = false;
        });

        lBtnOrLogin.setOnClickListener(v1 ->
            {
                mRegisterDialog.dismiss();
                askUserForLogin(activity);
            });

        lBtnShowHidePass.setOnClickListener(v1 -> {
            if (((Boolean) lBtnShowHidePass.getTag()) == false)
            {
                lPassword.setTransformationMethod(HideReturnsTransformationMethod
                        .getInstance());
                lBtnShowHidePass.setImageResource(R.drawable.ic_eye);
                lBtnShowHidePass.setTag(Boolean.valueOf(true));
            }
            else {
                lPassword.setTransformationMethod(PasswordTransformationMethod
                        .getInstance());
                lBtnShowHidePass.setImageResource(R.drawable.ic_eye_closed);
                lBtnShowHidePass.setTag(Boolean.valueOf(false));
            }

            lPassword.setSelection(lPassword.getText().length()); // caret at the end
        });
    }

    public void askUserToValidateEmail(Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_info_user_email_to_send, null);
        TextView lComment = v.findViewById(R.id.email_address_comment);
        lComment.setText(R.string.please_review_your_email);

        TextView lEmail = v.findViewById(R.id.email_address);
        lEmail.setText(getEmailAddress());

        AppCompatButton lBtnReviewNow = v.findViewById(R.id.btn_review_now);
        AppCompatButton lBtnReviewLater = v.findViewById(R.id.btn_review_later);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(v);

        AlertDialog lValidateEmailDialog = builder.create();
        lValidateEmailDialog.setCanceledOnTouchOutside(false);
        lValidateEmailDialog.setCancelable(false);
        lValidateEmailDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        lValidateEmailDialog.show();

        lBtnReviewNow.setOnClickListener(view ->
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_APP_EMAIL);
            activity.startActivity(Intent.createChooser(intent,
                    activity.getString(R.string.select_email_client)));

            lValidateEmailDialog.dismiss();
            mSignUpProcessStarted = false;
        });

        lBtnReviewLater.setOnClickListener(v1 -> {
            lValidateEmailDialog.dismiss();
            mSignUpProcessStarted = false;
        });
    }

    private void askUserForLogin(Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_login, null);
        TextView lEmail = v.findViewById(R.id.email_address);
        EditText lPassword = v.findViewById(R.id.password);

        ImageButton lBtnShowHidePass = v.findViewById(R.id.btn_show_hide_pass);
        lBtnShowHidePass.setTag(Boolean.valueOf(false) /*clicked*/);

        AppCompatButton lBtnOk = v.findViewById(R.id.btn_ok_sign_up);
        AppCompatButton lBtnCancel = v.findViewById(R.id.btn_cancel_sign_up);
        AppCompatButton lBtnForgotPass = v.findViewById(R.id.forgot_password_button);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(v);

        mLoginDialog = builder.create();
        mLoginDialog.setCanceledOnTouchOutside(false);
        mLoginDialog.setCancelable(false);
        mLoginDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        mLoginDialog.show();

        lBtnOk.setOnClickListener(view ->
        {
            String m = lEmail.getText().toString();
            String p = lPassword.getText().toString();

            if (m.isEmpty() || !m.contains("@")) {
                Toast.makeText(activity,
                        activity.getString(R.string.please_provide_valid_email),
                        Toast.LENGTH_SHORT).show();
            }
            else if (m.contains(" ") || p.contains(" ")) {
                Toast.makeText(activity,
                        activity.getString(R.string.please_make_sure_you_do_not_use_spaces),
                        Toast.LENGTH_SHORT).show();
            }
            else if (p.length() < MIN_PASS_LENGHT) {
                Toast.makeText(activity,
                        activity.getString(R.string.please_provide_longer_password),
                        Toast.LENGTH_SHORT).show();
            }
            else {
                encryptEmailAndPassword(m, p);
                loginUser(activity, () ->
                    // An error occurred. Or the user doesn't exist
                    Toast.makeText(activity,
                            activity.getString(R.string.email_or_password_incorrect),
                            Toast.LENGTH_SHORT).show()
                );
            }
        });

        lBtnCancel.setOnClickListener(v1 -> mLoginDialog.dismiss());

        lBtnForgotPass.setOnClickListener(v1 ->
        {
            mLoginDialog.dismiss();
            askToRestorePassword(activity);
        });

        lBtnShowHidePass.setOnClickListener(v1 -> {
            if (((Boolean) lBtnShowHidePass.getTag()) == false)
            {
                lPassword.setTransformationMethod(HideReturnsTransformationMethod
                        .getInstance());
                lBtnShowHidePass.setImageResource(R.drawable.ic_eye);
                lBtnShowHidePass.setTag(Boolean.valueOf(true));
            }
            else {
                lPassword.setTransformationMethod(PasswordTransformationMethod
                        .getInstance());
                lBtnShowHidePass.setImageResource(R.drawable.ic_eye_closed);
                lBtnShowHidePass.setTag(Boolean.valueOf(false));
            }

            lPassword.setSelection(lPassword.getText().length()); // caret at the end
        });
    }

    public boolean isUserRegistered() {
        if (!Prefs.instance.isUserRegisteredInJabber()) {
            Log.e(TAG, "User is not properly registered in jabber server");
            return false;
        }

        if (mEncryptedEmail == null) {
            Log.e(TAG, "The encrypted email is null");
            return false;
        }

        if (mEncryptedPassword == null) {
            Log.e(TAG, "The encrypted password is null");
            return false;
        }

        if (mUserId.equals(INVALID_USER)) {
            Log.e(TAG, "User id is invalid");
            return false;
        }

        return true;
    }

    public boolean isUserLoggedIn() {
        return ! mUserId.equals(INVALID_USER);
    }

    public void askUserForSigningUpIfNeeded(Activity activity,
                                            I_CallbackSuccessfulRegistration callback) {
        if (!isUserRegistered()) {
            askUserForSignUp(activity);
        }
        else if (!Prefs.instance.isEmailAddressValidated()) {
            askUserToValidateEmail(activity);
        }
        else {
            // directly performs the action that needed the user to be correctly registered
            callback.run();
        }
    }

    public String getUserId() { return mUserId; }

    private void loginUser(Activity activity, HttpTempo.HttpRespErrorHandler onError) {
        if (mEncryptedEmail.length == 0 || mEncryptedPassword.length == 0) {
            return;
        }

        StringRequest lStringReq = new StringRequest(
                Request.Method.POST,
                HttpTempo.URL_INDEX,
                response -> {
                    try {
                        if (response.contains("INVALID_CREDENTIALS")) {
                            onError.onRespErr();
                            return;
                        }

                        JSONObject obj = new JSONObject(response.replaceAll("\n", ""));

                        mUserId = obj.getString("uid");
                        mSQL.addMeAtLogin(mUserId,
                                obj.getString("name"),
                                obj.getString("location"),
                                obj.getDouble("lat"),
                                obj.getDouble("lon"),
                                (float) obj.getDouble("average_mark"),
                                obj.getInt("num_votes"));

                        boolean emailValidated = obj.getInt("email_validated") == 1;
                        Prefs.instance.setEmailValidated(emailValidated);

                        // user's services
                        JSONArray services = obj.getJSONArray("services");

                        for (int a = 0; a < services.length(); a++) {
                            JSONObject s = services.getJSONObject(a);

                            boolean isNeed = s.getInt("is_a_need") == 1;
                            Service service =
                                    new Service(s.getString("category"),
                                            s.getString("detailed_descr"),
                                            isNeed);
                            if (isNeed) {
                                mSQL.addNeedInCurrentThread(service);
                            }
                            else {
                                mSQL.addAbilityInCurrentThread(service);
                            }
                        }

                        // received opinions
//                        JSONArray lReceivedOpinions =
//                                obj.getJSONArray("opinions")
//                                        .getJSONObject(0)
//                                        .getJSONArray("received_opinions");
//
//                        ArrayList<Opinion> userOpinions = new ArrayList<>();
//                        int lNumOpinions = lReceivedOpinions.length();
//                        for (int i = 0; i < lNumOpinions; i++) {
//                            JSONObject jo = lReceivedOpinions.getJSONObject(i);
//                            Opinion op = new Opinion(jo.getString("evaluator_name"),
//                                    jo.getString("evaluation_date"),
//                                    (float) jo.getDouble("mark"),
//                                    jo.getString("topic"));
//
//                            userOpinions.add(op);
//                        }

                        // given opinions
//                        JSONArray lGivenOpinions =
//                                obj.getJSONArray("opinions")
//                                        .getJSONObject(0)
//                                        .getJSONArray("given_opinions");
//
//                        ArrayList<Opinion> userGivenOpinions = new ArrayList<>();
//                        int lNumGivenOpinions = lGivenOpinions.length();
//                        for (int i = 0; i < lNumGivenOpinions; i++) {
//                            JSONObject jo = lGivenOpinions.getJSONObject(i);
//                            Opinion op = new Opinion(jo.getString("opinion_receiver"),
//                                    jo.getString("evaluation_date"),
//                                    (float) jo.getDouble("mark"),
//                                    jo.getString("topic"));
//
//                            userGivenOpinions.add(op);
//                        }
//                                mGivenOpinions.setValue(userGivenOpinions);

                        // Consider that the user is registered and has jabber account
                        // this is called when the user registration is complete
                        setUserFullyRegistered(activity);

                        // once we have the user id, service that interacts with the Jabber server starts
                        JabberService.start(activity);

                        MyMessagingService.initializeFirebase(activity);

                        mLoginDialog.dismiss();

                        HttpTempo.getInstance().getUserPicNames(activity, mUserId,
                                list -> {
                                    for (String file: list) {
                                        ImgLoader.downloadPic(activity, file, () -> {});
                                    }
                                });

                        ContactsViewModel.mContactInfoObtainedFromServer = false;

                        requestContactIDs_onLogin(activity); // important to call this here because mUserId has correct value

                        Toast.makeText(activity, R.string.correctly_logged_in, Toast.LENGTH_LONG).show();

                        if (activity instanceof MainActivity)
                        {
                            ((MainActivity) activity).forceOnResumeServicesFragment();
                        }

                    } catch (JSONException e) {
                        Log.e(TAG, "Error parsing opinions response [" + e.getMessage() + " ]", e);
                    } catch (Throwable t) {
                        Log.e(TAG, "Could not parse malformed JSON: \"" + response + "\"", t);
                    }
                },
                error -> {
                    String e = error.getMessage();
                }
          ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("controller", "User");
                headers.put("action", "log_in");
                headers.put("email", new String(Base64.encode (mEncryptedEmail, Base64.NO_WRAP)));
                headers.put("password", new String(Base64.encode (mEncryptedPassword, Base64.NO_WRAP)));
                return headers;
            }

            @Override
            public Priority getPriority() {
                return Priority.IMMEDIATE;
            }
        };

        HttpTempo.getInstance().getRequestQueue(activity).add(lStringReq);
    }

    private void requestContactIDs_onLogin(Context context) {
        JsonArrayRequest jsonArrayRequestContactsList =
                new JsonArrayRequest(Request.Method.GET,
                        HttpTempo.URL_GET_CONTACTS_LIST + "&uid=" + mUserId,
                        null,
                        response -> {
                        try {
                            int lNumOpinions = response.length();
                            for (int i = 0; i < lNumOpinions; i++) {
                                JSONObject jo = response.getJSONObject(i);

                                String lContactID = jo.getString("contact_id");
                                String lService = jo.getString("service");

                                Log.d(TAG, "Adding this ID [" + lContactID + "]");
                                mSQL.storeContactWithJustName(lContactID, ""); // the name will be obtained when going to "contacts"
                                mSQL.setChatTopic(lContactID, lService,
                                        true);
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "Error requestContactIDs " + e.getMessage());
                        }

                        // there is no need to send information to server as it was just received from there
                        mSQL.resetContactSetNeedsRemoteUpdate();
                },

                error -> Log.e(TAG, "JSON Error requestContactIDs " + error)

        );
        HttpTempo.getInstance().getRequestQueue(context).add(jsonArrayRequestContactsList);
    }

    private void signUpUser(Activity activity) {
        if (mEncryptedEmail.length == 0 || mEncryptedPassword.length == 0) {
            return;
        }

        mSignUpProcessStarted = true;

        StringRequest signUpRequest = new StringRequest(Request.Method.POST,
                HttpTempo.URL_INDEX,
                (response) -> {
                    String respWithoutCR = response.replaceAll("\\n", "");
                    if (respWithoutCR.contains("EMAIL_REPEATED")) {
                        Toast.makeText(activity,
                        activity.getString(R.string.user_already_registered),
                        Toast.LENGTH_LONG).show();

                        mUserId = INVALID_USER;
                        mSignUpProcessStarted = false;
                    }
                    else if (respWithoutCR.contains("Invalid 'to' recipient")) {
                        Toast.makeText(activity,
                        activity.getString(R.string.invalid_email_address),
                        Toast.LENGTH_LONG).show();

                        mUserId = INVALID_USER;
                        mSignUpProcessStarted = false;
                    }
                    else if (respWithoutCR.contains("OK"))
                    {
                        // The user id comes in the form '"OKuser_idOK"
                        mUserId = respWithoutCR.replaceAll("OK", "");

                        mSQL.updateMyInfo(myName, new LonLatCity());

                        // once we have the user id, service that interacts with the Jabber server starts
                        JabberService.start(activity);

                        MyMessagingService.initializeFirebase(activity);

                        mRegisterDialog.dismiss();
                    }
                    else {
                        Toast.makeText(activity,
                        activity.getString(R.string.unkown_error),
                        Toast.LENGTH_LONG).show();

                        mUserId = INVALID_USER;
                        mSignUpProcessStarted = false;
                    }
                }, (volleyError) ->  {

                    mUserId = INVALID_USER;

                    mSignUpProcessStarted = false;

                    Toast.makeText(activity,
                            activity.getString(R.string.error_registering_user),
                            Toast.LENGTH_LONG).show();
                    if (volleyError instanceof TimeoutError) {
                        Log.e(TAG, "Error getting JSONArray (requestNearServices)",
                                volleyError);
                }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("controller", "User");
                headers.put("action", "sign_up");
                headers.put("user_name", myName);
                headers.put("email", new String(Base64.encode (mEncryptedEmail, Base64.NO_WRAP)));
                headers.put("password", new String(Base64.encode (mEncryptedPassword, Base64.NO_WRAP)));
                headers.put("user_locale", Locale.getDefault().getDisplayLanguage());
                return headers;
            }

            @Override
            public Priority getPriority() {
                return Priority.IMMEDIATE;
            }
        };

        HttpTempo.getInstance().getRequestQueue(activity).add(signUpRequest);
    }

    public void clearLocalData(Context context) {
        mUserId = INVALID_USER;

        for (File file: context.getApplicationContext().getFilesDir().listFiles()) {
            file.delete();
        }

        Prefs.instance.clear();
    }

    public void setJabberRegisterFailed(Context context) {
        // when registering a user, s/he is firstly added into 'CREDENTIALS' table and then
        // registered into Jabber. If Jabber registration fails, then rollback for the previous
        // stage is needed.
        StringRequest deleteUserRequest = new StringRequest(Request.Method.POST,
                HttpTempo.URL_INDEX,
                (response) -> {
                    String respWithoutCR = response.replaceAll("\\n",
                            "");
                    if (respWithoutCR.contains("OK"))
                    {
                        clearLocalData(context);
                    }
                    else {
                        mUserId = INVALID_USER;
                    }

                    mSignUpProcessStarted = false;
                }, (volleyError) ->  {

            if (volleyError instanceof TimeoutError) {
                Log.e(TAG, "Error getting JSONArray (requestNearServices)",
                        volleyError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("controller", "User");
                headers.put("action", "remove_account");
                headers.put("uid", mUserId);
                return headers;
            }

            @Override
            public Priority getPriority() {
                return Priority.IMMEDIATE;
            }
        };

        HttpTempo.getInstance().getRequestQueue(context).add(deleteUserRequest);
    }

    private void showDialogToResetPassword(Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_reset_password, null);
        TextView lVerificationCode = v.findViewById(R.id.verification_code);
        TextView lNewPassword = v.findViewById(R.id.new_password);
        AppCompatButton lBtnRestorePassword = v.findViewById(R.id.btn_restore_password_and_enter);
        AppCompatButton lBtnCancel = v.findViewById(R.id.btn_cancel);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(v);

        mRestorePasswordDialog = builder.create();
        mRestorePasswordDialog.setCanceledOnTouchOutside(false);
        mRestorePasswordDialog.setCancelable(false);
        mRestorePasswordDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        mRestorePasswordDialog.show();

        lBtnRestorePassword.setOnClickListener(v1 ->
        {
            String code = lVerificationCode.getText().toString();
            String newPass = lNewPassword.getText().toString();

            if (code.length() < 6) {
                Toast.makeText(activity,
                        activity.getString(R.string.please_provide_valid_code),
                        Toast.LENGTH_SHORT).show();
            }
            else if (newPass.length() < MIN_PASS_LENGHT)
            {
                Toast.makeText(activity,
                        activity.getString(R.string.please_provide_longer_password),
                        Toast.LENGTH_SHORT).show();
            }
            else {
                sendVerifCode_NewPassword_and_RestorePassword(activity, code, newPass);
            }
        });

        lBtnCancel.setOnClickListener(v1 -> mRestorePasswordDialog.dismiss());
    }

    private void askToRestorePassword(Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();

        View v = inflater.inflate(R.layout.dialog_forgot_password, null);
        TextView lEmail = v.findViewById(R.id.email_address);
        AppCompatButton lBtnSendCodeResetPassword = v.findViewById(R.id.send_verification_code_button);
        AppCompatButton lBtnCancel = v.findViewById(R.id.btn_cancel);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(v);

        mForgotPasswordDialog = builder.create();
        mForgotPasswordDialog.setCanceledOnTouchOutside(false);
        mForgotPasswordDialog.setCancelable(false);
        mForgotPasswordDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        mForgotPasswordDialog.show();

        lBtnSendCodeResetPassword.setOnClickListener(v1 ->
        {
            String m = lEmail.getText().toString();
            if (m.isEmpty() || !m.contains("@") || m.contains(" ")) {
                Toast.makeText(activity,
                        activity.getString(R.string.please_provide_valid_email),
                        Toast.LENGTH_SHORT).show();
            }
            else if (m.contains(" ")) {
                Toast.makeText(activity,
                        activity.getString(R.string.please_make_sure_you_do_not_use_spaces),
                        Toast.LENGTH_SHORT).show();
            }
            else {
                sendEmailToRestorePassword(activity, m);
            }
        });

        lBtnCancel.setOnClickListener(v1 -> mForgotPasswordDialog.dismiss());
    }

    private void sendVerifCode_NewPassword_and_RestorePassword(Context context,
                                                               String codeResetPass,
                                                               String newPassword) {
        try {
            StringRequest remindPasswordRequest = new StringRequest(Request.Method.POST,
                    HttpTempo.URL_INDEX,
                    (response) -> {
                        String respWithoutCR = response.replaceAll("\\n","");
                        if (!respWithoutCR.contains("ERROR") &&
                                !respWithoutCR.contains("Warning"))
                        {
                            Toast.makeText(context,
                                    context.getString(R.string.password_restored_msg),
                                    Toast.LENGTH_LONG).show();

                            mRestorePasswordDialog.dismiss();
                        }
                        else {
                            Toast.makeText(context,
                                    context.getString(R.string.couldnt_restore_password),
                                    Toast.LENGTH_LONG).show();
                        }
                    }, (volleyError) ->  {

                if (volleyError instanceof TimeoutError) {
                    Log.e(TAG, "Error getting JSONArray (sendVerifCode_NewPassword_and_LogIn)",
                            volleyError);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    try {
                        mEncryptedPassword = encryptMsg(newPassword);
                    } catch (Exception e) {
                        mEncryptedPassword = new byte[16]; // 16, arbitrary number
                    }

                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("controller", "User");
                    headers.put("action", "restore_password");
                    headers.put("email", new String(Base64.encode (mEncryptedEmail, Base64.NO_WRAP)));
                    headers.put("code_reset_pass", codeResetPass);
                    headers.put("new_password", new String(Base64.encode (mEncryptedPassword, Base64.NO_WRAP)));
                    return headers;
                }

                @Override
                public Priority getPriority() {
                    return Priority.IMMEDIATE;
                }
            };

            HttpTempo.getInstance().getRequestQueue(context).add(remindPasswordRequest);

        } catch (Exception e) {
            Log.e(TAG, "Error encrypting email when asking to remind password " + e.getMessage());
        }
    }

    private void sendEmailToRestorePassword(Activity activity, String email) {
        try {
            mEncryptedEmail = encryptMsg(email);

            FileOutputStream fos = activity.openFileOutput(EMAIL_FILE, Context.MODE_PRIVATE);
            fos.write(mEncryptedEmail);
            fos.flush();
            fos.close();

            StringRequest remindPasswordRequest = new StringRequest(Request.Method.POST,
                    HttpTempo.URL_INDEX,
                    (response) -> {
                        String respWithoutCR = response.replaceAll("\\n","");
                        if (!respWithoutCR.contains("ERROR") &&
                                !respWithoutCR.contains("Warning"))
                        {
                            Toast.makeText(activity,
                                    activity.getString(R.string.code_reset_password_sent),
                                    Toast.LENGTH_LONG).show();
                            mForgotPasswordDialog.dismiss();

                            showDialogToResetPassword(activity);
                        }
                        else {
                            Toast.makeText(activity,
                                    activity.getString(R.string.there_is_no_account_with_that_email),
                                    Toast.LENGTH_LONG).show();
                        }
                    }, (volleyError) ->  {

                if (volleyError instanceof TimeoutError) {
                    Log.e(TAG, "Error getting JSONArray (requestNearServices)",
                            volleyError);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("controller", "User");
                    headers.put("action", "send_email_to_reset_password");
                    headers.put("email", new String(Base64.encode (mEncryptedEmail, Base64.NO_WRAP)));
                    headers.put("user_locale", Locale.getDefault().getDisplayLanguage());
                    return headers;
                }

                @Override
                public Priority getPriority() {
                    return Priority.IMMEDIATE;
                }
            };

            HttpTempo.getInstance().getRequestQueue(activity).add(remindPasswordRequest);

        } catch (Exception e) {
            Log.e(TAG, "Error encrypting email when asking to remind password " + e.getMessage());
        }
    }

    /**
     * Encrypts email and password
     * @param email
     * @param password
     * @return true if all correct. false otherwise.
     */
    private boolean encryptEmailAndPassword(String email, String password) {
        try {
            mEncryptedEmail = encryptMsg(email);
            mEncryptedPassword = encryptMsg(password);
        } catch (Exception e) {
            Log.e(TAG, "Error encrypting email/pasword " + e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * Saves user id, encrypted email and password into private files.
     *
     * In that case, we consider that the user is fully registered but it is still pending to
     * validate the email address.
     */
    public void setUserFullyRegistered(Context context) {
        try {
            if (mEncryptedPassword.length == 0 ||
                    mEncryptedEmail.length == 0 || mUserId.equals(INVALID_USER)) {
                Log.e(TAG, "Error storing mail pass ");
                return;
            }

            FileOutputStream fos =
                    context.openFileOutput(EMAIL_FILE, Context.MODE_PRIVATE);
            fos.write(mEncryptedEmail);
            fos.flush();
            fos.close();

            fos = context.openFileOutput(PASSWORD_FILE, Context.MODE_PRIVATE);
            fos.write(mEncryptedPassword);
            fos.flush();
            fos.close();

            Prefs.instance.saveUserIdAndUserIsInJabber(mUserId);

            if (mMainActivity != null) {
                mMainActivity.invalidateOptionsMenu();

                if (!Prefs.instance.isEmailAddressValidated()) {
                    // the user still has to validate the email address.
                    askUserToValidateEmail(mMainActivity);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error saving saving user data " + e.getMessage());
        }

        if(mRegisterDialog != null && mRegisterDialog.isShowing()) {
            mRegisterDialog.dismiss();
        }

        mSignUpProcessStarted = false;
    }

    private byte[] getEncryptedEmail(Context context) {
        try {
            return readBytes(context.getFilesDir() + "/" + EMAIL_FILE);
        } catch (Exception e) {
            Log.e(TAG, "Error decrypting password " + e.getMessage());
        }
        return null;
    }

    private byte[] getEncryptedPassword(Context context) {
        try {
            return readBytes(context.getFilesDir() + "/" + PASSWORD_FILE);
        } catch (Exception e) {
            Log.e(TAG, "Error decrypting password " + e.getMessage());
        }
        return null;
    }

    // We will never change that password
    public String getEjabberdPassword() {
        String lRet = "";
        try {
            if (mEncryptedEmail != null) {
                String strong = decryptMsg(mEncryptedEmail);
                strong += "@#|A39RfW2P_x34!e3^s5d:O0L1verp00Ls·%Q,D";
                String strongBase64 = new String(Base64.encode (encryptMsg(strong), Base64.NO_WRAP));

                final int MAX_CHAR = 64;
                int maxLength = (strongBase64.length() < MAX_CHAR) ? strongBase64.length() : MAX_CHAR;
                lRet = strongBase64.substring(0, maxLength);
            }
        } catch (Exception e) {
            Log.e(TAG, "Error getting ejabberd password");
        }
        return lRet;
    }

    private String getEmailAddress() {
        try {
            return decryptMsg(mEncryptedEmail);
        } catch (Exception e) {
            Log.e(TAG, "Error getting unencrypted email address");
        }

        return "";
    }

    private byte[] readBytes(String path) throws Exception {
        File file = new File(path);
        int size = (int) file.length();
        byte[] bytes = new byte[size];
        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
        buf.read(bytes, 0, bytes.length);
        buf.close();
        return bytes;
    }

    private byte[] encryptMsg(String message)
            throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException
    {
   /* Encrypt the message. */
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        //TODO ECB mode seems to be insecure
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(MASTER_PASSWD, "AES"));
        return cipher.doFinal(message.getBytes(Charset.forName("UTF-8")));
    }

    private String decryptMsg(byte[] cipherText)
        throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidParameterSpecException,
            InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException,
            IllegalBlockSizeException, UnsupportedEncodingException
    {
        /* Decrypt the message, given derived encContentValues and initialization vector. */
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(MASTER_PASSWD, "AES"));
        return new String(cipher.doFinal(cipherText), "UTF-8");
    }

//    String encrypted_email = "1oxviwRb/HIiq7sGu5cgWgwl+AS/7E0QtS6Id9lUm+3rd5QhBcBtJ8/dKD7bdVz3\n";
    // the \n is added when using Base64.DEFAULT
//    byte[] aa = Base64.decode (encrypted_email, Base64.NO_WRAP);
//        try {
//        String decryptedEmail = decryptMsg(aa);
//    } catch (Exception e) {
//
//    }
}
