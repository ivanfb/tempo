package com.bandesoft.tempo.Remote;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.bandesoft.tempo.ChatActivity;
import com.bandesoft.tempo.Model.JsonMessage;
import com.bandesoft.tempo.Model.NotDeliveredMessage;
import com.bandesoft.tempo.Model.SQL;
import com.bandesoft.tempo.Model.Worker;
import com.bandesoft.tempo.R;
import com.bandesoft.tempo.Service.JabberService;
import com.bandesoft.tempo.Utils.Ctes;
import com.bandesoft.tempo.Utils.NotifsManager;
import com.bandesoft.tempo.Utils.Prefs;
import com.bandesoft.tempo.Utils.ThreadPool;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.sasl.SASLMechanism;
import org.jivesoftware.smack.sasl.provided.SASLDigestMD5Mechanism;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jivesoftware.smackx.offline.OfflineMessageHeader;
import org.jivesoftware.smackx.offline.OfflineMessageManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.parts.Localpart;
import org.jxmpp.stringprep.XmppStringprepException;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JabberConnection implements ConnectionListener, ReconnectionListener,
        IncomingChatMessageListener, ReceiptReceivedListener, StanzaListener {

    private static final String TAG = "JabberConnection";

    static {
        try {
            // make Dalvik load the ReconnectionManager class and run it's static initialization block
            Class.forName("org.jivesoftware.smack.ReconnectionManager");
        } catch (ClassNotFoundException ex) {
            // problem loading reconnection manager
            Log.e(TAG, "Class not found", ex);
        }
    }

    private static final int PORT = 5222;
    private static final String HOST = "35.242.198.191";
    public static final String DOMAIN = "tempo-xmpp-server.europe-west3-b.c.tempo-213421.internal";
//    private static final String HOST = "192.168.1.52";
//    public static final String DOMAIN = "192.168.1.52";

    private final Context mContext;
    private XMPPTCPConnection mConnection;
    private BroadcastReceiver mUiThreadMessageReceiver; //Receives messages from the ui thread.
    private ChatManager mChatManager;
    private ReconnectionManager mReconectManager;

    private DeliveryReceiptManager mDeliveryReceiptManager;
    private MessageDeliveryCache mDeliveryCache;

    private String myName;

    private SQL mSQL; // instance to the sqlite

    public JabberConnection(Context context) {
        Log.d(TAG, "JabberConnection constructor called.");
        mContext = context.getApplicationContext();
        mSQL = SQL.getInstance(context);
        mDeliveryCache = new MessageDeliveryCache(mSQL);
    }

    private void reconnect()
    {
        Log.d(TAG, "Reconnect");
        try {
            disconnect();
            connect();
        } catch (Exception e) {
            Log.e(TAG, "Exception when trying to reconnect");
        }
    }

    public void connect() throws Exception
    {
        Log.d(TAG, "connect");

        mDeliveryCache.retrieveDataDromSQL();

        // Trying to connect
        int numTrials = 0;
        boolean ok = false;
        do {
            try {
                String ejabberdPass = AuthManager.instance.getEjabberdPassword();
                if (mConnection == null && !ejabberdPass.isEmpty()) {

                    XMPPTCPConnectionConfiguration.Builder builder =
                            XMPPTCPConnectionConfiguration.builder()
                                    .setXmppDomain(JidCreate.domainBareFrom(DOMAIN))
                                    .setHostAddress(InetAddress.getByName(HOST))
                                    .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                                    .setSendPresence(false)
                                    .setUsernameAndPassword(AuthManager.instance.getUserId(),
                                            ejabberdPass)
                                    .setHost(HOST)
                                    .setPort(PORT);

                    Log.d(TAG, "New connection object created");
                    mConnection = new XMPPTCPConnection(builder.build());
                    mConnection.addConnectionListener(this); // to handle connection state changes

                    // To control the message reception
                    mDeliveryReceiptManager = DeliveryReceiptManager.getInstanceFor(mConnection);
                    mDeliveryReceiptManager.addReceiptReceivedListener(this);

                    // Stream Resumption is not working very well if the app is forced killed and then started manually
                    // Therefore, resumption module has been disabled in server.
                    mConnection.setUseStreamManagement(true);
                    mConnection.setUseStreamManagementResumption(true);

                    //Reconnect xmpp connection after every 10sec when connection is disconnected
                    // ivan: I comment this because I perform the reconnections manually
//                    mReconectManager = ReconnectionManager.getInstanceFor(mConnection);
//                    mReconectManager.enableAutomaticReconnection();
//                    mReconectManager.setReconnectionPolicy(ReconnectionManager.ReconnectionPolicy.RANDOM_INCREASING_DELAY);
//                    mReconectManager.addReconnectionListener(this);

                    mChatManager = ChatManager.getInstanceFor(mConnection);
                    mChatManager.addIncomingListener(this);

                    Log.d(TAG, "Connection initialised");
                }

                if (!mConnection.isConnected()) {
                    Log.d(TAG, "Trying to connect");
                    mConnection.connect();
                    Log.d(TAG, "Connected");
                }

                boolean lIsUserInJabber = Prefs.instance.isUserRegisteredInJabber();
                if (!lIsUserInJabber) { // create the user if doesn't exist yet
                    Log.d(TAG, "Trying to create account");
                    AccountManager accountManager = AccountManager.getInstance(mConnection);
                    accountManager.sensitiveOperationOverInsecureConnection(true);
                    accountManager.createAccount(Localpart.from(AuthManager.instance.getUserId()),
                            ejabberdPass);

                    AuthManager.instance.setUserFullyRegistered(mContext);
                }

                Log.d(TAG, "Before SASL");
                SASLMechanism mechanism = new SASLDigestMD5Mechanism();
                SASLAuthentication.registerSASLMechanism(mechanism);
                SASLAuthentication.blacklistSASLMechanism("SCRAM-SHA-1");
                SASLAuthentication.unBlacklistSASLMechanism("DIGEST-MD5");

                Log.d(TAG, "Trying to login");
                mConnection.login();
                Log.d(TAG, "Logged in :)");
                ok = true;

            } catch (SmackException.AlreadyLoggedInException alie) {

                Log.d(TAG, "Already logged in exception. Just dismissing this connect call");
                return;

            } catch (Exception nre) {
                Log.e(TAG, "Exception connecting/login ["+ numTrials + "] " + nre.getMessage());

                if (HttpTempo.hasInternetAccess(mContext))
                {
                    // If there is a problem but there is internet access, then, consider it in order
                    // to re-throw an exception.
                    // This exception should inform the user about the problem and sent to developer
                    // for further analysis.
                    numTrials++;
                }
                else {
                    // Try indefinitely while there is not internet connection
                    numTrials = 0;
                }

                Thread.sleep(2000);
                if (numTrials == 10) {
                    disconnect();
                    throw nre;
                }
            }
        } while (!ok);

        Log.d(TAG, "connection user[" + mConnection.getUser() + "]");

        handleOfflineMessages();

        // send presence
        Presence p = new Presence(Presence.Type.available);
        mConnection.sendStanza(p);

        String token = Prefs.instance.getFirebaseToken();
        if (!token.isEmpty()) { // this is needed to inform the user about incoming messages
            HttpTempo.getInstance().uploadFirebaseToken(token);
        }

        // TODO retries
//        // To get message sent status
//        DeliveryReceiptManager.setDefaultAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.always);
//        ProviderManager.addExtensionProvider(DeliveryReceipt.ELEMENT, DeliveryReceipt.NAMESPACE, new DeliveryReceipt.Provider());
//        ProviderManager.addExtensionProvider(DeliveryReceiptRequest.ELEMENT, DeliveryReceipt.NAMESPACE, new DeliveryReceiptRequest.Provider());

        //Set up the ui thread broadcast message receiver.
        setupUiThreadBroadCastMessageReceiver();

        // Establish chat with all contacts
        ArrayList<String> allUids = mSQL.getAllContactIDs();
        for (String uid: allUids) {
            String jabberID = uid + "@" + DOMAIN;
            try {
                Log.d(TAG, "Establishing chat with [" + jabberID + "]");
                mChatManager.chatWith(JidCreate.entityBareFrom(jabberID));
            } catch (XmppStringprepException e) {
                e.printStackTrace();
                Log.e(TAG, "Couldn't establish initial chat with user [" + uid + "]");
            }
        }

        startReSender();
    }

    /** Implementing IncomingChatMessageListener */
    @Override
    public void newIncomingMessage(EntityBareJid messageFrom, Message message, Chat chat) {
        String from = message.getFrom().toString();

        Log.d(TAG,"newIncomingMessage body [" + message.getBody() + "] from [" + from + "] " +
                "id [" + message.getStanzaId() + "]");

        String contactJid = from.contains("/") ? from.split("/")[0] : from;
        String subject = message.getSubject(); // the unix time (s) when was sent

        String[] time_name = subject.split("@");
        if (time_name.length == 2) {
            String timestamp = time_name[0];
            String senderName = time_name[1];

            String rawJson = message.getBody();

            JsonMessage jsonMessage = JsonMessage.parse(rawJson);

            String uid = contactJid;
            if (uid.contains("@")) {
                uid = uid.split("@")[0];
            }

            if (!mDeliveryCache.isMessageAlreadyReceived(uid, message.getStanzaId())) {
                mDeliveryCache.addNewIncomingMessage(uid, message.getStanzaId());

                String lChatTopic = Ctes.swapChatTopic(jsonMessage.getChatTopic());

                boolean infoHandled = false;
                if (!ChatActivity.IsVisible) {
                    updateLocalInfoForIncomingMessage(jsonMessage.getMessageType(),
                            jsonMessage.getBody(),
                            lChatTopic,
                            uid, timestamp, senderName);

                    NotifsManager.showFirebaseNotif(mContext, senderName, uid, lChatTopic);

                    infoHandled = true;
                }

                //Bundle up the intent and send the broadcast.
                Intent intent = new Intent(JabberService.NEW_MESSAGE);
                intent.setPackage(mContext.getPackageName());
                intent.putExtra(JabberService.BUNDLE_FROM_JID, contactJid);
                intent.putExtra(JabberService.BUNDLE_MESSAGE_TYPE, jsonMessage.getMessageType());
                intent.putExtra(JabberService.BUNDLE_MESSAGE_BODY, jsonMessage.getBody());
                intent.putExtra(JabberService.BUNDLE_CHAT_TOPIC, lChatTopic);
                intent.putExtra(JabberService.BUNDLE_SENT_TIME, timestamp);
                intent.putExtra(JabberService.BUNDLE_SENDER_NAME, senderName);
                intent.putExtra(JabberService.BUNDLE_INFO_HANDLED_IN_JABBER_CONN, infoHandled);
                mContext.sendBroadcast(intent);
            }
            else {
                Log.d(TAG, "Ignoring message [" + message.getStanzaId() + "] for user [" + uid + "]");
            }
        }
        else {
            Log.e(TAG, "Wrong subject received");
        }

        Log.d(TAG,"Received message from :" + contactJid + " broadcast sent.");
    }

    public boolean isAuthenticated() {
        if (mConnection == null) {
            return false;
        }
        else {
            return JabberService.sConnectionState == ConnectionState.AUTHENTICATED;
        }
    }

    public void disconnect() {
        mDeliveryCache.persistMessagesIntoSQL();

        if (mConnection != null) {
            Log.d(TAG, "disconnecting connection user[" + mConnection.getUser() + "] [" +
                    mConnection.getStreamId() + "]");

            if (mConnection.isConnected()) {
                // notify the server of unavailability
                Presence p = new Presence(Presence.Type.unavailable);
                try {
                    mConnection.sendStanza(p);
                    Log.d(TAG, "Unavailable presence stanza sent correctly");
                } catch (Exception e) {
                    Log.e(TAG, "couldn't send unavailable presence stanza");
                }
            }

            int numTrials = 0;
            boolean cont = false;
            do {
                try {
                    mConnection.removeConnectionListener(this);
                    mConnection.disconnect();
                    Log.d(TAG, "Connection disconnected [" + mConnection.getStreamId() + "]");
                    cont = true;
                } catch (Exception nre) {
                    Log.e(TAG, "Exception in disconnecting ["+ numTrials + "] " +
                            nre.getMessage());
                    numTrials++;
                    if (numTrials == 5) {
                        cont = true;
                    }
                }
            } while (!cont);
        }

        mConnection = null;

        if (mReconectManager != null) {
            Log.d(TAG, "Removing reconnection listener");
            mReconectManager.removeReconnectionListener(this);
            mReconectManager = null;
        }

        if (mContext != null) {
            try {
                if (mDeliveryReceiptManager != null) {
                    Log.d(TAG, "Removing receipt receiver");
                    mDeliveryReceiptManager.removeReceiptReceivedListener(this);
                    mDeliveryReceiptManager = null;
                }
            } catch (IllegalArgumentException iae) {
                Log.e(TAG, "Exception unregistering message receiver [" + iae.getMessage() + "]");
            }
        }

        if (mChatManager != null) {
            Log.d(TAG, "Removing incoming listener");
            mChatManager.removeIncomingListener(this);
            mChatManager = null;
        }
    }

    private void handleOfflineMessages() {
        Log.d(TAG, "handleOfflineMessages");

        try {
            OfflineMessageManager offlineManager =
                new OfflineMessageManager(mConnection);

            int lNumOfflineMsgs = offlineManager.getMessageCount();

            Log.i(TAG, "Number of offline messages: " + lNumOfflineMsgs);

//            Toast.makeText(mContext, "handleOffline", Toast.LENGTH_SHORT).show();

            if (lNumOfflineMsgs > 0) {

//                Toast.makeText(mContext, "handleOffline there are some offline", Toast.LENGTH_SHORT).show();
                List<String> offlineMsgStamps = new ArrayList<>();

                for (OfflineMessageHeader omh : offlineManager.getHeaders()) {
                    Log.i(TAG, "offline message header. Timestamp: [" + omh.getStamp() + "]");
                    offlineMsgStamps.add(omh.getStamp());
                }

                for (Message message : offlineManager.getMessages(offlineMsgStamps)) {
                    Log.i(TAG, "receive offline messages, the Received from [" +
                            message.getFrom() + "] the message:" +
                            message.getBody());

                    Log.d(TAG, "message [" + message.toString() + "]");

                    Localpart localPart = message.getFrom().getLocalpartOrNull();

                    String subject = message.getSubject();
                    String rawJson = message.getBody();

                    if (localPart != null && subject != null && rawJson != null) {
                        String uid = localPart.toString();
                        if (uid.contains("@")) {
                            uid = uid.split("@")[0];
                        }

                        String[] time_name = subject.split("@");

                        if (time_name.length == 2) {
                            String timestamp = time_name[0];
                            String senderName = time_name[1];

                            JsonMessage jsonMessage = JsonMessage.parse(rawJson);

                            updateLocalInfoForIncomingMessage(jsonMessage.getMessageType(),
                                                              jsonMessage.getBody(),
                                                              Ctes.swapChatTopic(jsonMessage.getChatTopic()),
                                                              uid,
                                                              timestamp,
                                                              senderName);
                        }
                    }
                }

                // deleting messages from the server
                offlineManager.deleteMessages();

            } //if (lNumOfflineMsgs > 0)
            else if (lNumOfflineMsgs == 0) {
                NotifsManager.clearNotifs(mContext);
            }

        } catch (Exception e) {
            Log.e(TAG, "Error handling offline messages", e);
        }
    }

    private void updateLocalInfoForIncomingMessage(int messageType, String body, String chatTopic,
                                                   String uid, String timestamp,
                                                   String senderName) {

        Log.d(TAG, "updateLocalInfoForIncomingMessage messageType[" +
                messageType + "] uid[" + uid + "] timestamp[" + timestamp + "] senderName[" + senderName + "]");

        if (messageType == JsonMessage.AGREEMENT_REQUEST)
        {
            mSQL.setChatInfo(uid, false,
                    SQL.eChatStatus.STATUS_ONE_PERSON_STARTED_THE_DEAL, chatTopic);
            body = mContext.getString(R.string.body_msg_agreement_req_received);
        }
        else if (messageType == JsonMessage.AGREEMENT_ACCEPT)
        {
            mSQL.updateChatStatus(uid, SQL.eChatStatus.STATUS_AGREEMENT_REACHED);
            body = mContext.getString(R.string.body_msg_agreement_req_accepted) + senderName;
        }
        else if (messageType == JsonMessage.ASSESSMENT_DONE)
        {
            body = String.format(mContext.getString(R.string.body_msg_assessment_received),
                    senderName);
        }

        // persisting messages
        mSQL.addMessage(uid, body, timestamp, false, chatTopic, true);
        mSQL.storeContactWithJustName(uid, senderName);
        mSQL.setPendingMsgsForUser(uid, true);
    }

    private void setupUiThreadBroadCastMessageReceiver() {
        Log.d(TAG, "setupUiThreadBroadCastMessageReceiver");

        if (mUiThreadMessageReceiver == null) {
            mUiThreadMessageReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    //Check if the Intents purpose is to send the message.
                    String action = intent.getAction();
                    if (action != null) {
                        if (action.equals(JabberService.SEND_MESSAGE)) {
                            //SENDS THE ACTUAL MESSAGE TO THE SERVER
                            int messageType = intent.getIntExtra(JabberService.BUNDLE_MESSAGE_TYPE, JsonMessage.NORMAL);
                            String body = intent.getStringExtra(JabberService.BUNDLE_MESSAGE_BODY);
                            String chatTopic = intent.getStringExtra(JabberService.BUNDLE_CHAT_TOPIC);

                            String rawJson = JsonMessage.serialize(messageType, body, chatTopic);
                            String toJid = intent.getStringExtra(JabberService.BUNDLE_TO);
                            String sendTime = intent.getStringExtra(JabberService.BUNDLE_SENT_TIME);

                            sendMessage(rawJson, toJid, sendTime, null);
                        }
                        else if (action.equals(JabberService.STORE_PENDING_MESSAGES)) {
                            mDeliveryCache.persistMessagesIntoSQL();
                        }
                    }
                }
            };

            IntentFilter filter = new IntentFilter();
            filter.addAction(JabberService.SEND_MESSAGE);
            filter.addAction(JabberService.STORE_PENDING_MESSAGES);
            mContext.registerReceiver(mUiThreadMessageReceiver, filter);
        }
    }

    private void sendMessage(String rawJson, String toJid, String sendTime, String stanzaId) {
        try {
            EntityBareJid jid = JidCreate.entityBareFrom(toJid);

            Worker me = mSQL.getMe();
            if (me != null) {
                myName = me.getName();
            }

            String subject = sendTime + "@" + myName;

            Message message = new Message(jid, Message.Type.chat);
            message.setBody(rawJson);
            message.setSubject(subject);

            if (stanzaId != null) {
                message.setStanzaId(stanzaId); // used when re-sending messages not properly delivered
            }

            String deliveryReceiptId = DeliveryReceiptRequest.addTo(message);

            if (mConnection != null) {
                ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
                Chat chat = chatManager.chatWith(jid);

                // To get delivery aknowledge from server
                if (mConnection.isSmEnabled()) {
                    mConnection.addStanzaIdAcknowledgedListener(deliveryReceiptId,
                            this);
                }
                mDeliveryCache.addNewSentMessage(rawJson, toJid, sendTime, deliveryReceiptId);

                chat.send(message);

                Log.d(TAG, "sendMessage: to [" + toJid + "] \n" +
                        " conn Id [" + (mConnection != null ? mConnection.getStreamId() : "null") + "] \n" +
                        " deliveryReceiptId [" + deliveryReceiptId + "] \n" +
                        " rawJson [" + rawJson + "]");
            }
            else {
                // mConnection is null here. Adding msg to cache to further retrial
                mDeliveryCache.addNewSentMessage(rawJson, toJid, sendTime, deliveryReceiptId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean removeAccount() {
        try {
            AccountManager accountManager = AccountManager.getInstance(mConnection);
            accountManager.deleteAccount();
            Log.d(TAG, "Account removed correctly");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Exception in removeAccount", e);
            return false;
        }
    }

    @Override
    // implementing 'StanzaListener'
    public void processStanza(Stanza packet) throws SmackException.NotConnectedException,
            InterruptedException, SmackException.NotLoggedInException {
        // If this is called, I will consider that the message was correctly delivered to the server

        Log.d(TAG, "processStanza " + packet.toString());

        mDeliveryCache.considerMessageAsDelivered(packet.getStanzaId());
    }

    @Override
    public void connected(XMPPConnection connection) {
        JabberService.sConnectionState = ConnectionState.CONNECTED;

        boolean lIsAuth = connection.isAuthenticated();
        boolean lIsConn = connection.isConnected();

        Log.d(TAG, "is connected [" + (lIsConn ? "Y":"N") +
                "] is auth [" + (lIsAuth ? "Y":"N") + "]");
    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        JabberService.sConnectionState = ConnectionState.AUTHENTICATED;
        Log.d(TAG, "Authenticated Successfully");
    }

    @Override
    public void connectionClosed() {
        JabberService.sConnectionState = ConnectionState.DISCONNECTED;
        Log.d(TAG, "Connectionclosed()");
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        JabberService.sConnectionState = ConnectionState.DISCONNECTED;
        Log.d(TAG, "ConnectionClosedOnError, error " + e.toString());

        if (Prefs.instance.isUserRegisteredInJabber()) {
            // Don't consider the reconnection logic when user is registering
            Log.d(TAG, "Trying to reconnect again");
            reconnect();
        }
    }

    @Override
    public void reconnectingIn(int seconds) {
        Log.d(TAG, "reconnectingIn " + seconds + " seconds");
    }

    @Override
    public void reconnectionFailed(Exception e) {
        Log.d(TAG, "reconnectionFailed " + e.getMessage());
    }

    @Override
    public void onReceiptReceived(Jid fromJid, Jid toJid, String receiptId, Stanza receipt) {
        // This is called when the receiver receives the message. Will be useful for showing double-check
        Log.d(TAG, "Receipt received for " + receiptId);
    }

    /** Starts the timer that tries to send, again, messages not correctly delivered */
    private void startReSender() {
        ThreadPool.instance.runTask(() -> {
            while (mConnection != null && mConnection.isConnected()) {

                for (Map.Entry<String, NotDeliveredMessage> m :
                        mDeliveryCache.getNotDeliveredMsgs().entrySet()) {
                    String stanzaId = m.getKey();
                    NotDeliveredMessage ndm = m.getValue();

                    long sentTime = Long.decode(ndm.getSendTime());

                    if ((System.currentTimeMillis() - sentTime) > 5000) {
                        // only try to re-send a message after 5s of not receiving ack from server
                        sendMessage(ndm.getRawJson(), ndm.getToJid(), ndm.getSendTime(), stanzaId);
                    }
                }

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public enum ConnectionState {
        CONNECTED, AUTHENTICATED, CONNECTING, DISCONNECTING, DISCONNECTED
    }
}

