package com.bandesoft.tempo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.bandesoft.tempo.Model.LonLatCity;
import com.bandesoft.tempo.Model.SQL;
import com.bandesoft.tempo.Model.Service;
import com.bandesoft.tempo.Model.ServicesViewModel;
import com.bandesoft.tempo.Model.Worker;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Utils.ImgLoader;
import com.bandesoft.tempo.Utils.Prefs;
import com.bandesoft.tempo.Vista.WorkersList;

import java.util.ArrayList;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class ServicesFragment extends Fragment
        implements AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener {

    private static final int REQ_EDIT_SEARCH_FILTERS = 0;

    private ServicesViewModel mViewModel;
    private ListView mListCandidates; // list that shows the candidates

    private SearchView mSearchView;

    private SQL mSQL;

    public ServicesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_services, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mSQL = SQL.getInstance(getActivity());

        mListCandidates = getView().findViewById(R.id.list_candidates);
        mListCandidates.setOnItemClickListener(this);

        mViewModel = new ViewModelProvider(this).get(ServicesViewModel.class);
    }

    @Override
    public void onResume() {
        super.onResume();

        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.updateServicesBtnText();

        updateWorkersList(getContext(),"");
    }

    private void updateWorkersList(Context context, String textToSearch) {
        LonLatCity lSearchLocation = Prefs.instance.getSearchLoc();

        double lLat = lSearchLocation.getLat();
        double lLon = lSearchLocation.getLon();
        int lRadiusKm = lSearchLocation.getRadiusKm();

        String lSearchTagCategory = Prefs.instance.getSearchTagCategory();

        mViewModel.getWorkersList(context,
                lLon, lLat, lSearchTagCategory, textToSearch, lRadiusKm)
                .observe(this,
                wList -> mListCandidates.setAdapter(new WorkersList(getActivity(), wList))
        );
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Worker w = mViewModel.getWorkerAtIndex(position);
        Intent i = new Intent(getActivity(), WorkerActivity.class);

        if (Prefs.instance.isSearchOfferedServicesActive()) {
            i.putExtra(WorkerActivity.EXTRA_ORIGIN, WorkerActivity.EXT_ORIGIN_OFFERED_SERVICES);
        }
        else {
            i.putExtra(WorkerActivity.EXTRA_ORIGIN, WorkerActivity.EXT_ORIGIN_NEEDED_SERVICES);
        }

        ImageView userImage = view.findViewById(R.id.worker_item_pic);
        Drawable drawable = userImage.getDrawable();
        if (drawable instanceof BitmapDrawable) {
            // passing the image through a temporary file
            ImgLoader.saveImageToInternalStorage(getActivity(),
                    ((BitmapDrawable) drawable).getBitmap(), WorkerActivity.WORKER_PIC);
            i.putExtra(WorkerActivity.WORKER_PIC_PASSED, true);
        }

        i.putExtra(WorkerActivity.WORKER, w);
        getActivity().startActivity(i);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                Intent i = new Intent(getActivity(), SearchFilterActivity.class);
                getActivity().startActivityForResult(i, REQ_EDIT_SEARCH_FILTERS);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK || data == null) {
            return;
        }

        if (requestCode == REQ_EDIT_SEARCH_FILTERS)
        {
            ArrayList<Worker> lSearchResults = (ArrayList<Worker>)
                    data.getSerializableExtra(SearchFilterActivity.EXTRA_SERVICES);
            mListCandidates.setAdapter(new WorkersList(getActivity(), lSearchResults));
        }
        else if (requestCode == MyProfileActivity.REQ_ADD_NEW_NEED)
        {
            Bundle b = data.getExtras();
            if (b != null) {
                String lTagCategory = b.getString(AddEditAbilityActivity.TAG_CATEGORY);
                String lDetailedDescr = b.getString(AddEditAbilityActivity.DETAILED_DESCR);

                mSearchView.setQuery(lTagCategory, true);
                mSearchView.clearFocus();

                mSQL.addMyNeed(new Service(lTagCategory, lDetailedDescr, true));
                HttpTempo.getInstance().sendUserInformation(mSQL.getMe(), () ->
                        Toast.makeText(getActivity(),
                                getString(R.string.need_added_to_my_profile),
                                Toast.LENGTH_LONG).show()
                );
            }
        }
        else if (requestCode == MyProfileActivity.REQ_ADD_NEW_ABILITY) {
            Bundle b = data.getExtras();
            if (b != null) {
                String lTagCategory = b.getString(AddEditAbilityActivity.TAG_CATEGORY);
                String lDetailedDescr = b.getString(AddEditAbilityActivity.DETAILED_DESCR);

                mSearchView.setQuery(lTagCategory, true);
                mSearchView.clearFocus();

                mSQL.addMyAbility(new Service(lTagCategory, lDetailedDescr, false));
                HttpTempo.getInstance().sendUserInformation(mSQL.getMe(), () ->
                        Toast.makeText(getActivity(),
                                getString(R.string.service_added_to_my_profile),
                                Toast.LENGTH_LONG).show()
                );
            }
        }
    }
}
