package com.bandesoft.tempo.Model;

import android.graphics.Bitmap;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MyProfileViewModel extends ViewModel {

    private MutableLiveData<Worker> mMe = new MutableLiveData<>();

    public MyProfileViewModel() {
        mMe.setValue(new Worker());
    }

    public void init(Worker me) {
        mMe.setValue(me);
    }

    public MutableLiveData<Worker> getMe() { return mMe; }

    public void editMyAbility(String prevCategory, Service newService) {
        Worker me = mMe.getValue();
        ArrayList<Service> abilities = me.getAbilities();
        for (Service a : abilities) {
            if (a.getCategory().compareTo(prevCategory) == 0) {
                a.setCategory(newService.getCategory());
                a.setDetailedDescr(newService.getDetailedDescr());
                a.setIs_A_Need(newService.getIs_A_Need());
            }
        }
        mMe.setValue(me);
    }

    public void editMyNeed(String prevcategory, Service newNeed) {
        Worker me = mMe.getValue();
        ArrayList<Service> needs = me.getNeeds();
        for (Service need : needs) {
            if (need.getCategory().compareTo(prevcategory) == 0) {
                need.setCategory(newNeed.getCategory());
                need.setDetailedDescr(newNeed.getDetailedDescr());
                need.setIs_A_Need(newNeed.getIs_A_Need());
            }
        }
        mMe.setValue(me);
    }

    public Service getMyAbility(String category) {
        ArrayList<Service> abilities = mMe.getValue().getAbilities();
        for (Service a : abilities) {
            if (a.getCategory().equals(category)) {
                return a;
            }
        }
        return null;
    }

    public Service getMyNeed(String category) {
        ArrayList<Service> needs = mMe.getValue().getNeeds();
        for (Service need : needs) {
            if (need.getCategory().equals(category)) {
                return need;
            }
        }
        return null;
    }

    public void clearAbility(String categoryiption) {
        Worker me = mMe.getValue();
        ArrayList<Service> abilities = me.getAbilities();

        for (Service a : abilities) {
            if (a.getCategory().equals(categoryiption)) {
                abilities.remove(a);
                break;
            }
        }

        mMe.setValue(me);
    }

    public void clearNeed(String categoryiption) {
        Worker me = mMe.getValue();
        ArrayList<Service> needs = me.getNeeds();

        for (Service need : needs) {
            if (need.getCategory().equals(categoryiption)) {
                needs.remove(need);
                break;
            }
        }

        mMe.setValue(me);
    }

    public void setMyName(String name) {
        Worker me = mMe.getValue();
        me.setName(name);
        mMe.setValue(me);
    }

    public void setMyLocation(LonLatCity location) {
        Worker me = mMe.getValue();
        me.setLocation(location);
        mMe.setValue(me);
    }

    // TODO maybe it should be a Bitmap reference here. possible mem leak ??
    public void setMyPic(Bitmap pic) {
        Worker me = mMe.getValue();
        me.setPic(pic);
        mMe.setValue(me);
    }

    public Bitmap getMyPic() {
        Worker me = mMe.getValue();
        if (me == null) {
            return null;
        }

        return me.getPic();
    }

}
