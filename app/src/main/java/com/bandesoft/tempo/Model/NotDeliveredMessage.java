package com.bandesoft.tempo.Model;

public class NotDeliveredMessage {
    private String mRawJson;
    private String mToJid;
    private String mSendTime;

    private NotDeliveredMessage() {}

    public NotDeliveredMessage(String rawJson, String toJid, String sendTime) {
        mRawJson = rawJson;
        mToJid = toJid;
        mSendTime = sendTime;
    }

    public String getRawJson() {
        return mRawJson;
    }

    public String getToJid() {
        return mToJid;
    }

    public String getSendTime() {
        return mSendTime;
    }
}
