package com.bandesoft.tempo.Model;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonMessage extends JSONObject {

    private static final String MESSAGE_BODY = "body";
    private static final String MESSAGE_TYPE = "message_type";
    private static final String CHAT_TOPIC = "chat_topic";

    public static final int WRONG_TYPE = -1;
    public static final int AGREEMENT_REQUEST = 0;
    public static final int AGREEMENT_ACCEPT = 1;
    public static final int NORMAL = 2;
    public static final int ASSESSMENT_DONE = 3;

    private JsonMessage() {
        super();
    }

    private JsonMessage(String json) throws JSONException {
        super(json);
    }

    public static JsonMessage parse(String rawMessage) {
        try {
            return new JsonMessage(rawMessage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String serialize(int type, String body, String chatTopic) {
        try
        {
            JsonMessage jsonMessage = new JsonMessage();
            jsonMessage.put(MESSAGE_TYPE, type);
            jsonMessage.put(MESSAGE_BODY, body);
            jsonMessage.put(CHAT_TOPIC, chatTopic);
            return jsonMessage.toString();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public int getMessageType() {
        try {
            return getInt(MESSAGE_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return WRONG_TYPE;
    }

    public String getBody() {
        try {
            return getString(MESSAGE_BODY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getChatTopic() {
        try {
            return getString(CHAT_TOPIC);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
