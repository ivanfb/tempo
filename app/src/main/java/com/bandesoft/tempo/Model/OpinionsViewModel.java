package com.bandesoft.tempo.Model;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class OpinionsViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Opinion>> mOpinions = new MutableLiveData<>();

    public MutableLiveData<ArrayList<Opinion>> getOpinions() {
        return mOpinions;
    }

    public void setOpinions(ArrayList<Opinion> opinions) {
        this.mOpinions.setValue(opinions);
    }
}
