package com.bandesoft.tempo.Model;

import com.bandesoft.tempo.Utils.Ctes;

import java.util.Calendar;

public class Contact extends Worker {
    private String mServiceTopic;
    private String mLastMessage;
    private String mLastDateTime; // last time shown in contacts list

    /** Represents the last epoch activity (unix time) */
    private long mLastTimeActivity;

    public Contact() {
        super();
    }

    public Contact(String uid, String name, String city, float mark,
                   int numVotes, double lat, double lon, String lastDateImageChanged) {
        super(uid, name, city, mark, numVotes, lat, lon, lastDateImageChanged);
    }

    public void setServiceTopic(String serviceTopic) {
        mServiceTopic = serviceTopic;
    }

    public String getServiceTopic() {
        return mServiceTopic;
    }

    public long getLastTimeActivity() {
        return mLastTimeActivity;
    }

    public void setLastMessage(String lastMessage) {
        mLastMessage = lastMessage;
    }

    public void setLastTimeActivity(long lastTimeActivity) {
        mLastTimeActivity = lastTimeActivity;
        mLastDateTime = setLastDayTime();
    }

    // Given the current contact "last time activity" value,
    // this returns the time as more human readable value.
    // Ex: yesterday; two days ago; etc
    private String setLastDayTime() {
        long nowMilliseconds = System.currentTimeMillis();
        long diffSeconds = (nowMilliseconds - mLastTimeActivity) / 1000;

        Calendar yesterday = Calendar.getInstance(); // today
        yesterday.add(Calendar.DAY_OF_YEAR, -1); // yesterday

        Calendar lastCalendarTime = Calendar.getInstance();
        lastCalendarTime.setTime(new java.util.Date(mLastTimeActivity));

        final long numSecondsInElevenMonths = 28908000L;
        final long numSecondsInMonth = 2628000L;
        final long numSecondsInYear = 31536000L;
        final long numSecondsInDay = 86400L;
        final long numSecondsTwoWeeks = 1209600L;
        final long numSecondsThreeWeeks = 1814400L;

        if (yesterday.get(Calendar.YEAR) == lastCalendarTime.get(Calendar.YEAR) &&
                yesterday.get(Calendar.DAY_OF_YEAR) == lastCalendarTime.get(Calendar.DAY_OF_YEAR))
        {
            return Ctes.getYesterday();
        }
        else if (diffSeconds > numSecondsInDay &&
                diffSeconds <= numSecondsTwoWeeks)
        {
            return Ctes.getSomeDaysAgo();
        }
        else if (diffSeconds > numSecondsTwoWeeks &&
                diffSeconds <= numSecondsThreeWeeks)
        {
            return Ctes.getThreeWeeksAgo();
        }
        else if (diffSeconds > numSecondsThreeWeeks &&
                diffSeconds <= numSecondsInMonth)
        {
            return Ctes.getAlmostOneMonth();
        }
        else if (diffSeconds > numSecondsInMonth &&
                diffSeconds <= numSecondsInElevenMonths)
        {
            return Ctes.getSomeMonthsAgo();
        }
        else if (diffSeconds > numSecondsInElevenMonths &&
                diffSeconds <= numSecondsInYear)
        {
            return Ctes.getAlmostOneYear();
        }
        else if (diffSeconds > numSecondsInYear)
        {
            return Ctes.getMoreThanOneYear();
        }
        else {
            return lastCalendarTime.get(Calendar.HOUR_OF_DAY) + ":" +
                    lastCalendarTime.get(Calendar.MINUTE);
        }
    }

    public String getLastDayTime() {
        return mLastDateTime;
    }

    public String getLastMessage() {
        return mLastMessage;
    }
}
