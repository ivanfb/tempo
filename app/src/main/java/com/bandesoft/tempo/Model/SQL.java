package com.bandesoft.tempo.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.bandesoft.tempo.Remote.AuthManager;
import com.bandesoft.tempo.Utils.ThreadPool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class SQL extends SQLiteOpenHelper {

    private static final String TAG = "SQL";

    private static final int DATABASE_VERSION = 19;

    private static final String DB_NAME = "tempo_sqlite";

    /** Table worker */
    private static final String TABLE_WORKER = "worker";
    private static final String ATT_UID = "uid";
    private static final String ATT_NAME = "name";
    private static final String ATT_LOCATION = "location";
    private static final String ATT_LOCATION_LATITUDE = "location_latitude";
    private static final String ATT_LOCATION_LONGITUDE = "location_longitude";
    private static final String ATT_AVERAGE_MARK = "average_mark";
    private static final String ATT_NUM_VOTES_RX = "num_votes_rx";
    private static final String[] WORKER_COLUMNS = {ATT_UID, ATT_NAME,
            ATT_LOCATION, ATT_AVERAGE_MARK, ATT_NUM_VOTES_RX, ATT_LOCATION_LATITUDE, ATT_LOCATION_LONGITUDE};

    private static final String CREATE_WORKER_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_WORKER +
            " (" + ATT_UID + " INTEGER PRIMARY KEY, " +
            ATT_NAME + " TEXT, " +
            ATT_LOCATION + " TEXT, " +
            ATT_AVERAGE_MARK + " REAL, " +
            ATT_NUM_VOTES_RX + " INTEGER, " +
            ATT_LOCATION_LATITUDE + " REAL, " +
            ATT_LOCATION_LONGITUDE + " REAL)";

    /** Table ability */
    private static final String TABLE_ABILITY = "ability";
    private static final String ATT_USER = "uid";
    private static final String ATT_CATEGORY = "category";
    private static final String ATT_DETAILED_DESCR = "detailed_descr";
    private static final String[] ABILITY_COLUMNS = {ATT_USER,
            ATT_CATEGORY, ATT_DETAILED_DESCR};

    private static final String CREATE_USER_ABILITY_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_ABILITY +
            " (" + ATT_USER + " INTEGER NOT NULL, " +
                    ATT_CATEGORY + " TEXT NOT NULL, " +
            ATT_DETAILED_DESCR + " TEXT NOT NULL, " +
            " PRIMARY KEY (" + ATT_USER + ", " +
                    ATT_CATEGORY + ", " + ATT_DETAILED_DESCR + "), " +
            " FOREIGN KEY (" + ATT_USER +
            ") REFERENCES " + TABLE_WORKER + " (" + ATT_UID + ") " +
            " ON UPDATE SET NULL)";

    /** Table need */
    private static final String TABLE_NEED = "need";
    private static final String[] NEED_COLUMNS = {ATT_USER,
            ATT_CATEGORY, ATT_DETAILED_DESCR};

    private static final String CREATE_USER_NEED_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NEED +
                    " (" + ATT_USER + " INTEGER NOT NULL, " +
                    ATT_CATEGORY + " TEXT NOT NULL, " +
                    ATT_DETAILED_DESCR + " TEXT NOT NULL, " +
                    " PRIMARY KEY (" + ATT_USER + ", " +
                    ATT_CATEGORY + ", " + ATT_DETAILED_DESCR + "), " +
                    " FOREIGN KEY (" + ATT_USER +
                    ") REFERENCES " + TABLE_WORKER + " (" + ATT_UID + ") " +
                    " ON UPDATE SET NULL)";

    /** Table StorableMessage - Intended to store messages with other colleagues */
    private static final String TABLE_MESSAGE = "message";
    private static final String ATT_CONTACT_UID = "uid"; // to identify the chat
    private static final String ATT_AM_I_THE_SENDER = "am_i_the_sender"; // 'true' if the msg was sent by me
    private static final String ATT_MSG_DATE_TIME = "msg_date_time"; // unix time
    private static final String ATT_MESSAGE_CONTENT = "message_content";

    private static final String[] MESSAGE_COLUMNS = {ATT_CONTACT_UID,
            ATT_AM_I_THE_SENDER, ATT_MSG_DATE_TIME, ATT_MESSAGE_CONTENT};

    private static final String CREATE_MESSAGE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_MESSAGE +
                    " (" + ATT_CONTACT_UID + " INTEGER NOT NULL, " +
                    ATT_AM_I_THE_SENDER + " BOOLEAN NOT NULL default 1, " +
                    ATT_MSG_DATE_TIME + " INTEGER, " +
                    ATT_MESSAGE_CONTENT + " TEXT, PRIMARY KEY (" +
                    ATT_CONTACT_UID + ", " + ATT_MSG_DATE_TIME + ", " +
                    ATT_AM_I_THE_SENDER + "), " +
                    " FOREIGN KEY (" + ATT_CONTACT_UID +
                    ") REFERENCES " + TABLE_WORKER + " (" + ATT_UID + ") " +
                    " ON UPDATE SET NULL)";

    /** Table ChatMetadata
     * > Info regarding the role of each counterparty (time buyer or seller)
     * > Status of the chat: before any action; action performed by one member; in agreement */
    private static final String TABLE_CHAT_METADATA = "chat_metadata";
    private static final String ATT_CHAT_ID = "uid";
    private static final String ATT_AMAGREEMENT_INITIATOR = "am_i_agreement_initiator";
    private static final String ATT_CHAT_STATUS = "chat_status";
    private static final String ATT_PENDING_MESSAGES = "there_are_pending_messages";
    private static final String ATT_CHAT_TOPIC = "chat_topic";
    private static final String ATT_LAST_ACTIVITY_TIME = "last_activity_time";
    private static final String ATT_LAST_MESSAGE = "last_message";

    private static final String[] CHAT_METADATA_COLUMNS = {ATT_CHAT_ID,
            ATT_AMAGREEMENT_INITIATOR, ATT_CHAT_STATUS, ATT_PENDING_MESSAGES, ATT_CHAT_TOPIC,
            ATT_LAST_ACTIVITY_TIME, ATT_LAST_MESSAGE};

    private static final String CREATE_CHAT_METADATA_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_CHAT_METADATA +
                    " (" + ATT_CHAT_ID + " INTEGER NOT NULL, " +
                    ATT_AMAGREEMENT_INITIATOR + " BOOLEAN NOT NULL default 1, " +
                    ATT_CHAT_STATUS + " INTEGER, " +
                    ATT_PENDING_MESSAGES + " BOOLEAN NOT NULL default 1, " +
                    ATT_CHAT_TOPIC + " TEXT, " +
                    ATT_LAST_ACTIVITY_TIME + " INTEGER, " +
                    ATT_LAST_MESSAGE + " TEXT, " +
                    "PRIMARY KEY (" + ATT_CHAT_ID + "), " +
                    " FOREIGN KEY (" + ATT_CHAT_ID +
                    ") REFERENCES " + TABLE_WORKER + " (" + ATT_UID + ") " +
                    " ON UPDATE SET NULL)";

    /** Table rx_xmpp_message */
    private static final String TABLE_RX_XMPP_MESSAGE = "rx_xmpp_message";
    private static final String ATT_RX_XMPP_STANZA_ID = "stanza_id";
    private static final String[] RX_XMPP_MESSAGE_COLUMNS = {ATT_UID, ATT_RX_XMPP_STANZA_ID};

    private static final String CREATE_RX_XMPP_MESSAGE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_RX_XMPP_MESSAGE +
                    " (" + ATT_UID + " INTEGER NOT NULL, " +
                    ATT_RX_XMPP_STANZA_ID + " TEXT NOT NULL, " +
                    " PRIMARY KEY (" + ATT_UID + "," + ATT_RX_XMPP_STANZA_ID + ") )";

    /** Table tx_xmpp_pending_message */
    private static final String TABLE_TX_XMPP_MESSAGE = "tx_xmpp_pending_message";
    private static final String ATT_TX_XMPP_STANZA_ID = "stanza_id";
    private static final String ATT_RAW_JSON = "raw_json";
    private static final String ATT_TO_JID = "to_joid";
    private static final String ATT_SEND_TIME = "send_time";

    private static final String[] TX_XMPP_MESSAGE_COLUMNS = {ATT_TX_XMPP_STANZA_ID, ATT_RAW_JSON,
            ATT_TO_JID, ATT_SEND_TIME};

    private static final String CREATE_TX_XMPP_MESSAGE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_TX_XMPP_MESSAGE +
                    " (" + ATT_TX_XMPP_STANZA_ID + " TEXT NOT NULL, " +
                    ATT_RAW_JSON + " TEXT NOT NULL, " +
                    ATT_TO_JID + " TEXT NOT NULL, " +
                    ATT_SEND_TIME + " TEXT NOT NULL, " +
                    "PRIMARY KEY (" + ATT_TX_XMPP_STANZA_ID + "))";

    private String[] allTables = {TABLE_WORKER, TABLE_ABILITY,
                                  TABLE_CHAT_METADATA, TABLE_MESSAGE, TABLE_NEED,
                                  TABLE_RX_XMPP_MESSAGE, TABLE_TX_XMPP_MESSAGE};

    private Boolean mContactSetNeedsLocalUpdate = false;
    private Boolean mContactSetNeedsRemoteUpdate = false;
    public Boolean mChatTopicChanged = false;

    public enum eChatStatus {
        STATUS_BEFORE_ANY_ACTION,
        STATUS_ONE_PERSON_STARTED_THE_DEAL,
        STATUS_AGREEMENT_REACHED
    }

    /** end of ChatMetadata table */

    private static SQL instance = null;

    private SQLiteDatabase mDB;

    private Worker me;

    private SQL(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        mDB = getWritableDatabase();
    }

    public static SQL getInstance(Context context) {
        if (instance == null) {
            instance = new SQL(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_WORKER_TABLE);
        db.execSQL(CREATE_USER_ABILITY_TABLE);
        db.execSQL(CREATE_MESSAGE_TABLE);
        db.execSQL(CREATE_CHAT_METADATA_TABLE);
        db.execSQL(CREATE_USER_NEED_TABLE);
        db.execSQL(CREATE_RX_XMPP_MESSAGE_TABLE);
        db.execSQL(CREATE_TX_XMPP_MESSAGE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {

            switch(oldVersion) {
                case 1: // upgrading from this version
                    db.execSQL(CREATE_USER_ABILITY_TABLE);
                    // we want both updates, so no break statement here...
                case 2: // upgrading from version '2'
                    db.execSQL(CREATE_MESSAGE_TABLE);
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    db.execSQL("drop table " + TABLE_MESSAGE);  // TODO pending to repopulate
                    db.execSQL(CREATE_MESSAGE_TABLE);
                case 8:
                case 9:
                    db.execSQL("drop table " + TABLE_CHAT_METADATA);  // TODO pending to repopulate
                    db.execSQL(CREATE_CHAT_METADATA_TABLE);
                case 10:
                    db.execSQL("ALTER TABLE " + TABLE_WORKER + " ADD COLUMN " + ATT_LOCATION_LATITUDE + " REAL");
                    db.execSQL("ALTER TABLE " + TABLE_WORKER + " ADD COLUMN " + ATT_LOCATION_LONGITUDE + " REAL");
                case 11:
                    db.execSQL("ALTER TABLE " + TABLE_CHAT_METADATA + " ADD COLUMN " + ATT_CHAT_TOPIC + " TEXT");
                case 12:
                    db.execSQL(CREATE_USER_NEED_TABLE);
                case 13:
                    db.execSQL("ALTER TABLE " + TABLE_CHAT_METADATA + " ADD COLUMN " + ATT_LAST_ACTIVITY_TIME + " INTEGER");
                case 14:
                    db.execSQL("ALTER TABLE " + TABLE_ABILITY +
                            " ADD CONSTRAINT PK_ability  PRIMARY KEY (" +
                            ATT_USER + ", " + ATT_CATEGORY + ", " + ATT_DETAILED_DESCR + ")");
                    db.execSQL("ALTER TABLE " + TABLE_NEED +
                            " ADD CONSTRAINT PK_ability  PRIMARY KEY (" +
                            ATT_USER + ", " + ATT_CATEGORY + ", " + ATT_DETAILED_DESCR + ")");
                case 15:
                    db.execSQL("ALTER TABLE " + TABLE_CHAT_METADATA + " ADD COLUMN " +
                            ATT_LAST_MESSAGE + " TEXT");
                case 16:
                case 17:
                    db.execSQL(CREATE_RX_XMPP_MESSAGE_TABLE);
                case 18:
                    db.execSQL(CREATE_TX_XMPP_MESSAGE_TABLE);
           }
        }
        catch (SQLiteException e) // avoid strange crashes on Samung Galaxy A50
        {
            Log.e(TAG, "Exception " + e.getMessage());
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

    private ContentValues createWorkerValues(Worker w) {
        ContentValues values = new ContentValues();

        values.put(ATT_UID, w.getUid());
        values.put(ATT_NAME, w.getName());
        values.put(ATT_LOCATION, w.getLocation().getCityName());
        values.put(ATT_NUM_VOTES_RX, 0);
        values.put(ATT_AVERAGE_MARK, 0);
        values.put(ATT_LOCATION_LATITUDE, w.getLocation().getLat());
        values.put(ATT_LOCATION_LONGITUDE, w.getLocation().getLon());

        return values;
    }

    private ContentValues createAbilityValues(String uid, Service a) {
        ContentValues values = new ContentValues();

        values.put(ATT_USER, uid);
        values.put(ATT_CATEGORY, a.getCategory());
        values.put(ATT_DETAILED_DESCR, a.getDetailedDescr());

        return values;
    }

    /** Changes the worker by the values of the worker given as a paramter  */
    private void setWorker(Worker w) {
        Log.d(TAG, "setWorker " + w.getName());

        ThreadPool.instance.runTask(() -> mDB.update(TABLE_WORKER, createWorkerValues(w),
                                            " uid = "+ w.getUid(),
                                            null));
    }

    public Worker getMe() {
        if (me == null) {
            String uid = AuthManager.instance.getUserId();
            if (uid.compareTo(AuthManager.INVALID_USER) != 0) {
                me = getWorker(uid);
            }

            if (me == null) { // the user should be created and added to database
                me = new Worker(uid, "null_name", "",
                        0, 0, 0, 0, "");
                Log.d(TAG, "addWorker " + me.getName());

                ThreadPool.instance.runTask(() ->
                        mDB.insert(TABLE_WORKER, null, createWorkerValues(me)));
            }
        }

        return me;
    }

    public void addMeAtLogin(String uid, String name, String city, double lat, double lon,
                             float averageMark, int numVotes) {
        me = new Worker(uid, "", "",
                0, 0, 0, 0, "");
        me.setUid(uid);
        me.setName(name);
        me.setCity(city);
        me.setLonLat(lon, lat);
        me.setAvgMark(averageMark);
        me.setNumVotes(numVotes);

        insertOrUpdateWorker(me);
    }

    private void insertOrUpdateWorker(Worker worker) {
        ContentValues workerValues = createWorkerValues(worker);

        // this is to avoid possible exception.
        int id = (int) mDB.insertWithOnConflict(TABLE_WORKER,
                null, workerValues, SQLiteDatabase.CONFLICT_IGNORE);
        if (id == -1) {
            mDB.update(TABLE_WORKER, workerValues,
                    ATT_UID + "= ?",
                    new String[] {worker.getUid()});
        }
    }

    // This is aimed to avoid delays when showing my profile (IOffer) for the very first time
    public void queryMeInBackground() {
        ThreadPool.instance.runTask(() ->
        {
            if (me == null) {
                String uid = AuthManager.instance.getUserId();
                if (uid.compareTo(AuthManager.INVALID_USER) != 0) {
                    me = getWorker(uid);
                }
            }
        });
    }

    private Worker getWorker(String uid) {
        Cursor cursor = mDB.query(TABLE_WORKER,
                WORKER_COLUMNS,
                " uid =" + uid,
                null,
                null,
                null,
                null,
                null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            Worker w = new Worker(cursor.getString(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getFloat(3),
                    cursor.getInt(4),
                    cursor.getDouble(5),
                    cursor.getDouble(6),
                    ""); // TODO: review this

            cursor.close();

            ArrayList<Service> abilities = queryWorkerAbilities(w.getUid());
            if (abilities != null)
            {
                for (Service userService : abilities) {
                    w.addService(userService);
                }
            }

            ArrayList<Service> needs = queryWorkerNeeds(w.getUid());
            if (needs != null)
            {
                for (Service userNeed : needs) {
                    w.addNeed(userNeed);
                }
            }

            return w;
        }
        return null;
    }

    public void clearAbilitiesNeedsFromContacts() {
        String whereClause = ATT_USER + " != ?";
        String[] whereArgs = new String[]{AuthManager.instance.getUserId()};
        mDB.delete(TABLE_ABILITY, whereClause, whereArgs);
        mDB.delete(TABLE_NEED, whereClause, whereArgs);
    }

    public ArrayList<String> getAllContactIDs() {
        ArrayList<String> lRet = new ArrayList<>();
        for (Contact contact : getAllContacts().values()) {
            lRet.add(contact.getUid());
        }

        return lRet;
    }

    public int getNumChatsWithPendingMessages() {
        int ret = 0;

        Cursor cursor = mDB.query(TABLE_CHAT_METADATA,
                CHAT_METADATA_COLUMNS,
                ATT_PENDING_MESSAGES + " = 1",
                null,
                null,
                null,
                null,
                null);

        if (cursor != null){
            cursor.moveToFirst();
            ret = cursor.getCount();
            cursor.close();
        }

        return ret;
    }

    public long getNumWorkers() {
        return DatabaseUtils.queryNumEntries(mDB, TABLE_WORKER);
    }

    public Map<String, Contact> getAllContacts() {
        // get all workers but me
        String columns = "";
        for (String column: WORKER_COLUMNS) {
            columns += TABLE_WORKER + "." + column + ",";
        }
        columns += ATT_CHAT_TOPIC + "," + ATT_LAST_ACTIVITY_TIME + "," + ATT_LAST_MESSAGE;

        Cursor cursor = mDB.rawQuery("SELECT " +
                columns + " " +
                "FROM " + TABLE_WORKER + " LEFT JOIN " + TABLE_CHAT_METADATA + " ON " +
                TABLE_WORKER+"."+ATT_UID + " = " + TABLE_CHAT_METADATA+"."+ATT_CHAT_ID + " " +
                "WHERE worker.uid != " + AuthManager.instance.getUserId(), null);

        HashMap<String, Boolean> pendingMessages = getPendingMessagesList();

        Map<String, Contact> contacts = new HashMap<>();
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                Contact c = new Contact(cursor.getString(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getFloat(3),
                        cursor.getInt(4),
                        cursor.getDouble(5),
                        cursor.getDouble(6),
                        ""); // TODO: review this

                c.setServiceTopic(cursor.getString(7));
                c.setLastTimeActivity(cursor.getLong(8));
                c.setLastMessage(cursor.getString(9));

                if (pendingMessages.size() > 0 &&
                        pendingMessages.containsKey(c.getUid())) {
                    c.setHasPendingMessages(pendingMessages.get(c.getUid()));
                }

                contacts.put(c.getUid(), c);

                ArrayList<Service> abilities = queryWorkerAbilities(c.getUid());
                if (abilities != null)
                {
                    for (Service userService : abilities) {
                        c.addService(userService);
                    }
                }

                ArrayList<Service> needs = queryWorkerNeeds(c.getUid());
                if (needs != null)
                {
                    for (Service userNeed : needs) {
                        c.addNeed(userNeed);
                    }
                }

            } while (cursor.moveToNext());

            cursor.close();
        }
        return contacts;
    }

    public void removeEverything() {
        if (me != null) {
            me.clear();
        }

        for (String table: allTables) {
            mDB.delete(table, null, null);
        }

        me = null;
    }

    /**
     * Deletes the list of uid's passed as a parameter
     * @param uidToRemove
     */
    public void removeContacts(String[] uidToRemove) {
        String whereClause = String.format(ATT_UID +
                " in (%s)",
                new Object[]
                        { TextUtils.join(",",
                                Collections.nCopies(uidToRemove.length, "?")) });

        ThreadPool.instance.runTask(() ->
        {
            mDB.delete(TABLE_WORKER,
                    whereClause,
                    uidToRemove);

            mDB.delete(TABLE_MESSAGE,
                    whereClause,
                    uidToRemove);

            mDB.delete(TABLE_CHAT_METADATA,
                    whereClause,
                    uidToRemove);
        });
    }

    private ArrayList<Service> queryWorkerAbilities(String uid) {
        Cursor cursor = mDB.query(TABLE_ABILITY,
                ABILITY_COLUMNS,
                " " + ATT_USER + " = " + uid,
                null,
                null,
                null,
                null,
                null);

        ArrayList<Service> ret = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();

            do {
                ret.add(new Service(cursor.getString(1),
                        cursor.getString(2),
                        false));
            } while (cursor.moveToNext());

            cursor.close();

            return ret;
        }

        return null;
    }

    private ArrayList<Service> queryWorkerNeeds(String uid) {
        Cursor cursor = mDB.query(TABLE_NEED,
                NEED_COLUMNS,
                " " + ATT_USER + " = " + uid,
                null,
                null,
                null,
                null,
                null);

        ArrayList<Service> ret = new ArrayList<>();
        if (cursor != null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();

            do {
                ret.add(new Service(cursor.getString(1),
                        cursor.getString(2),
                        true));
            } while (cursor.moveToNext());

            cursor.close();

            return ret;
        }

        return null;
    }

    public void updateMyName(String name) {
        Worker w = getMe();
        if (w != null) {
            w.setName(name);
            setWorker(w);
        }
        else {
            Log.e(TAG, "Error getting 'me' in updateMyName");
        }
    }

    public void updateMyInfo(String name,
                             LonLatCity location) {
        Worker w = getMe();
        if (w != null) {
            w.setName(name);
            w.setLocation(location);
            setWorker(w);
            // abilities are added right after the result is got from 'AddEditAbility'
        }
        else {
            Log.e(TAG, "Error getting 'me' in updateMyInfo");
        }
    }

    public void updateMyLocation(LonLatCity location) {
        Worker w = getMe();
        if (w != null) {
            w.setLocation(location);
            setWorker(w);
        }
        else {
            Log.e(TAG, "Error getting 'me' in updateMyLocation");
        }
    }

    public void removeMyAbility(String category) {
        Worker w = getMe();
        if (w != null) {
            Log.d(TAG, "removeMyAbility " + category);
            String whereClause = ATT_USER + " = '" + me.getUid() +
                    "' and " + ATT_CATEGORY + " = ?";

            ThreadPool.instance.runTask(
                    () -> mDB.delete(TABLE_ABILITY,
                            whereClause,
                            new String[]{category})
            );
        }
        else {
            Log.e(TAG, "Error getting 'me' in removeMyAbility");
        }
    }

    public void removeMyNeed(String category) {
        Worker w = getMe();
        if (w != null) {
            Log.d(TAG, "removeMyNeed " + category);
            String whereClause = ATT_USER + " = '" + me.getUid() +
                    "' and " + ATT_CATEGORY + " = ?";

            ThreadPool.instance.runTask(() -> mDB.delete(TABLE_NEED,
                                                         whereClause,
                                                         new String[]{category}));
        }
        else {
            Log.e(TAG, "Error getting 'me' in removeMyNeed");
        }
    }

    public void editMyAbility(String prevShortDesc, Service newService) {
        Worker w = getMe();
        if (w != null) {
            String whereClause = ATT_USER + " = '" + w.getUid() +
                    "' and " + ATT_CATEGORY + " = ?";

            ThreadPool.instance.runTask(() -> mDB.update(TABLE_ABILITY,
                    createAbilityValues(w.getUid(), newService),
                    whereClause,
                    new String[]{prevShortDesc})
            );
        }
        else {
            Log.e(TAG, "Error getting 'me' in editMyAbility");
        }
    }

    public void editMyNeed(String prevShortDesc, Service newNeed) {
        Worker w = getMe();
        if (w != null) {
            String whereClause = ATT_USER + " = '" + w.getUid() +
                    "' and " + ATT_CATEGORY + " = ?";

            ThreadPool.instance.runTask(() -> mDB.update(TABLE_NEED,
                    createAbilityValues(w.getUid(), newNeed),
                    whereClause,
                    new String[]{prevShortDesc})
            );
        }
        else {
            Log.e(TAG, "Error getting 'me' in editMyNeed");
        }
    }

    // Adds ability to myself
    public void addMyAbility(Service a) {
        Worker w = getMe();
        if (w != null) {
            Log.d(TAG, "addAbility " + a.getCategory());
            ThreadPool.instance.runTask(() ->
                    mDB.insert(TABLE_ABILITY, null,
                    createAbilityValues(w.getUid(), a)));
            w.addService(a);
        }
        else {
            Log.e(TAG, "Error getting 'me' in addAbility");
        }
    }

    public void addContactAbility(Contact contact, String uid, Service a) {
        Log.d(TAG, "addContactAbility " + a.getCategory() + " to " + uid);
        addOrUpdateAbility(TABLE_ABILITY, contact, uid, a);
    }

    private void addOrUpdateAbility(String table, Contact contact, String uid, Service a) {
        ThreadPool.instance.runTask(() ->
        {
            boolean ok = true;
            ContentValues abilityValues = createAbilityValues(uid, a);

            int id = (int) mDB.insertWithOnConflict(table,
                    null, abilityValues, SQLiteDatabase.CONFLICT_IGNORE);
            if (id == -1) {
                try {
                    mDB.update(table, abilityValues, ATT_UID + " = ? AND " +
                                    ATT_CATEGORY + " = ?",
                            new String[]{uid, a.getCategory()});

                } catch (Exception e) {
                    ok = false;
                    // ignoring this exception. Given the use of relational database, here we
                    // could receive the same row multiple times because of the triple join
                }
            }

            if (ok) {
                if (table.compareTo(TABLE_ABILITY) == 0)
                {
                    contact.addService(a);
                }
                else
                {
                    contact.addNeed(a);
                }
            }

        });
    }

    public void addContactNeed(Contact contact, String uid, Service a) {
        Log.d(TAG, "addContactNeed " + a.getCategory() + " to " + uid);
        addOrUpdateAbility(TABLE_NEED, contact, uid, a);
    }

    public void addMyNeed(Service need) {
        Worker w = getMe();
        if (w != null) {
            Log.d(TAG, "addNeed " + need.getCategory());
            ThreadPool.instance.runTask(() -> mDB.insert(TABLE_NEED, null,
                                              createAbilityValues(w.getUid(), need)));
            w.addNeed(need);
        }
        else {
            Log.e(TAG, "Error getting 'me' in addNeed");
        }
    }

    public void clearAllTxXmppMessages() {
        mDB.delete(TABLE_TX_XMPP_MESSAGE, null, null);
    }

    public void clearAllRxXmppMessages() {
        mDB.delete(TABLE_RX_XMPP_MESSAGE, null, null);
    }

    public void addTxXmppMessage(String stanzaId, String rawJson, String toJid, String sendTime) {
        try {
            Log.d(TAG, "addTxXmppMessage rawJson [" + rawJson + "] [" + stanzaId + "]");

            ContentValues values = new ContentValues();
            values.put(ATT_TX_XMPP_STANZA_ID, stanzaId);
            values.put(ATT_RAW_JSON, rawJson);
            values.put(ATT_TO_JID, toJid);
            values.put(ATT_SEND_TIME, sendTime);

            mDB.insert(TABLE_TX_XMPP_MESSAGE, null, values);

        } catch (Exception e) {
            Log.e(TAG, "Error addTxXmppMessage rawJson [" + rawJson + "] [" + stanzaId + "] " +
                    "error [" + e.getMessage() + "]");
        }
    }

    public void addRxXmppMessage(String uid, String stanzaId) {
        try {
            Log.d(TAG, "addRxXmppMessage uid [" + uid + "] [" + stanzaId + "]");

            ContentValues values = new ContentValues();
            values.put(ATT_UID, uid);
            values.put(ATT_RX_XMPP_STANZA_ID, stanzaId);

            mDB.insert(TABLE_RX_XMPP_MESSAGE, null, values);

        } catch (Exception e) {
            Log.e(TAG, "Error addRxXmppMessage uid [" + uid + "] [" + stanzaId + "] " +
                    "error [" + e.getMessage() + "]");
        }
    }

    public HashMap<String, LinkedList<String>> getAllRxXmppMessages() {
        Cursor cursor = mDB.query(TABLE_RX_XMPP_MESSAGE,
                RX_XMPP_MESSAGE_COLUMNS,
                null,
                null,
                null,
                null,
                null,
                null);

        HashMap<String, LinkedList<String>> ret = new HashMap<>();

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                String uid = cursor.getString(0); // uid

                if (!ret.containsKey(uid)) {
                    ret.put(uid, new LinkedList<>());
                }

                String stanzaId = cursor.getString(1); // stanzaId
                ret.get(uid).add(stanzaId);

            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }

        return ret;
    }

    public HashMap<String, NotDeliveredMessage> getAllTxXmppMessages() {
        Cursor cursor = mDB.query(TABLE_TX_XMPP_MESSAGE,
                TX_XMPP_MESSAGE_COLUMNS,
                null,
                null,
                null,
                null,
                null,
                null);

        HashMap<String, NotDeliveredMessage> ret = new HashMap<>();

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                String stanzaId = cursor.getString(0); // stanzaId

                if (!ret.containsKey(stanzaId)) {
                    ret.put(stanzaId,
                            new NotDeliveredMessage(cursor.getString(1) /* raw json */,
                                    cursor.getString(2) /* to JID */,
                            cursor.getString(3) /* send time */));
                }

            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }

        return ret;
    }

    // Adds ability to the worker
    public void addAbilityInCurrentThread(Service a) {
        Worker w = getMe();
        if (w != null) {
            Log.d(TAG, "addAbility " + a.getCategory());
            mDB.insert(TABLE_ABILITY, null, createAbilityValues(w.getUid(), a));
            w.addService(a);
        }
        else {
            Log.e(TAG, "Error getting 'me' in addAbility");
        }
    }

    public void addNeedInCurrentThread(Service need) {
        Worker w = getMe();
        if (w != null) {
            Log.d(TAG, "addNeed " + need.getCategory());
            mDB.insert(TABLE_NEED, null, createAbilityValues(w.getUid(), need));
            w.addNeed(need);
        }
        else {
            Log.e(TAG, "Error getting 'me' in addNeed");
        }
    }

    /**
     * Creates, or updates, a worker within the database.
     *
     * @param uid The id that represents the contact
     * @param name
     */
    public void storeContactWithJustName(String uid, String name) {
        ContactsViewModel.mContactInfoObtainedFromServer = false; // a refresh is needed

        Worker w = new Worker();
        w.setUid(uid);
        w.setName(name);

        ThreadPool.instance.runTask(() -> insertOrUpdateWorker(w));
    }

    public void storeContact(Contact contact) {
        ThreadPool.instance.runTask(() -> insertOrUpdateWorker(contact));
        setChatTopic(contact.getUid(),
                contact.getServiceTopic(),
                false);
    }

    /**
     * Adding message into database
     * @param contactUID Contact identifier
     * @param message
     * @param timestamp unix time
     */
    public void addMessage(String contactUID, String message,
                           String timestamp,
                           boolean amISender,
                           String chatTopic,
                           boolean pendingMessages) {
        try {
            ThreadPool.instance.runTask(() -> {
                ContentValues values = new ContentValues();

                values.put(ATT_CONTACT_UID, contactUID); // this identifies the chat
                values.put(ATT_AM_I_THE_SENDER, amISender); // if 'true', the message was sent by me
                values.put(ATT_MSG_DATE_TIME, timestamp);
                values.put(ATT_MESSAGE_CONTENT, message);

                mDB.insert(TABLE_MESSAGE, null, values);

                // update last time activity for the chat
                ContentValues metadataValues = new ContentValues();
                metadataValues.put(ATT_CHAT_ID, contactUID);
                metadataValues.put(ATT_LAST_ACTIVITY_TIME, timestamp);
                metadataValues.put(ATT_CHAT_TOPIC, chatTopic);
                metadataValues.put(ATT_PENDING_MESSAGES, pendingMessages ? 1 : 0);
                metadataValues.put(ATT_LAST_MESSAGE, message);

                int id = (int) mDB.insertWithOnConflict(TABLE_CHAT_METADATA,
                        null, metadataValues, SQLiteDatabase.CONFLICT_IGNORE);
                if (id == -1) {
                    mDB.update(TABLE_CHAT_METADATA, metadataValues,
                            ATT_CHAT_ID + "= ?", new String[]{contactUID});
                }
                mContactSetNeedsLocalUpdate = true;
            });

        } catch (Exception e) {
            Log.e(TAG, "Exception in addMessage", e);
        }
    }

    public ArrayList<StorableMessage> getLastMessages(int maxNumMessages,
                                                      String contactUID) {
        Cursor cursor = mDB.query(TABLE_MESSAGE,
                MESSAGE_COLUMNS,
                 ATT_CONTACT_UID+ " = ? ",
                new String[] {contactUID},
                null,
                null,
                ATT_MSG_DATE_TIME + " DESC ",
                Integer.toString(maxNumMessages));

        if (cursor != null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();

            ArrayList<StorableMessage> ret = new ArrayList<>();
            do {
                boolean am_I_Sender = cursor.getInt(1) == 1;
                long timestamp = cursor.getLong(2);
                String body = cursor.getString(3);

                ret.add(new StorableMessage(body, am_I_Sender, timestamp));
            }
            while (cursor.moveToNext());

            cursor.close();

            return ret;
        }

        return null;
    }

    public void setChatInfo (String chatId, boolean am_I_agreement_initiator, eChatStatus status,
                             String chatTopic) {
        ThreadPool.instance.runTask(() ->
            {
                ContentValues values = new ContentValues();
                values.put(ATT_CHAT_ID, chatId);
                values.put(ATT_AMAGREEMENT_INITIATOR, am_I_agreement_initiator ? 1:0);
                values.put(ATT_CHAT_STATUS, status.ordinal());
                values.put(ATT_CHAT_TOPIC, chatTopic);

                int id = (int) mDB.insertWithOnConflict(TABLE_CHAT_METADATA,null,
                        values, SQLiteDatabase.CONFLICT_IGNORE);
                if (id == -1) {
                    mDB.update(TABLE_CHAT_METADATA, values,
                            ATT_CHAT_ID + "= ?",
                            new String[] {chatId});
                }
            }
        );
    }

    public void updateChatStatus(String chatId, eChatStatus status) {
        ThreadPool.instance.runTask(() ->
        {
            ContentValues values = new ContentValues();
            values.put(ATT_CHAT_ID, chatId);
            values.put(ATT_CHAT_STATUS, status.ordinal());

            int id = (int) mDB.insertWithOnConflict(TABLE_CHAT_METADATA,
                    null, values, SQLiteDatabase.CONFLICT_IGNORE);
            if (id == -1) {
                mDB.update(TABLE_CHAT_METADATA, values,
                        ATT_CHAT_ID + "= ?",
                        new String[] {chatId});
            }
        });
    }

    // TODO make queries asynchronous
    public eChatStatus getChatStatus(String chatId) {
        Cursor cursor = mDB.query(TABLE_CHAT_METADATA,
                CHAT_METADATA_COLUMNS,
                " " + ATT_CHAT_ID + " = ?",
                new String[]{chatId},
                null,
                null,
                null,
                null);

        if (cursor != null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            int ret = cursor.getInt(2);
            cursor.close();
            return eChatStatus.values()[ret];
        }

        return eChatStatus.STATUS_BEFORE_ANY_ACTION;
    }

    public boolean am_I_agreementInitiator(String uid) {
        Cursor cursor = mDB.query(TABLE_CHAT_METADATA,
                CHAT_METADATA_COLUMNS,
                " " + ATT_CHAT_ID + " = ?",
                new String[]{uid},
                null,
                null,
                null,
                null);

        if (cursor != null && cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            int ret = cursor.getInt(1);
            cursor.close();
            return ret == 1;
        }
        return false;
    }

    public void setChatTopic(String uid, String chatTopic, boolean resetPendingMsgs) {

        ThreadPool.instance.runTask(() -> {
            ContentValues values = new ContentValues();
            values.put(ATT_CHAT_ID, uid);
            values.put(ATT_CHAT_TOPIC, chatTopic);
            if (resetPendingMsgs) {
                values.put(ATT_PENDING_MESSAGES, 0);
            }

            try {
                int id = (int) mDB.insertWithOnConflict(TABLE_CHAT_METADATA,
                        null, values, SQLiteDatabase.CONFLICT_IGNORE);
                if (id == -1) {
                    mDB.update(TABLE_CHAT_METADATA, values,
                            ATT_CHAT_ID + "= ?",
                            new String[]{uid});
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception in 'setChatTopic' [" + uid + "] [" + chatTopic + "] " +
                        e.getMessage(), e);
            }
        });

        mContactSetNeedsRemoteUpdate = true;
        mChatTopicChanged = true;
    }

    /** Sets the user as having pending messages to be read */
    public void setPendingMsgsForUser(String uid, boolean pendingMsgs) {
        ThreadPool.instance.runTask(() ->
        {
            ContentValues values = new ContentValues();
            values.put(ATT_CHAT_ID, uid);
            values.put(ATT_PENDING_MESSAGES, pendingMsgs ? 1:0);

            try {
                int id = (int) mDB.insertWithOnConflict(TABLE_CHAT_METADATA,
                        null, values, SQLiteDatabase.CONFLICT_IGNORE);
                if (id == -1) {
                    mDB.update(TABLE_CHAT_METADATA, values,
                            ATT_CHAT_ID + "= ?",
                            new String[]{uid});
                }

                if (pendingMsgs) {
                    mContactSetNeedsRemoteUpdate = true;
                }
            } catch (Exception e) {
                Log.e(TAG, "Exception in 'setPendingMsgsForUser' [" + uid + "] [" +
                        (pendingMsgs ? "YES":"NO") + "] " +
                        e.getMessage(), e);
            }
        });
    }

    /**
     * Returns a boolean array that indicate if each user has pending messages
     * to be read.
     */
    public HashMap<String, Boolean> getPendingMessagesList() {
        Cursor cursor = mDB.query(TABLE_CHAT_METADATA,
                CHAT_METADATA_COLUMNS,
                null,
                null,
                null,
                null,
                null,
                null);

        HashMap<String, Boolean> ret = new HashMap<>();

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            do {
                String uid = cursor.getString(0); // uid
                ret.put(uid, cursor.getInt(3) == 1);
            } while (cursor.moveToNext());
        }

        if (cursor != null) {
            cursor.close();
        }

        return ret;
    }

    /** This is intended to reorder the contacts list when new messages are sent/received */
    public Boolean contactsNeedLocalUpdate() {
        return mContactSetNeedsLocalUpdate;
    }

    public void resetContactsNeedLocalUpdate() {
         mContactSetNeedsLocalUpdate = false;
    }

    public Boolean contactSetNeedsRemoteUpdate() {
        return mContactSetNeedsRemoteUpdate;
    }

    public void resetContactSetNeedsRemoteUpdate() {
        mContactSetNeedsRemoteUpdate = false;
    }
}
