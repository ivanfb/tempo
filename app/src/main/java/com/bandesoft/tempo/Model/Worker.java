package com.bandesoft.tempo.Model;

import android.graphics.Bitmap;

import com.bandesoft.tempo.Remote.AuthManager;
import com.bandesoft.tempo.Utils.Ctes;

import java.io.Serializable;
import java.util.ArrayList;

public class Worker implements Serializable {
    private String uid;
    private String name = "";
    private float mark; // average mark given by other users
    private int numVotes; // number of votes received
    private ArrayList<Service> abilities = new ArrayList<>();
    private ArrayList<Service> needs = new ArrayList<>();
    private LonLatCity location = new LonLatCity();
    private Bitmap pic;
    private boolean selected = false; // needed to handle selections in list views
    private String lastDateImageChanged; // used as StringSignature in glide (user's images).

    private boolean hasPendingMessages = false;

    public Worker() {

    }

    public Worker(String uid, String name, String city,
                  float mark, int numVotes, double lat, double lon,
                  String lastDateImageChanged) {
        this.uid = uid;
        this.name = name;
        this.location.setCityName(city);
        this.location.setLat(lat);
        this.location.setLon(lon);
        this.mark = mark;
        this.numVotes = numVotes;
        this.abilities = new ArrayList<>();
        this.lastDateImageChanged = lastDateImageChanged;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getMark() {
        return mark;
    }

    public void setLastDateImageChanged(String lastChanged) { lastDateImageChanged = lastChanged; }

    public String getLastDateImageChanged() { return lastDateImageChanged; }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public void setCity(String city) { this.location.setCityName(city); }

    public void setLonLat(double lon, double lat) {
        this.location.setLon(lon);
        this.location.setLat(lat);
    }

    public void setAvgMark(float mark) { this.mark = mark; }

    public void setNumVotes(int numVotes) { this.numVotes = numVotes; }

    public int getNumVotes() {
        return numVotes;
    }

    public void addService(Service service) {
        boolean found = false;
        for(Service a: this.abilities) {
            if (a.getCategory().compareTo(service.getCategory()) == 0) {
                a.setDetailedDescr(service.getDetailedDescr());
                found = true;
            }
        }

        if (!found) {
            this.abilities.add(service);
        }
    }

    public void addNeed(Service need) {
        boolean found = false;
        for(Service a: this.needs) {
            if (a.getCategory().compareTo(need.getCategory()) == 0 &&
                a.getDetailedDescr().compareTo(need.getDetailedDescr()) == 0)
            {
                found = true;
            }
        }

        if (!found) {
            this.needs.add(need);
        }
    }

    public void setLocation(LonLatCity location) {
        this.location = location;
    }

    public ArrayList<Service> getAbilities() {
        return this.abilities;
    }

    public ArrayList<Service> getNeeds() {
        return this.needs;
    }

    public LonLatCity getLocation() {
        return this.location;
    }

    public void setPic(Bitmap pic) {
        this.pic = pic;
    }

    public Bitmap getPic() {
        return pic;
    }

    public String getServDescriptions(boolean lIsSearchNeedActive) {
        ArrayList<Service> list = lIsSearchNeedActive ? this.abilities : this.needs;

        StringBuilder lRet = new StringBuilder();
        if (list.size() > 0) {
            int lNumAbMinusOne = list.size() - 1;
            int i = 0;

            for (; i < lNumAbMinusOne; i++) {
                String lDescr = list.get(i).getDetailedDescr();

                int lCarriageRetIndex = lDescr.indexOf('\n');
                if (lCarriageRetIndex != -1) {
                    final int MAX_LINE_LENGTH = 80;
                    int lIndexWhereToCut = Math.min(lCarriageRetIndex, MAX_LINE_LENGTH);
                    lRet.append("· ").append(lDescr.substring(0, lIndexWhereToCut)).append('\n');
                }
                else {
                    lRet.append("· ").append(lDescr).append('\n');
                }
            }

            String lDescr = list.get(i).getDetailedDescr();
            lRet.append("· ").append(lDescr);
        }
        return lRet.toString();
    }

    public String getCategories(boolean lIsSearchNeedActive) {

        ArrayList<Service> list = lIsSearchNeedActive ? this.abilities : this.needs;

        StringBuilder lRet = new StringBuilder();
        if (list.size() > 0) {
            int lNumAbMinusOne = list.size() - 1;
            int i = 0;

            for (; i < lNumAbMinusOne; i++) {
                String lCat = Ctes.translateCategoryFromTag(list.get(i).getCategory());

                lRet.append("· ").append(lCat).append('\n');
            }

            String lCat = Ctes.translateCategoryFromTag(list.get(i).getCategory());
            lRet.append("· ").append(lCat);
        }
        return lRet.toString();
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setHasPendingMessages(boolean hasPendingMessages) {
        this.hasPendingMessages = hasPendingMessages;
    }

    public boolean hasPendingMsgs() {
        return hasPendingMessages;
    }

    public void clear() {
        uid = AuthManager.INVALID_USER;
        name = "";
        mark = 0;
        numVotes = 0;
        abilities.clear();
        location.clear();
        lastDateImageChanged = "";
    }
}
