package com.bandesoft.tempo.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Opinion implements Parcelable {
    private String mCounterpartyName;
    private String mDate;
    private float mRating;
    private String mComment;

    public Opinion(String counterpartyName, String date, float rating, String comment) {
        this.mCounterpartyName = counterpartyName;
        this.mDate = date;
        this.mRating = rating;
        this.mComment = comment;
    }

    protected Opinion(Parcel in) {
        mCounterpartyName = in.readString();
        mDate = in.readString();
        mRating = in.readFloat();
        mComment = in.readString();
    }

    public float getRating() {
        return mRating;
    }

    public String getEvaluatorName() {
        return mCounterpartyName;
    }

    public String getDate() {
        return mDate;
    }

    public String getComment() { return mComment; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mCounterpartyName);
        parcel.writeString(mDate);
        parcel.writeFloat(mRating);
        parcel.writeString(mComment);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public Object createFromParcel(Parcel parcel) {
            return new Opinion(parcel);
        }

        @Override
        public Object[] newArray(int size) {
            return new Opinion[size];
        }
    };
}
