package com.bandesoft.tempo.Model;

/** This class permits to keep in memory SQLite messages */
public class StorableMessage {
    public String rawBody;
    public boolean am_I_the_Sender;
    public long timestamp;

    public StorableMessage(String rawBody, boolean am_I_the_Sender,
                           long timestamp) {
        this.rawBody = rawBody;
        this.am_I_the_Sender = am_I_the_Sender;
        this.timestamp = timestamp;
    }
}
