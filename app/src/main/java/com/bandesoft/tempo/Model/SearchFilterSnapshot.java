package com.bandesoft.tempo.Model;

public class SearchFilterSnapshot {

    private boolean mIsSearchOfferedActive;

    private String mTagCategorySearch;
    private String mCategorySearch;
    private String mCitySearch;
    private double mLatSearch;
    private double mLonSearch;
    private int mRadiusKm;

    private SearchFilterSnapshot() {}

    public SearchFilterSnapshot(boolean lIsSearchOfferedActive,
                                String lTagCategorySearch,
                                String lCategorySearch,
                                String lCitySearch,
                                double lLatSearch,
                                double lLonSearch,
                                int lRadiusSearchKm) {
        this.mIsSearchOfferedActive = lIsSearchOfferedActive;
        this.mTagCategorySearch = lTagCategorySearch;
        this.mCategorySearch = lCategorySearch;
        this.mCitySearch = lCitySearch;
        this.mLatSearch = lLatSearch;
        this.mLonSearch = lLonSearch;
        this.mRadiusKm = lRadiusSearchKm;
    }

    public boolean isSearchOfferedActive() {
        return mIsSearchOfferedActive;
    }

    public String getTagCategorySearch() {
        return mTagCategorySearch;
    }

    public String getCategorySearch() {
        return mCategorySearch;
    }

    public String getCitySearch() {
        return mCitySearch;
    }

    public double getLatSearch() {
        return mLatSearch;
    }

    public double getLonSearch() {
        return mLonSearch;
    }

    public int getRadiusKm() {
        return mRadiusKm;
    }

}
