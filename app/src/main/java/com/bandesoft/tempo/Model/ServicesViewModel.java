package com.bandesoft.tempo.Model;

import android.content.Context;

import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Utils.Prefs;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ServicesViewModel extends ViewModel {

    private static final String TAG = "ServicesViewModel";

    private MutableLiveData<ArrayList<Worker>> mWorkers;

    public MutableLiveData<ArrayList<Worker>> getWorkersList(Context context,
                                                             double lon,
                                                             double lat,
                                                             String tagCategory,
                                                             String textToSearch,
                                                             int radiusKm) {
        if (mWorkers == null) {
            mWorkers = new MutableLiveData<>();
        }

        boolean iOfferSearch = !Prefs.instance.isSearchOfferedServicesActive();

        HttpTempo.getInstance().requestNearServices(context,
                lon, lat,
                tagCategory, textToSearch, radiusKm, iOfferSearch,
                services -> mWorkers.setValue(services));

        return mWorkers;
    }

    public Worker getWorkerAtIndex(int index) {
        if (index >= 0 && index < mWorkers.getValue().size()) {
            return mWorkers.getValue().get(index);
        }
        return null;
    }
}
