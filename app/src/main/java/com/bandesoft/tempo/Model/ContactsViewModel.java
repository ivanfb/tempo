package com.bandesoft.tempo.Model;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bandesoft.tempo.Remote.AuthManager;
import com.bandesoft.tempo.Remote.HttpTempo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ContactsViewModel extends ViewModel {

    private static final String TAG = "ContactsViewModel";

    public static boolean mContactInfoObtainedFromServer = false;
    private SQL mSQL;
    private MutableLiveData<Map<String, Contact>> mContacts = new MutableLiveData<>();

    public ContactsViewModel() {
        mContacts.setValue(new HashMap<>());
    }

    public MutableLiveData<Map<String, Contact>> getContacts(Context context) {
        if (!mContactInfoObtainedFromServer) {
            requestMoreInfoFromContacts(context);
            mContactInfoObtainedFromServer = true;
        }

        return mContacts;
    }

    public void updateContactsFromSql() {
        mContacts.setValue(mSQL.getAllContacts());
    }

    public void setSQL_instance(SQL sql) {
        this.mSQL = sql;
    }

    public void removeContacts(String[] uidsToRemove) {
        Map<String, Contact> currentContactList = mContacts.getValue();
        Collection<Contact> lSetToRemove = new HashSet<>();
        for (String uid: uidsToRemove) {
            Contact lRemovedContact = currentContactList.remove(uid);
            if (lRemovedContact != null) {
                lSetToRemove.add(lRemovedContact);
            }
        }

        mContacts.setValue(currentContactList);

        HttpTempo.getInstance().removeContacts(AuthManager.instance.getUserId(),
                                                lSetToRemove,
                                                () -> mSQL.removeContacts(uidsToRemove));
    }

    public void setEmptyMsgsForContact(String uid) {
        Map<String, Contact> currentContactList = mContacts.getValue();
        Contact contact = currentContactList.get(uid);
        contact.setHasPendingMessages(false);
        mContacts.setValue(currentContactList);

        mSQL.setPendingMsgsForUser(uid, false);
    }

    public void newMsgReceivedFromUser(String uid, String senderName,
                                       String chatTopic, String msgBody) {
        Map<String, Contact> currentContactList = mContacts.getValue();

        if (currentContactList.containsKey(uid)) {
            Contact contact = currentContactList.get(uid);
            contact.setHasPendingMessages(true);
            mSQL.setPendingMsgsForUser(uid, true);
        }
        else {
            Contact contact = new Contact();
            contact.setName(senderName);
            contact.setUid(uid);
            contact.setHasPendingMessages(true);
            contact.setLastTimeActivity(System.currentTimeMillis());
            contact.setServiceTopic(chatTopic);
            contact.setLastMessage(msgBody);
            currentContactList.put(uid, contact);
        }

        mContacts.setValue(currentContactList);
    }

    /** Downloads and updates contacts information */
    private void requestMoreInfoFromContacts(Context context) {

        mSQL.clearAbilitiesNeedsFromContacts();

        Map<String, Contact> currentContacts = mContacts.getValue();

        ArrayList<String> lUsrIds = new ArrayList<>();
        for (Contact contact : currentContacts.values()) {
            lUsrIds.add(contact.getUid());
        }

        String GET_URL = HttpTempo.URL_INDEX +
                "?action=get_contacts_fresh_info&controller=Search" +
                "&contacts_list='" + TextUtils.join("','", lUsrIds) + "'";

        JsonArrayRequest jsonArrayRequest =
                new JsonArrayRequest(Request.Method.GET,
                        GET_URL,
                        null,
                        response ->
                        {
                            int lNumWorkers = response.length();
                            try {
                                for (int i = 0; i < lNumWorkers; i++) {
                                    JSONObject jo = response.getJSONObject(i);

                                    String uid = jo.getString("uid");
                                    Contact c = currentContacts.get(uid);

                                    c.setName(jo.getString("name"));
                                    c.setCity(jo.getString("location")); // TODO
                                    c.setLonLat(0, 0); // TODO
                                    c.setAvgMark((float) jo.getDouble("average_mark"));
                                    c.setNumVotes(jo.getInt("num_votes"));
                                    c.setLastDateImageChanged(jo.getString("pics_change_time"));

                                    JSONArray services = jo.getJSONArray("services");

                                    for (int a = 0; a < services.length(); a++) {
                                        JSONObject s = services.getJSONObject(a);
                                        boolean isANeed = s.getInt("is_a_need") == 1;
                                        Service service =
                                                new Service(s.getString("category"),
                                                        s.getString("detailed_descr"),
                                                        isANeed);
                                        if (isANeed) {
                                            mSQL.addContactNeed(c, uid, service);
                                        }
                                        else {
                                            mSQL.addContactAbility(c, uid, service);
                                        }
                                    }

                                    mSQL.storeContact(c);
                                }
                            } catch (org.json.JSONException je) {
                                Log.e(TAG, "Error parsing response (requestNearServices)", je);
                                mContacts.setValue(mSQL.getAllContacts());
                            }
                            mContacts.setValue(currentContacts);

                        }, error -> {
                            Log.e(TAG, "Error getting JSONArray (requestNearServices)", error);
                            mContacts.setValue(mSQL.getAllContacts());
                        }
                );

        // Access the RequestQueue through your singleton class.
        HttpTempo.getInstance().getRequestQueue(context).add(jsonArrayRequest);
    }
}
