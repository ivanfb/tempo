package com.bandesoft.tempo.Model;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bandesoft.tempo.Remote.HttpTempo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class WorkerViewModel extends ViewModel {

    private static final String TAG = "WorkerViewModel";

    private MutableLiveData<Worker> mWorker = new MutableLiveData<>();

    private MutableLiveData<ArrayList<Opinion>> mOpinions;

    private MutableLiveData<ArrayList<Opinion>> mGivenOpinions;

    public void setWorker(Worker w) {
        mWorker.setValue(w);
    }

    public String getUID() { return mWorker.getValue().getUid(); }

    public String getWorkerName() { return mWorker.getValue().getName(); }

    public MutableLiveData<Worker> getWorker() {
        return mWorker;
    }

    public MutableLiveData<ArrayList<Opinion>> getOpinions() {
        if (mOpinions == null) {
            mOpinions = new MutableLiveData<>();
        }
        return mOpinions;
    }

    public MutableLiveData<ArrayList<Opinion>> getGivenOpinions() {
        if (mGivenOpinions == null) {
            mGivenOpinions = new MutableLiveData<>();
        }
        return mGivenOpinions;
    }

    public String getLastDateImageChanged() {
        return mWorker.getValue().getLastDateImageChanged();
    }

    public void requestOpinions(Context context, String uid) {
        String GET_URL = HttpTempo.URL_SEARCH_OPINIONS + "&user_id=" + uid;

        // For debugging purposes
//        StringRequest lStringReq = new StringRequest(GET_URL,
//                response -> {
//                    String a = response;
//                },
//                error -> {
//
//                }
//          );
//        HttpTempo.getInstance().getRequestQueue().add(lStringReq);

        JsonObjectRequest jsonArrayRequest =
                new JsonObjectRequest(Request.Method.GET,
                        GET_URL,
                        null,
                        response ->
                        {
                            try {
                                // received opinions
                                JSONArray lReceivedOpinions =
                                        (JSONArray)
                                                response.get("received_opinions");

                                ArrayList<Opinion> userOpinions = new ArrayList<>();
                                int lNumOpinions = lReceivedOpinions.length();
                                for (int i = 0; i < lNumOpinions; i++) {
                                    JSONObject jo = lReceivedOpinions.getJSONObject(i);
                                    Opinion op = new Opinion(jo.getString("evaluator_name"),
                                            jo.getString("evaluation_date"),
                                            (float) jo.getDouble("mark"),
                                            jo.getString("comment"));

                                    userOpinions.add(op);
                                }

                                mOpinions.setValue(userOpinions);

                                // given opinions
                                JSONArray lGivenOpinions =
                                        (JSONArray)
                                                response.get("given_opinions");

                                ArrayList<Opinion> userGivenOpinions = new ArrayList<>();
                                int lNumGivenOpinions = lGivenOpinions.length();
                                for (int i = 0; i < lNumGivenOpinions; i++) {
                                    JSONObject jo = lGivenOpinions.getJSONObject(i);
                                    Opinion op = new Opinion(jo.getString("opinion_receiver"),
                                            jo.getString("evaluation_date"),
                                            (float) jo.getDouble("mark"),
                                            jo.getString("comment"));

                                    userGivenOpinions.add(op);
                                }

                                mGivenOpinions.setValue(userGivenOpinions);
                            }
                            catch (JSONException e) {
                                Log.e(TAG, "Error parsing opinions response [" +
                                e.getMessage() + " ]");
                            }

                        }, error ->
                        Log.e(TAG, "Error getting JSONArray (requestOpinions)", error)
                );

        // Access the RequestQueue through your singleton class.
        HttpTempo.getInstance().getRequestQueue(context).add(jsonArrayRequest);
    }

    /**
     * This was originally intended for deep DynamicLink purposes
     */
    public void requestAllUserInfo(Context context, String uid) {
        // For debugging purposes
        String GET_URL = HttpTempo.URL_GET_USER_INFO + "&uid=" + uid;

//        StringRequest lStringReq = new StringRequest(GET_URL,
//                response -> {
//                    String a = response;
//                },
//                error -> {
//
//                }
//          );
//        HttpTempo.getInstance().getRequestQueue().add(lStringReq);

        JsonObjectRequest jsonArrayRequest =
                new JsonObjectRequest(Request.Method.GET,
                        GET_URL,
                        null,
                        response ->
                        {
                            try {
                                Worker w = new Worker(response.getString("uid"),
                                        response.getString("name"),
                                        response.getString("location"),
                                        (float) response.getDouble("average_mark"),
                                        response.getInt("num_votes"),
                                        0,
                                        0,
                                        response.getString("pics_change_time"));

                                JSONArray services = response.getJSONArray("services");

                                for (int a = 0; a < services.length(); a++) {
                                    JSONObject s = services.getJSONObject(a);

                                    Service service =
                                            new Service(s.getString("category"),
                                                    s.getString("detailed_descr"),
                                                    s.getInt("is_a_need") == 1);
                                    w.addService(service);
                                }

                                mWorker.setValue(w);

                                // received opinions
                                JSONArray lReceivedOpinions =
                                        response.getJSONArray("opinions")
                                                .getJSONObject(0)
                                                .getJSONArray("received_opinions");

                                ArrayList<Opinion> userOpinions = new ArrayList<>();
                                int lNumOpinions = lReceivedOpinions.length();
                                for (int i = 0; i < lNumOpinions; i++) {
                                    JSONObject jo = lReceivedOpinions.getJSONObject(i);
                                    Opinion op = new Opinion(jo.getString("evaluator_name"),
                                            jo.getString("evaluation_date"),
                                            (float) jo.getDouble("mark"),
                                            jo.getString("topic"));

                                    userOpinions.add(op);
                                }

                                mOpinions.setValue(userOpinions);

                                // given opinions
                                JSONArray lGivenOpinions =
                                        response.getJSONArray("opinions")
                                                .getJSONObject(0)
                                                .getJSONArray("given_opinions");

                                ArrayList<Opinion> userGivenOpinions = new ArrayList<>();
                                int lNumGivenOpinions = lGivenOpinions.length();
                                for (int i = 0; i < lNumGivenOpinions; i++) {
                                    JSONObject jo = lGivenOpinions.getJSONObject(i);
                                    Opinion op = new Opinion(jo.getString("opinion_receiver"),
                                            jo.getString("evaluation_date"),
                                            (float) jo.getDouble("mark"),
                                            jo.getString("topic"));

                                    userGivenOpinions.add(op);
                                }

                                mGivenOpinions.setValue(userGivenOpinions);
                            }
                            catch (JSONException e) {
                                Log.e(TAG, "Error parsing opinions response [" +
                                        e.getMessage() + " ]");
                            }

                        }, error ->
                        Log.e(TAG, "Error getting JSONArray (requestOpinions)", error)
                );

        // Access the RequestQueue through your singleton class.
        HttpTempo.getInstance().getRequestQueue(context).add(jsonArrayRequest);
    }

    public ArrayList<Service> getAbilities() {
        return mWorker.getValue().getAbilities();
    }

    public ArrayList<Service> getNeeds() {
        return mWorker.getValue().getNeeds();
    }
}
