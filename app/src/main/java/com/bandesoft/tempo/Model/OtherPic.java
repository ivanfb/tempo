package com.bandesoft.tempo.Model;

import android.graphics.Bitmap;

/**
 * This class is aimed to contain information and reference of a particular
 * "other pic" of the current user.
 */
public class OtherPic {
    private String mFileName;
    private Bitmap mBitmap;

    public OtherPic(String fileName, Bitmap bitmap) {
        this.mFileName = fileName;
        this.mBitmap = bitmap;
    }

    public String getFileName() { return mFileName; }
    public Bitmap getBitmap() { return mBitmap; }
}
