package com.bandesoft.tempo.Model;

import java.io.Serializable;

/** This could be used for both needs and offered services */
public class Service implements Serializable {
    private String category;
    private String detailedDescr; // detailed description
    private Boolean is_A_Need;

    public Service(String category, String detailedDescr, Boolean is_A_Need) {
        this.category = category;
        this.detailedDescr = detailedDescr;
        this.is_A_Need = is_A_Need;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDetailedDescr(String detailedDescr) {
        this.detailedDescr = detailedDescr;
    }

    public String getCategory() {
        return category;
    }

    public String getDetailedDescr() {
        return detailedDescr;
    }

    public void setIs_A_Need(Boolean is_A_Need) {
        this.is_A_Need = is_A_Need;
    }

    public Boolean getIs_A_Need() { return is_A_Need; }

}
