package com.bandesoft.tempo.Model;

import java.io.Serializable;

public class LonLatCity implements Serializable {
    private double lon, lat;
    private String cityName;
    private int radiusKm;

    public LonLatCity() {}

    public LonLatCity(double lon, double lat, String cityName) {
        this.lon = lon;
        this.lat = lat;
        this.cityName = cityName;
    }

    public double getLon() {return lon;}
    public double getLat() {return lat;}
    public String getCityName() {return cityName;}

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setLon(double lon) { this.lon = lon; }
    public void setLat(double lat) { this.lat = lat; }

    public int getRadiusKm() {
        return radiusKm;
    }

    public void setRadiusKm(int radiusKm) {
        this.radiusKm = radiusKm;
    }

    public void clear() {
        this.lon = this.lat = 0;
        this.cityName = "";
        this.radiusKm = 0;
    }
}
