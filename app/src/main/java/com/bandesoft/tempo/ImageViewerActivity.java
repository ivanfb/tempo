package com.bandesoft.tempo;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatButton;

public class ImageViewerActivity extends BaseActivity
                implements View.OnClickListener {

    // this is set before starting this activity
    public static Bitmap mBitmapToShow = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);

        AppCompatButton lBtnLeft, lBtnRight;

        lBtnLeft = findViewById(R.id.btn_left);
        lBtnLeft.setOnClickListener(this);

        lBtnRight = findViewById(R.id.btn_right);
        lBtnRight.setOnClickListener(this);

        ImageView v = findViewById(R.id.current_pic);
        if (mBitmapToShow != null) {
            v.setImageBitmap(mBitmapToShow);
        }
        else {
            finish(); // do nothing
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_left:
                break;
            case R.id.btn_right:
                break;
        }
    }
}
