package com.bandesoft.tempo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bandesoft.tempo.Model.Contact;
import com.bandesoft.tempo.Model.ContactsViewModel;
import com.bandesoft.tempo.Model.JsonMessage;
import com.bandesoft.tempo.Model.SQL;
import com.bandesoft.tempo.Model.Worker;
import com.bandesoft.tempo.Remote.HttpTempo;
import com.bandesoft.tempo.Service.JabberService;
import com.bandesoft.tempo.Utils.NotifsManager;
import com.bandesoft.tempo.Vista.ContactsList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

public class ContactsFragment extends Fragment
        implements AdapterView.OnItemClickListener,
                   AdapterView.OnItemLongClickListener {
    private static final String TAG = "ContactsFragment";

    private ContactsViewModel mViewModel;
    private ListView mListContacts;
    private ArrayList<Contact> mContacts;
    private ContactsList mContactsListAdapter;

    private SQL mSQL;

    private int mNumSelectedItems = 0;

    private Animation mFadeInAnimation;
    private Animation mFadeOutAnimation;

    private MenuItem mMenuNumSelectedItems;

    private ArrayList<String> mUidsToRemove = new ArrayList<>();

    private BroadcastReceiver mBroadcastReceiver; // to update pending messages to be read

    private static final Comparator<Contact> mContactComparator = (o1, o2) ->
         (int) ((o2.getLastTimeActivity() - o1.getLastTimeActivity()) / 1000);

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ContactsFragment() {  }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        NotifsManager.clearNotifs(getActivity());

// this is useful when resuming from screen off and messages were received when screen was off
        if (mSQL.getNumChatsWithPendingMessages() > 0 || mSQL.mChatTopicChanged) {
            mViewModel.updateContactsFromSql();
            mSQL.mChatTopicChanged = false;
        }

        if (mSQL.contactsNeedLocalUpdate()) {
            mViewModel.updateContactsFromSql();
            mSQL.resetContactsNeedLocalUpdate();
        }

        // update contacts remotely
        if (mSQL.contactSetNeedsRemoteUpdate()) {
            Worker me = mSQL.getMe();
            if (me != null) {
                HttpTempo.getInstance().saveMyContacts(me.getUid(),
                        mSQL.getAllContacts().values(),
                        () -> mSQL.resetContactSetNeedsRemoteUpdate());
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_agenda, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        goToChatIfComesFromNotif();

        mFadeInAnimation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.fade_in);
        mFadeOutAnimation = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.fade_out);

        mSQL = SQL.getInstance(getActivity());

        TextView lTxtNoContactsYet = getView().findViewById(R.id.txt_no_contacts_yet);
        if (mSQL.getNumWorkers() <= 1) {
            lTxtNoContactsYet.setVisibility(View.VISIBLE);
        }

        mListContacts = getView().findViewById(R.id.list_contacts);
        mListContacts.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        mViewModel = new ViewModelProvider(this).get(ContactsViewModel.class);
        // TODO improve the next line
        mViewModel.setSQL_instance(mSQL);

        mContacts = new ArrayList<>(mSQL.getAllContacts().values());
        mContactsListAdapter = new ContactsList(getActivity(), mContacts);
        mListContacts.setAdapter(mContactsListAdapter);

        mViewModel.updateContactsFromSql(); // read info from local sqlite
        mViewModel.getContacts(getContext()).observe(this, contactsMap ->
                {
                    mContacts = new ArrayList<>(contactsMap.values());
                    Collections.sort(mContacts, mContactComparator);
                    mContactsListAdapter.clear();
                    mContactsListAdapter.addAll(mContacts);

                    mContactsListAdapter.notifyDataSetChanged();
                }
        );

        mListContacts.setOnItemClickListener(this);
        mListContacts.setOnItemLongClickListener(this);

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action == null) {
                    return; // do nothing
                }

                switch (action) {
                    case JabberService.NEW_MESSAGE:
                        Log.d(TAG, "receiving message");

                        String from = intent.getStringExtra(JabberService.BUNDLE_FROM_JID);
                        String uid = from;
                        if (from.contains("@")) {
                            uid = from.split("@")[0];
                        }

                        String senderName = intent.getStringExtra(JabberService.BUNDLE_SENDER_NAME);
                        String chatTopic = intent.getStringExtra(JabberService.BUNDLE_CHAT_TOPIC);

                        String msgBody = "";
                        int messageType = intent.getIntExtra(JabberService.BUNDLE_MESSAGE_TYPE, JsonMessage.NORMAL);
                        if (messageType == JsonMessage.NORMAL) {
                            msgBody = intent.getStringExtra(JabberService.BUNDLE_MESSAGE_BODY);
                        }

                        mViewModel.newMsgReceivedFromUser(uid, senderName, chatTopic, msgBody);
                }
            }
        };
    }

    private void updateItemSelection(View view, int position) {
        Worker w = mContacts.get(position);

        ImageView imgItemSel = view.findViewById(R.id.imgItemSelected);

        if (imgItemSel.getVisibility() == View.VISIBLE) {
            imgItemSel.clearAnimation();
            imgItemSel.setVisibility(View.INVISIBLE);
            imgItemSel.startAnimation(mFadeOutAnimation);
            view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));

            w.setSelected(false);
            view.setSelected(false);

            mNumSelectedItems--;

            if (mNumSelectedItems == 0) {
                setHasOptionsMenu(false);
            }

            mUidsToRemove.remove(w.getUid());
        }
        else {
            setHasOptionsMenu(true);
            imgItemSel.clearAnimation();
            imgItemSel.setVisibility(View.VISIBLE);
            imgItemSel.startAnimation(mFadeInAnimation);
            view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorGreySoft));

            w.setSelected(true);
            view.setSelected(true);

            mNumSelectedItems++;

            mUidsToRemove.add(w.getUid());
        }

        if (mMenuNumSelectedItems != null) {
            mMenuNumSelectedItems.setTitle(Integer.toString(mNumSelectedItems));
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mNumSelectedItems == 0) {
            Contact c = mContacts.get(position);
            startChatActivity(c.getUid(), c.getName(), c.getServiceTopic());
        }
        else { // we are in select mode, then, select this item
            updateItemSelection(view, position);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        updateItemSelection(view, position);
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_contacts_fragment, menu);

        MenuItem lBinMenu = menu.findItem(R.id.bin);
        lBinMenu.setOnMenuItemClickListener(item ->
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle(getString(R.string.deleting_contacts));
            alertDialog.setPositiveButton("Ok",
                    (dialog, which) -> deleteSelectedContacts());
            alertDialog.setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel() );
            alertDialog.show();
            return true;
        });
        mMenuNumSelectedItems = menu.findItem(R.id.numSelected);

        mMenuNumSelectedItems.setTitle(Integer.toString(mNumSelectedItems));

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void deleteSelectedContacts() {
        String[] arrayContactsRm = new String[mUidsToRemove.size()];
        arrayContactsRm = mUidsToRemove.toArray(arrayContactsRm);

        mViewModel.removeContacts(arrayContactsRm);

        // check if there are still pending messages
        boolean lThereAreAnyPendingMsgs = false;
        for (Contact contact: mContacts) {
            if (contact.hasPendingMsgs()) {
                lThereAreAnyPendingMsgs = true;
            }
        }

        if (!lThereAreAnyPendingMsgs) {
            MainActivity lMainActivity = (MainActivity) getActivity();
            lMainActivity.setNoPendingMessages();
        }
        // end check if there are still pending messages after removal

        mNumSelectedItems = 0;
        setHasOptionsMenu(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        IntentFilter filter = new IntentFilter(JabberService.NEW_MESSAGE);
        getActivity().registerReceiver(mBroadcastReceiver, filter);
    }

    @Override
    public void onStop() {
        super.onStop();

        try {
            getActivity().unregisterReceiver(mBroadcastReceiver);
        } catch(IllegalArgumentException e) {
            Log.e(TAG, "Exception destroying", e);
        }
    }

    private void goToChatIfComesFromNotif() {
        Bundle args = getArguments();
        if (args != null) {
            // in that case, we can assume it comes from a notification. The user clicked the notification,
            // Then, we show the chat with the user who raised the notification
            String lUid = args.getString(NotifsManager.SENDER_ID);
            String lContactName = args.getString(NotifsManager.SENDER_NAME);
            String lChatTopic = args.getString(NotifsManager.CHAT_TOPIC);

            startChatActivity(lUid, lContactName, lChatTopic);
        }
    }

    private void startChatActivity(String contactID,
                                   String contactName,
                                   String chatTopic) {
        Intent i = new Intent(getActivity(), ChatActivity.class);

        i.putExtra(ChatActivity.STR_EXTR_CONTACT_UID, contactID);
        i.putExtra(ChatActivity.STR_EXTR_CONTACT_NAME, contactName);
        i.putExtra(ChatActivity.STR_EXTR_SERVICE_NAME, chatTopic);
        startActivity(i);

        if (mViewModel != null) {
            mViewModel.setEmptyMsgsForContact(contactID);
        }
    }
}
