package com.bandesoft.tempo;

import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;
import com.bandesoft.tempo.Remote.AuthManager;
import com.bandesoft.tempo.Remote.HttpTempo;

import java.util.HashMap;
import java.util.Map;

public class SuggestionsActivity extends BaseActivity {

    private static final String TAG = "SuggestionsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestions);

        super.initToolBar();

        findViewById(R.id.btn_send_suggestion).setOnClickListener(view -> {
            EditText lTxtSuggestion = findViewById(R.id.text_suggestion);
            if (lTxtSuggestion.length() != 0)
            {
                sendSuggestion(lTxtSuggestion.getText().toString());
            }
            else {
                Toast.makeText(this, getString(R.string.please_add_suggestion),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void sendSuggestion(String suggestion) {
        StringRequest sendSuggestion = new StringRequest(Request.Method.POST,
                HttpTempo.URL_INDEX,
                (response) -> {
                    String respWithoutCR = response.replaceAll("\\n", "");
                    if (respWithoutCR.contains("OK")) {
                        Toast.makeText(this, R.string.the_suggestion_could_be_sent_correctly,
                                Toast.LENGTH_LONG).show();
                        finish();
                    }
                    else {
                        Toast.makeText(this, R.string.the_suggestion_could_not_be_sent,
                                Toast.LENGTH_LONG).show();
                    }
                }, (volleyError) ->  {

            if (volleyError instanceof TimeoutError) {
                Log.e(TAG, "Error getting JSONArray (sendFeedback)",
                        volleyError);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("controller", "User");
                headers.put("action", "send_suggestion");
                headers.put("uid", AuthManager.instance.getUserId());
                headers.put("suggestion", suggestion);
                return headers;
            }

            @Override
            public Request.Priority getPriority() {
                return Request.Priority.IMMEDIATE;
            }
        };

        HttpTempo.getInstance().getRequestQueue(this).add(sendSuggestion);
    }
}